# jtda #

jtda is a java-based interface to the TD Ameritrade API.

## Overview ##
The goal is to provide a simple stand-alone library (jar) of commands.
It also provides a sample command line client to offer example use cases.

### Run tests / generate coverage report ###
`$ mvn clean verify`

### Build the standalone jar ###
`$ mvn clean package`

### Run the CLI ###
`$ java -jar target/jtda.jar`

### Usage ###
To my knowledge there is no test or demo account to use for testing :(
Therefore, you will need a TD account in order to use jtda.
  

Create the file `~/.jtda/config` ensuring it contains your application's
source (obtained from TDA) and your application's version.

    source=TESTSOURCE
    version=1.0


Then start the cli
    
    $ java -jar target/jtda-1.0-SNAPSHOT.jar 
    
    unable to find credentials file /Users/jeremy/.jtda/credentials
    UserId: myusername
    Password: 
    Welcome, myusername.
    
    >balance
   
    NetLiq      : 9187.63
    Available   : 2653.63
    Cash Balance: 4867.63

    >volhistory aapl -p 1y

    Symbol: AAPL
    DataDate        DataTime        Vol
    2015-12-01      05:00:00.000    0.201033
    2015-12-02      05:00:00.000    0.20703
    2015-12-03      05:00:00.000    0.219194
    2015-12-04      05:00:00.000    0.185351
    2015-12-07      05:00:00.000    0.199919
    2015-12-08      05:00:00.000    0.210548
    . . .
    2016-11-21      05:00:00.000    0.174414
    2016-11-22      05:00:00.000    0.169042
    2016-11-23      05:00:00.000    0.162747
    2016-11-25      05:00:00.000    0.163574
    2016-11-28      05:00:00.000    0.172116
    2016-11-29      05:00:00.000    0.165782
    2016-11-30      05:00:00.000    0.173455
    2016-12-01      05:00:00.000    0.178549
    
    >mls aapl

    Symbol : AAPL
    Last   : 217.58
    Bid    : 217.5
    Ask    : 217.55
    BASize : 400X1000
    YdayCls: 217.58
    Expirations:
    2018-08-24	AAPL_082418C245	0.00	0.00	0X0	�		245	AAPL_082418P245	0.00	0.00	0X0	�
    2018-08-31	AAPL_083118C245	0.00	0.00	0X0	�		245	AAPL_083118P245	0.00	0.00	0X0	�
    2018-09-07	AAPL_090718C245	0.00	0.00	0X0	�		245	AAPL_090718P245	0.00	0.00	0X0	�
    2018-09-14	AAPL_091418C245	0.00	0.00	0X0	�		245	AAPL_091418P245	0.00	0.00	0X0	�
    2018-09-21	AAPL_092118C187.5	0.00	0.00	0X0	�		187.5	AAPL_092118P187.5	0.00	0.00	0X0	�
    2018-09-28	AAPL_092818C245	0.00	0.00	0X0	�		245	AAPL_092818P245	0.00	0.00	0X0	�
    2018-10-19	AAPL_101918C220	5.95	6.05	15X2	0.4680	19.676	220	AAPL_101918P220	7.45	7.55	2X15	-0.5380	17.975
    2018-11-16	AAPL_111618C220	8.65	8.75	1X1	0.4890	22.462	220	AAPL_111618P220	10.10	10.25	41X15	-0.5130	21.371
    2018-12-21	AAPL_122118C220	10.15	10.25	2X2	0.4980	21.846	220	AAPL_122118P220	11.35	11.50	12X15	-0.5020	20.814
    2019-01-18	AAPL_011819C220	11.40	11.50	2X12	0.5050	21.86	220	AAPL_011819P220	12.20	12.30	2X8	-0.4960	20.443
    2019-02-15	AAPL_021519C220	13.15	13.35	5X6	0.5130	22.966	220	AAPL_021519P220	13.80	14.00	12X4	-0.4870	21.657
    2019-04-18	AAPL_041819C2.5	0.00	0.00	0X0	�		2.5	AAPL_041819P2.5	0.00	0.00	0X0	�
    2019-06-21	AAPL_062119C220	18.15	18.35	3X1	0.5320	23.651	220	AAPL_062119P220	17.65	17.85	12X2	-0.4670	22.031
    2020-01-17	AAPL_011720C225	22.80	23.00	22X1	0.5210	24.52	225	AAPL_011720P225	25.05	25.30	41X12	-0.4800	22.209
    2020-06-19	AAPL_061920C225	26.35	26.80	1X12	0.5340	24.526	225	AAPL_061920P225	27.20	27.95	3X3	-0.4650	22.04
    
    >buy 1000 AAPL_101918C220 0.01

     1883229576 B O 1000 AAPL Oct 19 2018 220 Call 0.01
    
    # Look up quotes for the most liquid strikes (mls) on BofA and then "short" it by buying the Dec 14th, $27.5 put 
    # for its bid price with our "short N" command syntax.
    # You can also go long (buying a call) with the "long N" syntax.  Both commands default to one contract and 
    # the N represents strike line from the 'mls' command (first column of output).
    
    >mls bac

    Symbol : BAC (Bank of America Corporation Common Stock)
    Last   : 27.75
    Bid    : 27.68
    Ask    : 27.79
    BASize : 800X1800
    YdayCls: 27.75
    Expirations:
    1**	2018-11-23	BAC_112318C28	0.23	0.24	200X113	0.3950	24.236	28	BAC_112318P28	0.45	0.48	151X319	-0.6120	22.668
    2**	2018-11-30	BAC_113018C28	0.41	0.44	319X319	0.4390	25.713	28	BAC_113018P28	0.62	0.66	332X332	-0.5660	24.237
    3-*	2018-12-07	BAC_120718C27.5	0.78	0.83	128X131	0.5730	25.985	27.5	BAC_120718P27.5	0.59	0.62	54X115	-0.4300	28.202
    4**	2018-12-14	BAC_121418C27.5	0.87	0.91	38X67	0.5680	25.205	27.5	BAC_121418P27.5	0.69	0.72	316X80	-0.4340	27.69
    5	2018-12-21	BAC_122118C20.5	0.00	0.00	0X0	�		20.5	BAC_122118P20.5	0.00	0.00	0X0	�
    6--	2018-12-28	BAC_122818C27.5	1.02	1.07	11X54	0.5620	24.649	27.5	BAC_122818P27.5	0.82	0.87	16X89	-0.4380	26.373
    7**	2019-01-18	BAC_011819C28	1.07	1.10	54X161	0.4900	26.423	28	BAC_011819P28	1.32	1.36	31X319	-0.5060	27.002
    8--	2019-02-15	BAC_021519C28	1.30	1.38	319X263	0.5000	26.514	28	BAC_021519P28	1.51	1.58	263X252	-0.4960	26.246
    9--	2019-05-17	BAC_051719C28	1.91	2.02	252X254	0.5180	26.67	28	BAC_051719P28	2.04	2.15	275X252	-0.4750	25.884
    10--	2019-06-21	BAC_062119C28	2.09	2.18	252X331	0.5220	26.411	28	BAC_062119P28	2.23	2.31	252X287	-0.4690	25.878
    11--	2020-01-17	BAC_011720C27	3.55	3.75	2X2550	0.5890	27.935	27	BAC_011720P27	2.52	2.62	322X199	-0.3930	25.985
    12	2020-06-19	BAC_061920C27	3.75	4.25	1810X760	0.5890	26.685	27	BAC_061920P27	2.72	3.05	1813X1039	-0.3870	24.965
    13	2021-01-15	BAC_011521C28	3.40	5.10	200X1544	0.5580	27.387	28	BAC_011521P28	2.83	3.90	30X1	-0.4280	21.51

    >short 4

    buy 1 BAC_121418P27.5 @ 0.69
    1977243447 B O 1 BAC Dec 14 2018 27.5 Put (Weekly) 0.69

    >os open

    orderId	status	receivedTime	reportedTime	timeToReport	strategy	price
    1977243447	Open	2018-11-18 20:09:15 EST	null	N/A	null	0.69
        1977243447	1 B2O	BAC DEC 14 2018 27.5 Put
    
    >cancel 1977243447

    0: The order was submitted for cancellation. Please check its status.

    >os open

    orderId	status	receivedTime	reportedTime	timeToReport	strategy	price

    >q
    $

The java API will be something like:

    JtdaClient client = JtdaClient.builder()
        .withAppId("abcdef")      //source in tda api
        .withAppVersion("1.0")    //version property in tda api
        .build()
    String usr = ...;
    String pwd = ...;
    client.login(usr, pwd)
    
    //Get implied vol history sampled every day of the last year 
    HistoryRequest request = VolHistoryRequest.builder()
        .forSymbols(symbolList)
        .every(DAY)
        .duringLast(Period.withYears(1))
        .build();
    HistoryResponse response = client.fetchHistory(request)

    //Get historical vol history sampled every month of the last year     
    HistoryRequest request2 = VolHistoryRequest.builder()
        .forSymbols(symbolList)
        .usingHistoricalVol()
        .every(1, MONTH)
        .starting(LocalDate.now().minusDays(365))
        .ending(LocalDate.now())
        .build();
    HistoryResponse response2 = client.fetchHistory(request2)

    //Get implied vol history sampled every 2 days     
    PriceHistoryRequest request3 = PriceHistoryRequest.builder()
        .forSymbols(symbolList)
        .every(2, DAY)
        .ending(LocalDate.now())
        .build();
    HistoryResponse response3 = client.fetchHistory(request3)

### Scripts ###
You can run scripts against jtda
 
 For example with the following:
`cat scripts/vh-delta.txt|java -jar target/jtda-1.0-SNAPSHOT.jar`

jtda will make a VolatilityHistory request for implied volatility all possible
DTE and delta values for FB and download the results to files, one per
request to ~/jtdaData/
    
### API Notes ###

For the VolatilityHistory command the SKEW surfacetype does not appear to be a valid 
request anymore with either the 100, 200, or 300 versions of the api as all of these 
requests fail (it didn't matter the setting for surfacetypevalue, they all failed):
https://apis.tdameritrade.com/apps/100/VolatilityHistory;jsessionid=XXXX?source=YOUR_SOURCE&requestidentifiertype=SYMBOL&requestvalue=AAPL&volatilityhistorytype=I&period=1&periodtype=YEAR&intervalduration=1&intervaltype=DAILY&startdate=&enddate=20180602&daystoexpiration=90&surfacetypeidentifier=SKEW&surfacetypevalue=60%2C-50 
https://apis.tdameritrade.com/apps/200/VolatilityHistory;jsessionid=XXXX?source=YOUR_SOURCE&requestidentifiertype=SYMBOL&requestvalue=AAPL&volatilityhistorytype=I&period=1&periodtype=YEAR&intervalduration=1&intervaltype=DAILY&startdate=&enddate=20180602&daystoexpiration=90&surfacetypeidentifier=SKEW&surfacetypevalue=60%2C-50 
https://apis.tdameritrade.com/apps/300/VolatilityHistory;jsessionid=XXXX?source=YOUR_SOURCE&requestidentifiertype=SYMBOL&requestvalue=AAPL&volatilityhistorytype=I&period=1&periodtype=YEAR&intervalduration=1&intervaltype=DAILY&startdate=&enddate=20180602&daystoexpiration=90&surfacetypeidentifier=SKEW&surfacetypevalue=60%2C-50 

After the session has expired (jsessionid is no longer valid), executing a request 
for a command with a binary (not xml) response can return 0x00 as the first byte. According
to the API if the first byte is 0x01 an error occurred. However, a 0x00 is the first byte
returned for a BinaryOptionChain request with an expired session. This is a bug in the TDA api.
jtda handles this case by only executing commands when the session is valid.