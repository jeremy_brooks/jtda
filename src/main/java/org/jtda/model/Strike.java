package org.jtda.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Objects;

/**
 * Created by jeremy on 06-11-2018.
 */
public class Strike {
    private static final Logger LOG = LoggerFactory.getLogger(Strike.class);
    private OptionContract callContract;
    private OptionContract putContract;

    /**
     * Construct a strike with the given contract
     * and fill in a NULL contract for the opposing/matching contract.
     *
     * @param callOrPutContract the contract from which to construct a strike
     */
    public Strike(final OptionContract callOrPutContract) {
        switch (callOrPutContract.getRight()) {
            case 'C':
                callContract = callOrPutContract;
                putContract = new OptionContract(callContract);
                break;
            case 'P':
                putContract = callOrPutContract;
                callContract = new OptionContract(putContract);
                break;
            default:
                throw new IllegalArgumentException("only C(all) or P(ut) expected");
        }
    }

    public Strike(final OptionContract callContract, final OptionContract putContract) {
        this.callContract = Objects.requireNonNull(callContract);
        this.putContract = Objects.requireNonNull(putContract);
        if (!callContract.getStrikePrice().equals(putContract.getStrikePrice())) {
            LOG.trace("call={},put={}", callContract.toString(), putContract.toString());
            //throw new IllegalArgumentException("both contracts must have same strike price");
        }
        if (!callContract.getExpirationDate().equals(putContract.getExpirationDate())) {
            LOG.trace("call={},put={}", callContract.toString(), putContract.toString());
            //LOG.warn("cexpDate={},pexpDate={}", callContract.getExpirationDate(), putContract.getExpirationDate());
            throw new IllegalArgumentException("both contracts must have same expiration date");
        }
        if (callContract.isStandardOption() != putContract.isStandardOption()) {
            LOG.trace("call={},put={}", callContract.toString(), putContract.toString());
            //LOG.warn("cexpType={},pexpType={}", callContract.getExpirationType(), putContract.getExpirationType());
            //throw new IllegalArgumentException("contracts must be both standard or both non-standard options");
        }
    }

    public double getStrikePrice() {
        return callContract.getStrikePrice().doubleValue();
    }

    public LocalDate getExpirationDate() {
        return callContract.getExpirationDate();
    }

    public boolean isStandardOption() {
        return callContract.isStandardOption();
    }

    public OptionContract getCallContract() {
        return callContract;
    }

    public OptionContract getPutContract() {
        return putContract;
    }

    /**
     * Returns the absolute difference between the put and call mid price.
     *
     * @return the difference between put mid price and call mid price.
     */
    public double diff() {
        return Math.abs(putMidQuote() - callMidQuote());
    }

    public double callMidQuote() {
        if(callContract.getQuote() == null){
            return Double.NaN;
        }
        return callContract.getQuote().mid();
    }

    public double putMidQuote() {
        if(putContract.getQuote() == null){
            return Double.NaN;
        }
        return putContract.getQuote().mid();
    }

    public long combinedVolume(){
        return callContract.getQuote().getVolume() + putContract.getQuote().getVolume();
    }

    @Override
    public String toString() {
        return alternateToString();
//        return "Strike{" +
//                "price=" + getStrikePrice() +
//                ", callPrice=" + callMidQuote() +
//                ", putPrice=" + putMidQuote() +
//                ", diff()=" + diff() +
//                '}';
    }

    public String alternateToString() {
        return new ToStringBuilder(this)
                .append("callContract", callContract)
                .append("putContract", putContract)
                .toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        final Strike strike = (Strike) o;

        return new EqualsBuilder()
                .append(getCallContract(), strike.getCallContract())
                .append(getPutContract(), strike.getPutContract())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getCallContract())
                .append(getPutContract())
                .toHashCode();
    }

    /**
     * Returns true if the strike is 'not new' meaning both the call and the put
     * have non-zero bids and non-zero open interest. This can be handy when looking
     * at option chains on the weekend and you want to exclude newly added strikes
     * that have zero bid/ask/volume/open interest.
     *
     * Still a work in progress, don't use this yet
     *
     * @return
     */
    public boolean hasNonZeroBidsAndOpenInterest() {
        //TODO: This needs more unit testing; When i change the || logic to &&,
        //TODO: I get strikes with no bids and no open interest
        OptionContract contract = getCallContract();
        boolean hasNoBidsAndNoOpenInterest = false;
        if (Objects.nonNull(contract.getQuote())) {
            hasNoBidsAndNoOpenInterest = contract.getQuote().getBid() == 0  || contract.getQuote().getOpenInterest() == 0;
        }
        contract = getPutContract();
        if (Objects.nonNull(contract.getQuote())) {
            hasNoBidsAndNoOpenInterest = hasNoBidsAndNoOpenInterest && (contract.getQuote().getBid() == 0 || contract.getQuote().getOpenInterest() == 0);
        }
        if(hasNoBidsAndNoOpenInterest){
            LOG.trace("noBidNoOI strike " + getExpirationDate()
                    + "\ncall:" + getCallContract().toString()
                    + "\nput: " + getPutContract().toString());
        }
        return !hasNoBidsAndNoOpenInterest;
    }

    public static class CallPutDifferenceComparator implements Comparator<Strike> {
        @Override
        public int compare(Strike a, Strike b) {
            return a.diff() < b.diff()
                    ? -1
                    : a.diff() == b.diff() ? 0 : 1;
        }
    }


    /**
     * Order strikes by those with the most combined call and put volume.
     * Can help to find what strikes are most active today.
     */
    public static class CombinedCallPutVolumeComparator implements Comparator<Strike> {
        @Override
        public int compare(Strike a, Strike b) {
            return -1 * (a.combinedVolume() < b.combinedVolume()
                    ? -1
                    : a.combinedVolume() == b.combinedVolume() ? 0 : 1);
        }
    }

}
