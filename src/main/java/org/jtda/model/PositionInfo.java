package org.jtda.model;

import org.apache.commons.lang3.builder.CompareToBuilder;

import java.math.BigDecimal;

public class PositionInfo implements Comparable<PositionInfo> {
    private BigDecimal quantity;
    private String position;
    private String symbol;
    private String dte;
    private BigDecimal currentValue;
    private BigDecimal closePrice;
    private BigDecimal avgPrice;
    private BigDecimal profitAndLoss;
    private Double delta;
    private Double gamma;
    private Double theta;
    private Double vega;
    private Double iv;
    private BigDecimal maintenanceRequirement;

    @Override
    public int compareTo(PositionInfo o) {
        return new CompareToBuilder()
                .append(this.symbol, o.getSymbol())
                .append(this.dte, o.getDte())
                .toComparison();
    }

    @Override
    public String toString() {
        final String formatString = "%4.0f %-3s%15s%5s%10.2f%10.2f%10.2f%10.2f%8.2f%8.2f%8.2f%8.2f%7.2f%10.0f";
        return String.format(formatString, getQuantity(), getPosition(), symbol, dte,
                currentValue, closePrice, avgPrice, profitAndLoss, delta, gamma, theta, vega, iv, getMaintenanceRequirement());
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDte() {
        return dte;
    }

    public void setDte(String dte) {
        this.dte = dte;
    }

    public BigDecimal getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(BigDecimal currentValue) {
        this.currentValue = currentValue;
    }

    public BigDecimal getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    public BigDecimal getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(BigDecimal avgPrice) {
        this.avgPrice = avgPrice;
    }

    public BigDecimal getProfitAndLoss() {
        return profitAndLoss;
    }

    public void setProfitAndLoss(final BigDecimal profitAndLoss) {
        this.profitAndLoss = profitAndLoss;
    }

    public Double getDelta() {
        return delta;
    }

    public void setDelta(Double delta) {
        this.delta = delta;
    }

    public Double getGamma() {
        return gamma;
    }

    public void setGamma(Double gamma) {
        this.gamma = gamma;
    }

    public Double getTheta() {
        return theta;
    }

    public void setTheta(Double theta) {
        this.theta = theta;
    }

    public Double getVega() {
        return vega;
    }

    public void setVega(Double vega) {
        this.vega = vega;
    }

    public Double getIv() {
        return iv;
    }

    public void setIv(Double iv) {
        this.iv = iv;
    }

    public BigDecimal getMaintenanceRequirement() {
        return maintenanceRequirement;
    }

    public void setMaintenanceRequirement(BigDecimal maintenanceRequirement) {
        this.maintenanceRequirement = maintenanceRequirement;
    }
}
