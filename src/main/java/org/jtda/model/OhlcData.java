package org.jtda.model;

import java.time.ZonedDateTime;

public interface OhlcData {
    Float getOpen();
    Float getHigh();
    Float getLow();
    Float getClose();
    Float getVolume();
    ZonedDateTime getDateTimeUtc();
}
