package org.jtda.model;

import java.time.ZonedDateTime;

public class OhlcBar implements OhlcData {

    private final Float open;
    private final Float high;
    private final Float low;
    private final Float close;
    private final Float volume;
    private final ZonedDateTime dateTimeUtc;

    public OhlcBar(final Float open, final Float high, final Float low, final Float close,
                   final Float volume, final ZonedDateTime dateTimeUtc) {
        super();
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.dateTimeUtc = dateTimeUtc;
    }

    public Float getOpen() {
        return open;
    }

    public Float getHigh() {
        return high;
    }

    public Float getLow() {
        return low;
    }

    public Float getClose() {
        return close;
    }

    public Float getVolume() {
        return volume;
    }

    public ZonedDateTime getDateTimeUtc() {
        return dateTimeUtc;
    }


}
