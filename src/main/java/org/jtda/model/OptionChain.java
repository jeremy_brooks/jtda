package org.jtda.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by jeremy on 06-09-2018.
 */
public final class OptionChain {
    private static final Logger LOG = LoggerFactory.getLogger(OptionChain.class);

    private String underlyingSymbol;
    private String underlyingDescription;
    private double bid;
    private double ask;
    private String bidAskSize;
    private double last;
    private double open;
    private double high;
    private double low;
    private double close;
    private double volume;
    private double change;
    private boolean realTimeQuote;
    private String quoteTime;
    private Map<LocalDate, Series> series = new HashMap<>();


    public static class Series implements Comparable<Series> {
        private int expiration;
        private Map<Integer, OptionContract> callsByStrike;
        private Map<Integer, OptionContract> putsByStrike;

        public Series(final int expiration) {
            this.expiration = expiration;
            callsByStrike = new HashMap<>();
            putsByStrike = new HashMap<>();
        }

        public void add(OptionContract contract) {
            if (expiration == contract.getExpiration()) {
                if (contract.getRight() == 'C') {
                    callsByStrike.put(contract.getStrike(), contract);
                } else {
                    putsByStrike.put(contract.getStrike(), contract);
                }
            }
        }

        public int getExpiration() {
            return expiration;
        }


        public SortedSet<OptionContract> getCalls() {
            return new TreeSet<>(callsByStrike.values());
        }

        public SortedSet<OptionContract> getPuts() {
            return new TreeSet<>(putsByStrike.values());
        }

        public Set<Integer> getSetOfStrikes(){
            Set<Integer> set = new HashSet<>();
            set.addAll(callsByStrike.keySet());
            set.addAll(putsByStrike.keySet());
            return set;
        }

        public Strike getStrikeObject(final int strike) {
            return new Strike(callsByStrike.get(strike), putsByStrike.get(strike));
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;

            if (o == null || getClass() != o.getClass()) return false;

            final Series series = (Series) o;

            return new EqualsBuilder()
                    .append(expiration, series.expiration)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .append(expiration)
                    .toHashCode();
        }


        @Override
        public int compareTo(final Series o) {
            return Comparator.comparingInt(Series::getExpiration)
                    .compare(this, o);
        }


    }


    public void addContract(final OptionContract contract) {

        LocalDate expirationDate = contract.getExpirationDate();
        Series s = series.get(expirationDate);
        if (s == null){
            LOG.debug("add series " + contract.getExpirationDate());
            s = new Series(contract.getExpiration());
            series.put(expirationDate, s);
        }
        LOG.trace("add " + contract.getOptionSymbolName());
        s.add(contract);
    }


    public SortedSet<LocalDate> getOptionExpirations(){
        return new TreeSet<>(series.keySet());
    }

    public List<Series> getAllOptionSeries() {
        final List<Series> all = new ArrayList<>();
        for (final LocalDate expiration : getOptionExpirations()) {
            Series s = series.get(expiration);
            all.add(s);
        }
        return all;
    }

    public Series getSeries(LocalDate expirationDate){
        return series.get(expirationDate);
    }

    public OptionSeries getOptionSeries(LocalDate expirationDate) {
        List<Strike> strikes = getChain().stream()
                .filter(strike -> strike.getExpirationDate().equals(expirationDate))
                .collect(Collectors.toList());
        return new OptionSeries(expirationDate, strikes);
    }


    public List<Strike> getChain() {
        throw new RuntimeException("getChain() is no longer supported; use getSeries(LocalDate)");
    }


    public String getSymbol() {
        return underlyingSymbol;
    }

    public void setSymbol(final String underlyingSymbol) {
        this.underlyingSymbol = underlyingSymbol;
    }

    public String getDescription() {
        return underlyingDescription;
    }

    public void setDescription(final String underlyingDescription) {
        this.underlyingDescription = underlyingDescription;
    }

    public double getBid() {
        return bid;
    }

    public void setBid(final double bid) {
        this.bid = bid;
    }

    public double getAsk() {
        return ask;
    }

    public void setAsk(final double ask) {
        this.ask = ask;
    }

    public String getBidAskSize() {
        return bidAskSize;
    }

    public void setBidAskSize(final String bidAskSize) {
        this.bidAskSize = bidAskSize;
    }

    public double getLast() {
        return last;
    }

    public void setLast(final double last) {
        this.last = last;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(final double open) {
        this.open = open;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(final double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(final double low) {
        this.low = low;
    }

    public double getClose() {
        return close;
    }

    public void setClose(final double close) {
        this.close = close;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(final double volume) {
        this.volume = volume;
    }

    public double getChange() {
        return change;
    }

    public void setChange(final double change) {
        this.change = change;
    }

    public boolean isRealTimeQuote() {
        return realTimeQuote;
    }

    public void setRealTimeQuote(final boolean realTimeQuote) {
        this.realTimeQuote = realTimeQuote;
    }

    public String getQuoteTime() {
        return quoteTime;
    }

    public void setQuoteTime(final String quoteTime) {
        this.quoteTime = quoteTime;
    }


    public String getExpirationType(LocalDate expirationDate) {
        return getChain().stream()
                .filter(strike -> strike.getExpirationDate().equals(expirationDate))
                .findFirst()
                .get()
                .getCallContract()
                .getExpirationType();
    }

    public void addChain(final List<Strike> anotherChain) {
        ArrayList<Strike> newChain = new ArrayList<>(getChain().size() + anotherChain.size());
        for (Strike strike : getChain()) {
            newChain.add(strike);
        }
        for (Strike strike : anotherChain) {
            newChain.add(strike);
            //expirations.add(strike.getExpirationDate());
        }
        //chain = newChain;
    }
}
