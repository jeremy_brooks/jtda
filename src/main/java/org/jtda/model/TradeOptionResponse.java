package org.jtda.model;

import generated.Order;

/**
 * Created by jeremy on 08-18-2018.
 */
public class TradeOptionResponse {
    private String orderString;
    private String errorMessage;
    private Order order;

    public String getOrderString() {
        return orderString;
    }

    public void setOrderString(final String orderString) {
        this.orderString = orderString;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(final Order order) {
        this.order = order;
    }

    public boolean hasOrder(){
        return order != null;
    }
}
