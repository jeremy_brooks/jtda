package org.jtda.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountBalance {
    private String accountName;
    private LocalDateTime timestamp;
    private BigDecimal netLiq;
    private BigDecimal available;
    private BigDecimal stockBuyingPower;
    private BigDecimal optionBuyingPower;
    private BigDecimal cash;
    private BigDecimal moneyMarket;
    private BigDecimal longStock;
    private BigDecimal shortStock;
    private BigDecimal longOption;
    private BigDecimal shortOption;
    private BigDecimal maintenanceRequirement;

    /**
     * Returns the percentage of the account allocated for options trading
     *
     * @return abs(1 - OBP/NetLiq)
     */
    public double optionsCapitalAllocation() {
        Objects.requireNonNull(netLiq);
        Objects.requireNonNull(optionBuyingPower);

        double unAllocatedRatio = optionBuyingPower.doubleValue() / netLiq.doubleValue();
        return Math.abs(1 - unAllocatedRatio);
    }

    public double stocksCapitalAllocation() {
        Objects.requireNonNull(netLiq);
        Objects.requireNonNull(stockBuyingPower);
        //doesn't handle short stock

        double unAllocatedRatio = stockBuyingPower.doubleValue() / netLiq.doubleValue();
        return Math.abs(1 - unAllocatedRatio);
    }

    /**
     * Computes the total value of cash, money market funds, available funds
     * and long stock minus short stock. Does NOT include long or short option values.
     *
     * @return
     */
    public BigDecimal cashStockSweep() {
        Objects.requireNonNull(cash);
        Objects.requireNonNull(moneyMarket);
        Objects.requireNonNull(available);
        Objects.requireNonNull(longStock);
        Objects.requireNonNull(shortStock);

        return cash.add(moneyMarket)
                .add(available)
                .add(longStock)
                .subtract(shortStock);
    }

    public String asCsv() {
        StringBuilder sb = new StringBuilder();
        appendColumn(sb, accountName);
        appendColumn(sb, timestamp.format(DateTimeFormatter.BASIC_ISO_DATE));
        appendColumn(sb, netLiq);
        appendColumn(sb, optionBuyingPower);
        appendColumn(sb, optionsCapitalAllocation());
        appendColumn(sb, stockBuyingPower);
        appendColumn(sb, stocksCapitalAllocation());
        appendColumn(sb, available);
        appendColumn(sb, cash);
        appendColumn(sb, moneyMarket);
        appendColumn(sb, shortOption);
        appendColumn(sb, longOption);
        appendColumn(sb, shortStock);
        appendColumn(sb, longStock);
        appendColumn(sb, maintenanceRequirement);
        return sb.toString();
    }

    private StringBuilder appendColumn(StringBuilder sb, String columnValue) {
        return sb.append(columnValue).append(",");
    }

    private StringBuilder appendColumn(StringBuilder sb, BigDecimal columnValue) {
        return sb.append(columnValue.toString()).append(",");
    }

    private StringBuilder appendColumn(StringBuilder sb, double columnValue) {
        return sb.append(columnValue).append(",");
    }

}
