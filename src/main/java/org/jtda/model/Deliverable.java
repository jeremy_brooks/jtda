package org.jtda.model;

/**
 * Created by jeremy on 06-09-2018.
 */
public final class Deliverable {
    private String symbol;
    private int shares;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(final String symbol) {
        this.symbol = symbol;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(final int shares) {
        this.shares = shares;
    }

    @Override
    public String toString() {
        return "Deliverable{" +
                "symbol='" + symbol + '\'' +
                ", shares=" + shares +
                '}';
    }
}
