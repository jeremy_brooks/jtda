package org.jtda.model;

import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.Objects;


/**
 * Builds a representation of an XML Document using XmlNodes
 */
public class XmlNodeBuilder {
    private XmlNode root;   //root of tree
    private XmlNode cnode;  //Current Node used in building

    /**
     * Gets the root of tree.
     *
     * @return root node
     */
    public XmlNode getRoot() {
        return root;
    }


    /**
     * Handler for XML.
     */
    class Handler extends DefaultHandler {


        public void characters(char[] ch, int start, int length)

        {
            cnode.sb.append(ch, start, length);
        }


        public void startElement(String namespaceURI, String localName,
                                 String qName, org.xml.sax.Attributes atts)
        {
            try {
                XmlNode newnode = new XmlNode(qName, atts);
                if (root == null) {
                    root = newnode;

                } else {
                    cnode.add(newnode);
                }
                cnode = newnode;

            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
        }

        public void endElement(String namespaceURI, String localName,
                               String qName)
        {
            try {
                cnode.setValue(cnode.sb.toString().trim());
                cnode.sb = null;
                if (cnode.getParent() != null) {
                    cnode = (XmlNode) cnode.getParent();
                }

            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
        }

    }

    /**
     * Assemble XmlNode tree from an XML Document stored as a File.
     *
     * @param file File containing XML Document
     */
    public XmlNodeBuilder(File file) {
        try {
            init(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Assemble XmlNode tree from an XML Document which was stored in a String
     *
     * @param str String containing XML Document to process
     */
    public XmlNodeBuilder(String str) {

        init(new ByteArrayInputStream(str.getBytes()));
    }

    /**
     * Assemble XmlNode tree from an XML Document stored in a byte Array.
     *
     * @param bytes Byte Array containing XML Document
     */
    public XmlNodeBuilder(byte[] bytes) {

        init(new ByteArrayInputStream(bytes));
    }

    /**
     * Assemble XmlNode tree from an XML Document accessed via an InputStream
     *
     * @param in InputStream from where the XML Document will be read.
     */
    public XmlNodeBuilder(InputStream in) {
        init(in);
    }

    private void init(InputStream in) {
        Objects.requireNonNull(in);

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            parser.parse(in, new Handler());

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
