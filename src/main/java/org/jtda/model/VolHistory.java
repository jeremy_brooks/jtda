package org.jtda.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.jtda.cmd.Frequency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VolHistory extends ArrayList<VolData> {
    private static final int MIN_DATA_POINTS = 4;
    private static final Logger LOG = LoggerFactory.getLogger(VolHistory.class);
    private static final NumberFormat VOL_NUMBER_FORMATTER = new DecimalFormat("#0.0000");
    public static final char COMMA_DELIMITER = ',';
    public static final char TAB_DELIMITER = '\t';
    private static final String RECORD_DELIMITER = System.getProperty("line.separator");
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
    private static final long serialVersionUID = 1L;
    private String symbol;
    private String error;
    private Frequency barFrequency;
    private String ivRankDebug;

    public VolHistory(String symbol) {
        super();
        this.symbol = symbol;
    }

    public VolHistory(String symbol, Frequency frequency) {
        super();
        this.symbol = symbol;
        this.barFrequency = frequency;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Frequency getBarFrequency() {
        return barFrequency;
    }

    public int ivRank() {
        float low, high, mostRecentIv;
        String lowDate = "", highDate = "";
        if (size() >= MIN_DATA_POINTS) {
            VolData mostRecent = get(size() -1);
            mostRecentIv = mostRecent.getVol();
            low = mostRecentIv;
            high = mostRecentIv;
            lowDate = mostRecent.getDateTimeUtc().format(DATE_FORMAT);
            highDate = lowDate;
        } else {
            LOG.error("need at least {} days of vol history, found {}, returning -1",
                    MIN_DATA_POINTS, size());
            return -1;
        }

        int dataPoint = MIN_DATA_POINTS - 1; //zero based
        while (dataPoint >= 0) {
            VolData bar = get(dataPoint--);
            float currentIv = bar.getVol();
            if (currentIv < low) {
                low = currentIv;
                lowDate = bar.getDateTimeUtc().format(DATE_FORMAT);
            } else if (currentIv > high) {
                high = currentIv;
                highDate = bar.getDateTimeUtc().format(DATE_FORMAT);
            }
        }
//        LOG.info("iv={}, low={} on {}, high={} on {}", 
//                mostRecentIv, low, lowDate, high, highDate);
        float ivRank = (mostRecentIv - low) / (high - low) * 100;
        LOG.info(String.format("iv=%f, low=%f on %s, high=%f on %s", mostRecentIv, low, lowDate, high, highDate));
        return Math.round(ivRank);
    }

    /**
     * Thinkscript to calculate
     * # calculate the IV percentile
     * # ---------------------------
     * # how many times over the past year, has IV been below the current IV
     * def counts_below = fold i = 1 to days_back + 1 with count = 0
     * do
     * if df1[0] > df1[i] then
     * count + 1
     * else
     * count;
     *
     * def iv_percentile = Round(counts_below / days_back * 100.0, 0);
     * plot IVs = df1 * 100;
     * IVs.AssignValueColor(if iv_rank > 50 then Color.GREEN else Color.RED);
     *
     *
     * AddLabel(yes, "IV Percentile: " + iv_percentile + "% of days (over the past year) were below the current IV",
     *  if iv_percentile > 50 then Color.GREEN else Color.RED);
     *
     * @return the percent of days (as int) over past year lower than current IV
     */
    public int ivPercentile(){
        int numDaysBelowCurrentIv = -1;
        int countsBelow = 0;
        float mostRecentIv;
        if (size() >= MIN_DATA_POINTS) {
            mostRecentIv = get(size() - 1).getVol();
        } else {
            LOG.error("need at least {} days of vol history, found {}, returning -1",
                    MIN_DATA_POINTS, size());
            return numDaysBelowCurrentIv;
        }
        int dataPoint = MIN_DATA_POINTS - 1; //zero based
        while (dataPoint >= 0) {
            VolData bar = get(dataPoint--);
            float pastIv = bar.getVol();
            if (pastIv < mostRecentIv) {
                countsBelow++;
            }
        }
        return (int) Math.round(countsBelow / (double)size() * 100.0);
    }

    public String format(String columnSuffix, char delimiter, boolean includeSymbolAsColumn) {
        StringBuilder sb = new StringBuilder();
//        if (!includeSymbolAsColumn) {
//            sb.append("Symbol: ").append(symbol);
//            sb.append(RECORD_DELIMITER);
//        }
        if (StringUtils.isNotEmpty(error)) {
            sb.append("Error: ").append(error);
            return sb.toString();
        }
        sb.append("Date").append(delimiter);
        //sb.append("DataTime").append(delimiter);
        if (includeSymbolAsColumn) {
            sb.append("Symbol").append(delimiter);
        }
        sb.append("Vol"+columnSuffix).append(delimiter)
                .append(RECORD_DELIMITER);
        for (VolData bar : this) {
            sb.append(bar.getDateTimeUtc().format(DATE_FORMAT)).append(delimiter);
            //sb.append(bar.getDateTimeUtc().format(TIME_FORMAT)).append(delimiter);
            if (includeSymbolAsColumn) sb.append(symbol).append(delimiter);
            sb.append(Float.isNaN(bar.getVol()) ? "" : bar.getVol()).append(delimiter);
            sb.append(RECORD_DELIMITER);
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        //sb.append("Symbol: "+symbol);
        if (StringUtils.isNotEmpty(error)) {
            sb.append("Error: ").append(error);
            return sb.toString();
        }
        sb.append("DateTime").append(TAB_DELIMITER)
                .append("Symbol").append(TAB_DELIMITER)
                .append("Vol")
                .append(RECORD_DELIMITER);
        for (VolData bar : this) {
            sb.append(bar.getDateTimeUtc().format(DATE_FORMAT)).append(TAB_DELIMITER);
            sb.append(bar.getDateTimeUtc().format(TIME_FORMAT)).append(TAB_DELIMITER);
            sb.append(symbol).append(TAB_DELIMITER);
            sb.append(bar.getVol());
            sb.append(RECORD_DELIMITER);
        }
        return sb.toString();
    }
}
