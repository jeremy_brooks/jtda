package org.jtda.model;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HistoricalTransactionImpl implements HistoricalTransaction {

    private ZonedDateTime executedDate;
    private ZonedDateTime orderDateTime;
    private List<Transaction> transactions = new ArrayList<>(4);

    public HistoricalTransactionImpl(ZonedDateTime executedDate, ZonedDateTime orderDateTime, List<Transaction> transactions) {
        super();
        this.executedDate = executedDate;
        this.orderDateTime = orderDateTime;
        this.transactions = transactions;
    }

    public HistoricalTransactionImpl(Transaction transaction) {
        super();
        this.transactions.add(transaction);
    }

    @Override
    public String getName() {
        if (transactions.isEmpty()) {
            return "<empty>";
        } else if (transactions.size() == 1) {
            Transaction transaction = transactions.get(0);
            return transaction.getDescription() + " " + transaction.getSymbol();
        } else {
            //This is where we can build the name of the spread
            //
            //    Vertical GILD 100 21 Oct 16 74/70 P
            //
            StringBuilder sb = new StringBuilder();
            for (Transaction t : getTransactions()) {
                sb.append(t.getDescription()).append(" / ");
            }
            sb.setLength(sb.length() - 3);
            return sb.toString();
        }
    }

    @Override
    public BigDecimal getNetValue() {
        BigDecimal net = new BigDecimal("0");
        for (Transaction t : getTransactions()) {
            net = net.add(BigDecimal.valueOf(t.getNetValue()));
        }
        return net;
    }

    @Override
    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void add(Transaction t) {
        transactions.add(t);
    }

}
