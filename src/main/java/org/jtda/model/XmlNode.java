package org.jtda.model;

import org.jtda.StrHelper;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Objects;


/**
 * Class used to represent XML Nodes
 * <p>
 * The XML Nodes may also be displayed in a JTree, as they extend DefaultMutableTreeNode
 * <p>
 * This has proved to be an efficient class for processing XML.
 */
@SuppressWarnings("JavaDoc")
public class XmlNode extends DefaultMutableTreeNode {


    /**
     * Returns parent of node (saves having to call getParent() and cast)
     *
     * @return
     */
    public XmlNode getMyParent() {
        return (XmlNode) this.getParent();
    }


    /**
     * Name of Node
     */
    private String name;


    /**
     * Value of Node
     */
    private String value;

    /**
     * XML Attributes
     */
    LinkedHashMap<String, Object> attributes = new LinkedHashMap<>();

    /**
     * StringBuffer to be used in building tree.
     */

    public StringBuffer sb = new StringBuffer();


    /**
     * Suffix used in the toString method
     * This is usefull for appending to the name of each node
     * when the node is viewed in a JTree.
     * For example,  if want a node with name "books" to to be displayed as "books-2"
     * then set suffix to be "-2"
     */
    public String SUFFIX;


    /**
     * Renames the node, to a different Name
     *
     * @param newname NewName of node
     */
    public void rename(String newname) {
        this.name = newname;
    }


    /**
     * Sorts the children on the current node into alphabetical order.
     */
    public void sortChildren() {
        if (children == null) {
            throw new RuntimeException("no children for Node " + this.getTreePath());
        }

        this.children.sort((arg0, arg1) -> {
            XmlNode a = (XmlNode) arg0;
            XmlNode b = (XmlNode) arg1;
            return a.toString().compareToIgnoreCase(b.toString());
        });

    }


    /**
     * TreePath is of the form  aaa,bbb,ccc
     * <p>
     * Nodes can be retrieved from the tree path.
     * = *
     *
     * @param typepath
     * @return
     */

    public static XmlNode getNodeForPath(XmlNode root, String typepath) {
        Objects.requireNonNull(typepath);
        String trimpath;
        {
            StringBuilder trimpathSB = new StringBuilder();
            String[] tks = StrHelper.StringTrimTokenizer(typepath, ",");
            for (int i = 0; i < tks.length; i++) {
                trimpathSB.append(tks[i].trim());
                if (i < tks.length - 1) {
                    trimpathSB.append(",");
                }//if
            }//for
            trimpath = trimpathSB.toString();
        }


        Objects.requireNonNull(typepath);
        ArrayList al = new ArrayList();

        XmlNode[] nodes = root.getNodeList();
        for (XmlNode node : nodes) {
            if (node.getShortTreePath().equalsIgnoreCase(trimpath)) {
                al.add(node);
            }
        }

        if (al.size() > 1) {
            StringBuilder sb = new StringBuilder(" duplicate nodes with path >>" + typepath + "<<");
            for (Object anAl : al) {
                sb.append(anAl).append("\n");
            }
            throw new RuntimeException();

        }

        if (al.isEmpty()) {
            return null;
        }

        return (XmlNode) al.get(0);

    }

    /**
     * Returns short tree path for a node
     * For example aaa,bbb,ccc
     *
     * @return
     */

    public String getShortTreePath() {
        TreeNode[] nodes = this.getPath();
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < nodes.length; i++) {
            XmlNode node = (XmlNode) nodes[i];
            sb.append(node.getName());
            if (i < nodes.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }


    /**
     * Returns the tree path for a node
     *
     * @return
     */

    public TreePath getTreePath() {
        return new TreePath(this.getPath());
    }


    /**
     * Returns the node, and all child nodes, sorted in PreOrder.
     *
     * @return
     */
    public XmlNode[] getNodeList() {
        return (XmlNode[]) this.toArray().toArray(new XmlNode[]{});
    }


    /**
     * Adds Nodes to an Array, is recursive, and keeps adding
     * the node, then all its children.
     */
    private static void _toArray(ArrayList al, XmlNode node) {
        al.add(node);
        for (int i = 0; i < node.getChildCount(); i++) {
            _toArray(al, (XmlNode) node.getChildAt(i));
        }//for
    }


    /**
     * @param name  Name of node
     * @param value Value of node
     */
    public XmlNode(String name, String value) {
        if (name == null) {
            throw new RuntimeException("Null Name");
        }

        this.name = name;
        this.value = value;
    }


    /**
     * Name of the XML Node
     *
     * @param name
     */
    public XmlNode(String name) {
        if (name == null) {
            throw new RuntimeException("Null Name");
        }
        this.name = name;
    }

    /**
     * Return depth of the node.
     */
    public int getDepth() {
        return this.getLevel();
    }


    /**
     * @param name Name of the node
     * @param atts Attributes for the node
     */
    public XmlNode(String name, org.xml.sax.Attributes atts) {
        if (name == null) {
            throw new RuntimeException("Null Name");
        }

        this.name = name;
        this.attributes.clear();

        for (int i = 0; i < atts.getLength(); i++) {
            String attname = atts.getQName(i);
            String attvalue = atts.getValue(i);
            this.attributes.put(attname, attvalue);

        }//for


    }

    /**
     * Gets the name of the Element.
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the value of the Element.
     *
     * @return
     */
    public String getValue() {
        return this.value;
    }


    /**
     * Sets the value for the Node.
     *
     * @param value Value for the node
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value for the Attribute.
     *
     * @param selected_attribute_name Name of attribute to get value for
     * @return
     */
    public String getAttributeValue(String selected_attribute_name) {
        return (String) this.attributes.get(selected_attribute_name);
    }


    /**
     * Gets an attribute value, throws a RuntimeException if that value does not exist.
     *
     * @param selected_attribute_name
     * @return
     */
    public String getAttributeValueNonNull(String selected_attribute_name) {
        String val = (String) this.attributes.get(selected_attribute_name);

        if (val == null) {
            throw new RuntimeException(new TreePath(this.getPath()) + "  cant find attribute with name " + selected_attribute_name);
        }

        return val;

    }

    /**
     * Sets the attribute value for the node
     *
     * @param attname Attribute name to set
     * @param attval  Attribute value to set.
     */
    public void setAttribute(String attname, String attval) {
        Objects.requireNonNull(attname);
        Objects.requireNonNull(attval);
        attributes.put(attname, attval);
    }

    /**
     * Deletes attribute with a given name
     *
     * @param name
     */
    public void deleteAttribute(String name) {
        Objects.requireNonNull(name);
        //Undo the changes to converting to Tree/SortedMap
        //or fix these errors so they work with TreeMap
        attributes.remove(name);
    }


    /**
     * Attribute count
     *
     * @return Count of Attributes
     */
    public int getAttributeCount() {
        return attributes.size();
    }


//    /**
//     * Gets attribute at Index
//     *
//     * @param index Zero based index.
//     * @return
//     */
//    public String getAttributeName(int index) {
//        return (String) this.attributes.getKey(index);
//    }
//
//    public String getAttributeValue(int index) {
//        return (String) this.attributes.getValue(index);
//    }


    /**
     * Returns first child with the given name.
     * Throws an Exception, if cant find child with that name.
     *
     * @param name Name of child to return
     * @return 1st child with given name
     */
    public XmlNode getChildWithNameNonNull(String name) {
        XmlNode node = getChildWithName(name);

        if (node == null) {
            throw new RuntimeException("Cant find child with name " + name);
        }

        return node;

    }

    /**
     * gets the first child with Name
     *
     * @param name Name of Child to search for.
     * @return
     */
    public XmlNode getChildWithName(String name) {
        for (int i = 0; i < this.getChildCount(); i++) {
            XmlNode tempnode = (XmlNode) this.getChildAt(i);
            if (tempnode.getName().equals(name)) {
                return tempnode;
            }
        }//while
        return null;
    }//


    /**
     * Returns all the children with the given name
     *
     * @param name Name of child.
     * @return Array of Nodes where child has given name.
     */
    public XmlNode[] getChildrenWithName(String name) {
        ArrayList al = new ArrayList();
        for (int i = 0; i < this.getChildCount(); i++) {
            XmlNode tempnode = (XmlNode) this.getChildAt(i);
            if (tempnode.getName().equals(name)) {
                al.add(tempnode);
            }//if
        }//while

        return (XmlNode[]) al.toArray(new XmlNode[al.size()]);
    }//


    /**
     * Adds an XML Node to the parent.
     *
     * @param newChild
     */
    public void add(XmlNode newChild) {
        super.add(newChild);
    }


    /**
     * Adds node so that it is in alphabetical order with respect to its siblings.
     *
     * @param newChild
     */
    public void addNodeSorted(XmlNode newChild) {

        int i = 0;
        for (; i < this.getChildCount(); i++) {

            String name = "" + ((XmlNode) this.getChildAt(i)).getAttributeValue("name");
            String childname = "" + newChild.getAttributeValue("name");


            if (name.compareToIgnoreCase(childname) > 0) {
                break;
            }//if
        }//for

        super.insert(newChild, i);

    }


    /**
     * Creates a node with a given tree path
     * Path is relative to the root.
     * <p>
     * * Path is of the form aaa,bbb,ccc
     * <p>
     * <p>
     * If node exists, throw an exception
     *
     * @param root
     * @param path Path to create node
     * @return XmlNode, which was created at path
     */
    public static XmlNode createNewNodeWithPath(XmlNode root, String path) {

        {
            XmlNode existing = XmlNode.getNodeForPath(root, path);
            if (existing != null) {
                return existing;
//        throw new RuntimeException("we already have node with path>>"+path+"<<");
            }//if
        }
        return XmlNode.createNode(root, path);

    }


    /**
     * Path is of the form aaa,bbb,ccc
     * <p>
     * <p>
     * Creates a node, at the given path.
     *
     * @param root Root of XML Tree
     * @param path Path to create a node at.
     */
    private static XmlNode createNode(XmlNode root, String path) {

        String[] tks = StrHelper.StringTrimTokenizer(path.trim(), ",");
        XmlNode tnode = root;

        for (String tk : tks) {
            XmlNode childnode = tnode.getChildWithName(tk);
            if (childnode == null) {
                childnode = new XmlNode(tk, "");
                tnode.add(childnode);
            }
            tnode = childnode;
        }//for


        return tnode;

    }


    public String toString() {
        String append = StrHelper.nonNull(SUFFIX);

        String xname = this.getAttributeValue("name");
        if (xname != null) {
            return xname + append;
        } else {
            return name + append;
        }

    }

    /**
     * Returns set of XMLNodes as an ArrayList
     * Adds all Child nodes including himself.
     *
     * @return
     */
    public ArrayList toArray() {
        ArrayList al = new ArrayList();
        _toArray(al, this);
        return al;
    }


    /**
     * Sets the name of the node.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }


}//ElementTreeNode
