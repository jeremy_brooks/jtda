package org.jtda.model;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Option {
    private static final DateTimeFormatter EXPIRY_FORMAT = DateTimeFormatter.ofPattern("MM-dd-YYYY");
    private static final DecimalFormat STRIKE_FORMAT = new DecimalFormat("#.#");

    public enum Type {
        CALL,
        PUT
    }

    String underlying;
    LocalDate expirationDate;
    double strike;
    Type right;

    public Option(String underlying, LocalDate expirationDate, double strike, Type right) {
        super();
        this.underlying = underlying;
        this.expirationDate = expirationDate;
        this.strike = strike;
        this.right = right;
    }

    public static Option parse(String str) {
        String[] parts = str.split("_");
        if (parts.length != 2) {
            throw new IllegalArgumentException("option must have _ character");
        }
        String underlying = parts[0];
        String date = parts[1].substring(0, 6);
        char typeCh = parts[1].charAt(6);
        Type type = Type.CALL;
        if (typeCh == 'C') {
            type = Type.CALL;
        } else if (typeCh == 'P') {
            type = Type.PUT;
        } else {
            throw new IllegalArgumentException("must be C or P preceding strike price");
        }
        double strike = Double.parseDouble(parts[1].substring(7));
        return Option.builder()
                .underlying(underlying)
                .expirationDate(LocalDate.parse(date, DateTimeFormatter.ofPattern("MMddyy")))
                .strike(strike)
                .right(type)
                .build();
    }

    @Override
    public String toString() {
        String sb = underlying + " " +
                EXPIRY_FORMAT.format(expirationDate) + " " +
                STRIKE_FORMAT.format(strike) + " " +
                right;
        return sb;
    }
}
