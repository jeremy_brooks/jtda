package org.jtda.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

/**
 * Created by jeremy on 06-09-2018.
 */
public final class OptionContract implements Comparable<OptionContract>
{
    private int expiration; //in YYYYMMDD format
    private String expirationType; // R or L (regular or leap)
    private int strikePriceCents; //strike price in cents
    private boolean isStandardOption;
    private char right; //P or C, put/call
    private String optionSymbolName;
    private String optionDescription;
    private OptionQuote quote;

    public OptionContract() {
    }

    /**
     * Construct a null opposing/matching option contract.
     * If we are given a call, construct a put. If we are given a put construct a call.
     * The constructed option contract's getOptionSymbolName() will return null and getOptionDescription()
     * will also indicate a null contract.
     * @param opposingContract the opposing contract to create a null option contract
     */
    public OptionContract(OptionContract opposingContract) {
        //construct an opposing contract of the same strike and expiration
        setExpiration(opposingContract.getExpiration());
        setExpirationType(opposingContract.getExpirationType());
        setStrike(opposingContract.getStrike());
        setRight(opposingContract.getRight() == 'C' ? 'P' : 'C');
        setOptionSymbolName(null);
        setOptionDescription("" + getExpirationDate() + "-" + getStrikePrice() + getRight() + " is a null option");
    }

    public LocalDate getExpirationDate() {
        return LocalDate.parse(""+ expiration, DateTimeFormatter.BASIC_ISO_DATE);
    }

    public void setExpiration(final String yyyymmdd) {
        this.expiration = Integer.parseInt(yyyymmdd.replace("-", ""));
        //System.out.println(yyyymmdd + " becomes " + expiration);
    }

    public void setExpiration(final int yyyymmdd){
        this.expiration = yyyymmdd;
    }

    public int getExpiration(){
        return expiration;
    }

    public String getExpirationType() {
        return expirationType;
    }

    public void setExpirationType(final String expirationType) {
        this.expirationType = expirationType;
    }

    public BigDecimal getStrikePrice() {
        Double dollars = strikePriceCents / 100d;
        return BigDecimal.valueOf(dollars);
    }


    public int getStrike(){
        return strikePriceCents;
    }

    public void setStrike(final int strikeCents){
        this.strikePriceCents = strikeCents;
    }

    public void setStrike(final double strike){
        Double cents = strike * 100;
        this.strikePriceCents = cents.intValue();
    }

    public boolean isStandardOption() {
        return isStandardOption;
    }

    public void setStandardOption(final boolean standardOption) {
        isStandardOption = standardOption;
    }

    public char getRight() {
        return right;
    }

    public void setRight(final char right) {
        this.right = right;
    }

    public String getOptionSymbolName() {
        return optionSymbolName;
    }

    public void setOptionSymbolName(final String optionSymbolName) {
        this.optionSymbolName = optionSymbolName;
    }

    public String getOptionDescription() {
        return optionDescription;
    }

    public void setOptionDescription(final String optionDescription) {
        this.optionDescription = optionDescription;
    }

    public OptionQuote getQuote() {
        return quote;
    }

    public void setQuote(final OptionQuote quote) {
        this.quote = quote;
    }


    @Override
    public String toString() {
        return "OptionContract{" +
                expiration + "_" + strikePriceCents + "_" + right +
                ", optionSymbolName='" + optionSymbolName + '\'' +
                ", optionDescription='" + optionDescription + '\'' +
                ", expirationType='" + expirationType + '\'' +
                ", isStandardOption=" + isStandardOption +
                ", optionQuote='" + quote + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        final OptionContract that = (OptionContract) o;

        return new EqualsBuilder()
                .append(isStandardOption(), that.isStandardOption())
                .append(getRight(), that.getRight())
                .append(getExpirationDate(), that.getExpirationDate())
                .append(getExpirationType(), that.getExpirationType())
                .append(getStrikePrice(), that.getStrikePrice())
                .append(getOptionSymbolName(), that.getOptionSymbolName())
                .append(getOptionDescription(), that.getOptionDescription())
                .append(getQuote(), that.getQuote())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getExpirationDate())
                .append(getExpirationType())
                .append(getStrikePrice())
                .append(isStandardOption())
                .append(getRight())
                .append(getOptionSymbolName())
                .append(getOptionDescription())
                .append(getQuote())
                .toHashCode();
    }

    @Override
    public int compareTo(final OptionContract o) {
        final Comparator<OptionContract> comparator = new ExpirationStrikeRightComparator();
        return comparator.compare(this, o);
    }


    public static class ExpirationStrikeRightComparator implements Comparator<OptionContract> {

        @Override
        public int compare(OptionContract a, OptionContract b) {
            return Comparator.comparing(OptionContract::getExpirationDate)
                    .thenComparing(OptionContract::getStrikePrice)
                    .thenComparing(OptionContract::getRight)
                    .compare(a, b);
        }
    }

}
