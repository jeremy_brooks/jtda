package org.jtda.model;

import java.time.ZonedDateTime;

public interface VolData {
    Float getVol();
    ZonedDateTime getDateTimeUtc();
}
