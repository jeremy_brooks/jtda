package org.jtda.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by jeremy on 06-11-2018.
 */
public class OptionQuote {
    private double bid;
    private double ask;
    private String bidAskSize;
    private double last;
    private String lastTradeSize;
    private String lastTradeDate;
    private long volume;
    private int openInterest;
    private boolean realTimeQuote;
    private String underlyingSymbol;
    private double delta;
    private double gamma;
    private double theta;
    private double vega;
    private double rho;
    private double impliedVolatility;
    private double timeValueIndex;
    private double multiplier;
    private double change;
    private double changePercentage;
    private boolean inTheMoney;
    private boolean nearTheMoney;
    private double theoValue;
    private String deliverableNote;
    private double cashInLieuDollarAmount;
    private double opCashDollarAmount; //OP = ?
    private boolean indexOption;
    private List<Deliverable> deliverables;

    public OptionQuote() {
    }

    public OptionQuote(final double bid, final double ask) {
        this.bid = bid;
        this.ask = ask;
    }

    public double mid(){
        return (bid + ask) / 2.0d;
    }


    public double getBid() {
        return bid;
    }

    public void setBid(final double bid) {
        this.bid = bid;
    }

    public double getAsk() {
        return ask;
    }

    public void setAsk(final double ask) {
        this.ask = ask;
    }

    public String getBidAskSize() {
        return bidAskSize;
    }

    public void setBidAskSize(final String bidAskSize) {
        this.bidAskSize = bidAskSize;
    }

    public double getLast() {
        return last;
    }

    public void setLast(final double last) {
        this.last = last;
    }

    public String getLastTradeSize() {
        return lastTradeSize;
    }

    public void setLastTradeSize(final String lastTradeSize) {
        this.lastTradeSize = lastTradeSize;
    }

    public String getLastTradeDate() {
        return lastTradeDate;
    }

    public void setLastTradeDate(final String lastTradeDate) {
        this.lastTradeDate = lastTradeDate;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(final long volume) {
        this.volume = volume;
    }

    public int getOpenInterest() {
        return openInterest;
    }

    public void setOpenInterest(final int openInterest) {
        this.openInterest = openInterest;
    }

    public boolean isRealTimeQuote() {
        return realTimeQuote;
    }

    public void setRealTimeQuote(final boolean realTimeQuote) {
        this.realTimeQuote = realTimeQuote;
    }

    public String getUnderlyingSymbol() {
        return underlyingSymbol;
    }

    public void setUnderlyingSymbol(final String underlyingSymbol) {
        this.underlyingSymbol = underlyingSymbol;
    }

    public double getDelta() {
        return delta;
    }

    public void setDelta(final double delta) {
        this.delta = delta;
    }

    public double getGamma() {
        return gamma;
    }

    public void setGamma(final double gamma) {
        this.gamma = gamma;
    }

    public double getTheta() {
        return theta;
    }

    public void setTheta(final double theta) {
        this.theta = theta;
    }

    public double getVega() {
        return vega;
    }

    public void setVega(final double vega) {
        this.vega = vega;
    }

    public double getRho() {
        return rho;
    }

    public void setRho(final double rho) {
        this.rho = rho;
    }

    public double getImpliedVolatility() {
        return impliedVolatility;
    }

    public void setImpliedVolatility(final double impliedVolatility) {
        this.impliedVolatility = impliedVolatility;
    }

    public double getTimeValueIndex() {
        return timeValueIndex;
    }

    public void setTimeValueIndex(final double timeValueIndex) {
        this.timeValueIndex = timeValueIndex;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(final double multiplier) {
        this.multiplier = multiplier;
    }

    public double getChange() {
        return change;
    }

    public void setChange(final double change) {
        this.change = change;
    }

    public double getChangePercentage() {
        return changePercentage;
    }

    public void setChangePercentage(final double changePercentage) {
        this.changePercentage = changePercentage;
    }

    public boolean isInTheMoney() {
        return inTheMoney;
    }

    public void setInTheMoney(final boolean inTheMoney) {
        this.inTheMoney = inTheMoney;
    }

    public boolean isNearTheMoney() {
        return nearTheMoney;
    }

    public void setNearTheMoney(final boolean nearTheMoney) {
        this.nearTheMoney = nearTheMoney;
    }

    public double getTheoValue() {
        return theoValue;
    }

    public void setTheoValue(final double theoValue) {
        this.theoValue = theoValue;
    }

    public String getDeliverableNote() {
        return deliverableNote;
    }

    public void setDeliverableNote(final String deliverableNote) {
        this.deliverableNote = deliverableNote;
    }

    public double getCashInLieuDollarAmount() {
        return cashInLieuDollarAmount;
    }

    public void setCashInLieuDollarAmount(final double cashInLieuDollarAmount) {
        this.cashInLieuDollarAmount = cashInLieuDollarAmount;
    }

    public double getOpCashDollarAmount() {
        return opCashDollarAmount;
    }

    public void setOpCashDollarAmount(final double opCashDollarAmount) {
        this.opCashDollarAmount = opCashDollarAmount;
    }

    public boolean isIndexOption() {
        return indexOption;
    }

    public void setIndexOption(final boolean indexOption) {
        this.indexOption = indexOption;
    }

    public List<Deliverable> getDeliverables() {
        return deliverables;
    }

    public void setDeliverables(final List<Deliverable> deliverables) {
        this.deliverables = deliverables;
    }

    public void addDeliverable(Deliverable deliverable) {
        if (Objects.isNull(deliverables)) {
            deliverables = new LinkedList<>();
        }
        deliverables.add(deliverable);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("bid", bid)
                .append("ask", ask)
                .append("bidAskSize", bidAskSize)
                .append("last", last)
                .append("lastTradeSize", lastTradeSize)
                .append("lastTradeDate", lastTradeDate)
                .append("volume", volume)
                .append("openInterest", openInterest)
                .append("realTimeQuote", realTimeQuote)
                .append("underlyingSymbol", underlyingSymbol)
                .append("delta", delta)
                .append("gamma", gamma)
                .append("theta", theta)
                .append("vega", vega)
                .append("rho", rho)
                .append("impliedVolatility", impliedVolatility)
                .append("timeValueIndex", timeValueIndex)
                .append("multiplier", multiplier)
                .append("change", change)
                .append("changePercentage", changePercentage)
                .append("inTheMoney", inTheMoney)
                .append("nearTheMoney", nearTheMoney)
                .append("theoValue", theoValue)
                .append("deliverableNote", deliverableNote)
                .append("cashInLieuDollarAmount", cashInLieuDollarAmount)
                .append("opCashDollarAmount", opCashDollarAmount)
                .append("indexOption", indexOption)
                .append("deliverables", deliverables)
                .toString();
    }
}
