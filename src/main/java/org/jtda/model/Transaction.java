package org.jtda.model;

import java.time.ZonedDateTime;

/**
 * A simple transaction from a broker.
 * <p>
 * Transactions are the smallest element of a trade. For instance
 * multiple transactions compose a complex option trade such as
 * an iron condor with 4 legs (ie, 4 individual option transactions).
 * <p>
 * Other examples of transactions include
 * the assignment of an underlying due to option assignment
 * an exercise of an option
 * a paid dividend
 * short sale of stock
 * and so on.
 *
 * @author jeremy
 */
public interface Transaction {

    String getAccountNumber();

    ZonedDateTime getExecutedDate();

    String getType();

    String getOrderNumber();

    String getSymbol();

    String getDescription();

    double getQuantity();

    double getPrice();

    double getCommission();

    double getFees();

    double getNetValue();

}
