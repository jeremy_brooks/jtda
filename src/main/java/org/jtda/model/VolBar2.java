package org.jtda.model;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * Created by jeremy on 6/4/18.
 */
public class VolBar2 implements VolData {
    private final long timestamp;
    private final float volatility;

    public VolBar2(long timestamp, float volatility) {
        this.timestamp = timestamp;
        this.volatility = volatility;
    }

    @Override
    public String toString() {
        return "VolBar2{" +
                "timestamp=" + timestamp +
                ", volatility=" + volatility +
                '}';
    }

    @Override
    public Float getVol() {
        return volatility;
    }

    @Override
    public ZonedDateTime getDateTimeUtc() {
        return Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.UTC);
    }
}
