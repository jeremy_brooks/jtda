package org.jtda.model;

import java.math.BigDecimal;
import java.util.Comparator;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class StrikeOld {
    private BigDecimal price;
    private double callPrice;
    private double putPrice;
    
    public double diff(){
        return Math.abs(putPrice - callPrice);
    }

    public static class CallPutDifferenceComparator implements Comparator<StrikeOld> {
        @Override
        public int compare(StrikeOld a, StrikeOld b) {
            return a.diff() < b.diff() ? -1 : a.diff() == b.diff() ? 0 : 1;
        }   
    }

    @Override
    public String toString() {
        return "StrikeOld{" +
                "price=" + price +
                ", callPrice=" + callPrice +
                ", putPrice=" + putPrice +
                ", diff()=" + diff() +
                '}';
    }
}
