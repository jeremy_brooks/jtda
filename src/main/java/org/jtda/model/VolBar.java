package org.jtda.model;

import java.time.ZonedDateTime;

public class VolBar implements VolData {

    private final Float vol;
    private final ZonedDateTime dateTimeUtc;

    public VolBar(Float volatility, ZonedDateTime dateTimeUtc) {
        super();
        this.vol = volatility;
        this.dateTimeUtc = dateTimeUtc;
    }

    public Float getVol() {
        return vol;
    }

    public ZonedDateTime getDateTimeUtc() {
        return dateTimeUtc;
    }

    @Override
    public String toString() {
        return "VolBar{" +
                "dateTimeUtc=" + dateTimeUtc +
                ", vol=" + vol +
                '}';
    }
}
