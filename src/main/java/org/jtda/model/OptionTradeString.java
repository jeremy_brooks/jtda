package org.jtda.model;


import java.util.Locale;

public class OptionTradeString {

    private static final String DELIMITER = "~";

    public enum Action {
        BUY_TO_OPEN,
        BUY_TO_CLOSE,
        SELL_TO_OPEN,
        SELL_TO_CLOSE;

        @Override
        public String toString() {
            //convert to TDA API string value
            switch (this) {
                case BUY_TO_OPEN:
                    return "buytoopen";
                case BUY_TO_CLOSE:
                    return "buytoclose";
                case SELL_TO_OPEN:
                    return "selltoopen";
                case SELL_TO_CLOSE:
                    return "selltoclose";
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    public enum TimeInForce {
        DAY,
        GTC;

        @Override
        public String toString() {
            //convert to TDA API string value
            switch (this) {
                case DAY:
                    return "day";
//                case MOC:          return "moc";
//                case DAY_EXT:      return "day_ext";
                case GTC:
                    return "gtc";
//                case GTC_EXT:      return "gtc_ext";
//                case AM:           return "am";
//                case PM:           return "pm";
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    public enum OrderType {
        MARKET,
        LIMIT,
        STOP_MARKET,
        STOP_LIMIT,
        TRAILING_STOP_PERCENT,
        TRAILING_STOP_DOLLAR;


        @Override
        public String toString() {
            //convert to TDA API string value
            switch (this) {
                case MARKET:
                    return "market";
                case LIMIT:
                    return "limit";
                case STOP_MARKET:
                    return "stop_market";
                case STOP_LIMIT:
                    return "stop_limit";
                case TRAILING_STOP_PERCENT:
                    return "tstoppercent";
                case TRAILING_STOP_DOLLAR:
                    return "tstopdollar";
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    private final Long accountId;
    private final Action action;
    private final TimeInForce timeInForce;
    private final OrderType orderType;
    private final String routing;
    private final int quantity;
    private final String symbol;

    private OptionTradeString(Builder builder) {
        this.accountId = builder.accountId;
        this.action = builder.action;
        this.timeInForce = builder.timeInForce;
        this.orderType = builder.orderType;
        this.routing = builder.routing;
        this.quantity = builder.quantity;
        this.symbol = builder.symbol;
    }

    @Override
    public String toString() {
        return "accountId=" + accountId + DELIMITER
                + "action=" + action + DELIMITER
                + "quantity=" + quantity + DELIMITER
                + "symbol=" + symbol.toUpperCase(Locale.ENGLISH) + DELIMITER
                + "expire=" + timeInForce + DELIMITER
                + "ordType=" + orderType + DELIMITER
                + "routing=" + routing;
    }

    public static Builder newOptionTradeStringBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private Long accountId;
        private Action action;
        private TimeInForce timeInForce;
        private OrderType orderType;
        @SuppressWarnings("CanBeFinal")
        private String routing = "auto";
        private int quantity;
        private String symbol;

        private Builder() {
        }

        public OptionTradeString build() {
            return new OptionTradeString(this);
        }

        public Builder accountId(final Long accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder action(final Action action) {
            this.action = action;
            return this;
        }

        public Builder timeInForce(final TimeInForce timeInForce) {
            this.timeInForce = timeInForce;
            return this;
        }

        public Builder orderType(final OrderType orderType) {
            this.orderType = orderType;
            return this;
        }

        public Builder quantity(final int quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder symbol(final String symbol) {
            this.symbol = symbol;
            return this;
        }
    }
}
