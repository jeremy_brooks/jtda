package org.jtda.model;

public enum TransactionType {
    BUY,                //TR-BY
    SELL,               //TR-SL
    CLOSE_SHORT,        //TR-CS
    SHORT_SALE,         //TR-SS
    DIV_REINVEST,       //TR-DR
    ASSIGNED,           //TR-OA
    EXERCISED,          //TR-OE
    EXPIRED,            //RD-OE
    QUALIFIED_DIVIDEND, //DV-QF
    UNKNOWN;            //Jtda doesn't support this type yet

    public static TransactionType parse(String type, String subType) {
        if ("TR".equals(type)) {
            switch (subType) {
                case "BY":
                    return BUY;
                case "SL":
                    return SELL;
                case "CS":
                    return CLOSE_SHORT;
                case "SS":
                    return SHORT_SALE;
                case "DR":
                    return DIV_REINVEST;
                case "OA":
                    return ASSIGNED;
                case "OE":
                    return EXERCISED;
                default:
                    throw new IllegalStateException();
            }
        } else if ("RD".equals(type)) {
            switch (subType) {
                case "OX":
                    return EXPIRED;
                default:
                    throw new IllegalStateException();
            }
        } else if ("DV".equals(type)) {
            switch (subType) {
                case "QF":
                    return QUALIFIED_DIVIDEND;
                default:

                    return UNKNOWN;
                    //throw new IllegalStateException();
            }
        }
        //LOG.warn("Unsupported transaction type: {} {}", type, subType);
        return UNKNOWN;
    }
}
