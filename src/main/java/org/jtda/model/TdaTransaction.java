package org.jtda.model;

import java.time.ZonedDateTime;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Builder;
import lombok.Data;

/**
 * A TD Ameritrade transaction.
 *
 * @author jeremy
 */
@Data
@Builder
public class TdaTransaction implements Transaction, Comparable<Transaction> {
    private static final Logger LOG = LoggerFactory.getLogger(TdaTransaction.class);

    public enum AssetType {
        OPTION,
        STOCK,
        FUND,
        BOND,
        UNITS,
        RIGHTS_WARRANTS;

        public static AssetType parse(String assetType) {
            switch (assetType) {
                case "O":
                    return OPTION;
                case "S":
                    return STOCK;
                case "F":
                    return FUND;
                case "B":
                    return BOND;
                case "U":
                    return UNITS;
                case "W":
                    return RIGHTS_WARRANTS;
                default:
                    throw new IllegalArgumentException("unsupported asset type:" + assetType);
            }
        }
    }

    public enum OptionType {
        CALL,
        PUT;

        public static OptionType parse(generated.OptionType optionType) {
            switch (optionType) {
                case C:
                    return CALL;
                case P:
                    return PUT;
                default:
                    throw new IllegalArgumentException("unsupported optionType:" + optionType);
            }
        }

        public Option.Type asJtdaOption() {
            switch (this) {
                case CALL:
                    return Option.Type.CALL;
                case PUT:
                    return Option.Type.PUT;
                default:
                    return null;
            }
        }
    }

    public enum Effect {
        OPENING,
        CLOSING;

        public static Effect parse(String openClose) {
            switch (openClose) {
                case "O":
                    return OPENING;
                case "C":
                    return CLOSING;
                default:
                    throw new IllegalArgumentException("unsupported openCloseEffect:" + openClose);
            }
        }
    }

    private String accountNumber;
    private String orderNumber;
    private String groupId;
    private String leg;
    private ZonedDateTime orderDateTime;
    private ZonedDateTime executedDate;
    private String type;
    private AssetType assetType;
    private String symbol;
    private String description;
    private String symbolDescription;
    private String cusip;
    private double price;
    private double quantity;
    private double value;
    private double commission;
    private double fees;
    private double additionalFees;
    private Effect effect;
    private Option option;
    private String optionUnderlyingCusip;
    private ZonedDateTime optionExpirationDate;
    private String optionStrike;
    private OptionType optionType;
    private double sharesBefore;
    private double sharesAfter;
    private String splitRatio;

//    @Override
//    public int compare(HistoricalTransactionOrderLeg o1, HistoricalTransactionOrderLeg o2) {
//        // TODO Auto-generated method stub
//        return o1.getExecutedDate().compareTo(o2.getExecutedDate());
//    }


    @Override
    public double getFees() {
        return fees + additionalFees;
    }

    @Override
    public int compareTo(Transaction o) {
        return new CompareToBuilder()
                .append(this.executedDate, o.getExecutedDate())
                .append(this.orderNumber, o.getOrderNumber())
                .toComparison();
    }

    @Override
    public double getNetValue() {
        return value;
    }

//    public static Comparator<Fruit> FruitNameComparator = new Comparator<Fruit>() {
//
//        public int compare(Fruit fruit1, Fruit fruit2) {
//
//            String fruitName1 = fruit1.getFruitName().toUpperCase();
//            String fruitName2 = fruit2.getFruitName().toUpperCase();
//
//            //ascending order
//            return fruitName1.compareTo(fruitName2);
//
//            //descending order
//            //return fruitName2.compareTo(fruitName1);
//        }
//
//    };

}

