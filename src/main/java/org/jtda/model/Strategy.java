package org.jtda.model;

import java.util.List;

public class Strategy {

    public static String generateSpreadName(List<TdaTransaction> xactions) {
        if (xactions.isEmpty()) {
            return "";
        }
        if (xactions.size() > 2) {
            throw new IllegalArgumentException("only 2 legs supported");
        }
        if (xactions.size() == 2) {
            TdaTransaction t1 = xactions.get(0);
            TdaTransaction t2 = xactions.get(1);
            boolean sameGroup = t1.getGroupId().equals(t2.getGroupId());
            boolean sameAssetType = t1.getAssetType().equals(t2.getAssetType());
            boolean sameUnderlying = t1.getOptionUnderlyingCusip().equals(t2.getOptionUnderlyingCusip());
            boolean sameExpiry = t1.getOption().getExpirationDate().equals(t2.getOption().getExpirationDate());
            boolean sameStrike = (0 == Double.compare(t1.getOption().getStrike(), t2.getOption().getStrike()));
            boolean sameType = t1.getOption().getRight().equals(t2.getOption().getRight());

            if (sameGroup) {
                if (sameAssetType) {
                    if (sameUnderlying) {
                        if (sameExpiry) {
                            if (!sameStrike)
                                if (sameType)
                                    return "Vertical";
                        }
                    }
                }
            }
        }
        return "Custom";
    }

}
