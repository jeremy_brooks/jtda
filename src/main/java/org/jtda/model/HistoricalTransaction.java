package org.jtda.model;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

public interface HistoricalTransaction {

    /**
     * The date/time of the transaction's execution.
     *
     * @return
     */
    ZonedDateTime getExecutedDate();

    /**
     * Represents the descriptive name of the transaction.
     * Can be things like:
     * an option spread     :  Vertical GILD 100 21 Oct 16 74/70 P
     * a stock sale/purchase:  AAPL
     * an expired option    :  FB Sep 16 2016 125.0 Put
     *
     * @return
     */
    String getName();

    /**
     * The aggregated net value (including commissions and all fees) of the transaction.
     * Positive values indicate a credit to the account and negative values are debits.
     *
     * @return a positive value when the transaction is a credit or negative if it's a debit
     */
    BigDecimal getNetValue();

    /**
     * Gets the transactions composing this aggregate transaction.
     * Every historical transaction has at least 1 and at most 4 underlying transactions.
     *
     * @return the list of transactions
     */
    List<Transaction> getTransactions();
}




