package org.jtda.model;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Created by jeremy on 06-15-2018.
 */
public class OptionSeries {
    private final LocalDate expirationDate;
    private final String expirationType;
    private final List<Strike> strikes;

    public OptionSeries(final LocalDate expirationDate, final List<Strike> strikes) {
        Objects.requireNonNull(strikes);
        Objects.requireNonNull(strikes.get(0));

        this.expirationDate = expirationDate;
        this.expirationType = strikes.get(0).getCallContract().getExpirationType();
        this.strikes = strikes;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public long getDaysToExpiration(){
        return DAYS.between(LocalDate.now(), expirationDate);
    }

    public String getExpirationType() {
        return expirationType;
    }

    public List<Strike> getStrikes() {
        return strikes;
    }

    public List<Strike> getStrikesBelow(double strikePrice){
        return strikes.stream()
                .filter(s -> s.getStrikePrice() < strikePrice)
                //descending strike price sort
                .sorted((o1, o2) -> Double.compare(o2.getStrikePrice(), o1.getStrikePrice()))
                .collect(Collectors.toList());
    }

    public List<Strike> getStrikesAbove(double strikePrice){
        return strikes.stream()
                .filter(s -> s.getStrikePrice() > strikePrice)
                //ascending strike price sort
                .sorted(Comparator.comparingDouble(Strike::getStrikePrice))
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final OptionSeries that = (OptionSeries) o;

        if (expirationDate != null ? !expirationDate.equals(that.expirationDate) : that.expirationDate != null)
            return false;
        return strikes != null ? strikes.equals(that.strikes) : that.strikes == null;
    }

    @Override
    public int hashCode() {
        int result = expirationDate != null ? expirationDate.hashCode() : 0;
        result = 31 * result + (strikes != null ? strikes.hashCode() : 0);
        return result;
    }
}
