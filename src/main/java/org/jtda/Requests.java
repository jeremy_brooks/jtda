package org.jtda;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.SortedMap;

public class Requests {


    public static File getLogDir() {

        return new File("c:\\temp\\");

    }

    public static String get(String url) throws IOException {
        URL u = new URL(url);
        return StrHelper.inputStreamToString(u.openStream());
    }

    public static String post(String url, SortedMap<String, String> params) throws IOException {
        URL u = new URL(url);
        URLConnection urlConn = u.openConnection();
        urlConn.setDoInput(true); // Let the run-time system (RTS) know that we want input.
        urlConn.setDoOutput(true); // Let the RTS know that we want to do output.
        urlConn.setUseCaches(false); // No caching, we want the real thing.
        urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");// Specify the content type.

        // Send POST output.
        DataOutputStream dos = new DataOutputStream(urlConn.getOutputStream());
        StringBuilder data = new StringBuilder();
        int i = 0;
        for(String key: params.keySet()){
            String value = params.get(key);
            String obscured = value;
            if("password".equals(key)) {
                obscured = "*******";
            }
            System.out.println("POST param: " + key + "=" + obscured);
            data.append(URLEncoder.encode(key, "UTF-8")).append("=").append(URLEncoder.encode(value, "UTF-8"));
            if (i++ < params.size() - 1) {
                data.append("&");
            }
        }
        dos.writeBytes(data.toString());
        dos.flush();
        dos.close();

        // Get response data.
        return StrHelper.inputStreamToString(urlConn.getInputStream());
    }


}
