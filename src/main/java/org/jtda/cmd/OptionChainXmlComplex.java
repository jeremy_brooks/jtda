package org.jtda.cmd;

import generated.Amtd;
import generated.ComplexOptionChainResults;
import generated.OptionRowList;
import generated.OptionStrategyList;
import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

/**
 * Examples:
 * occ -t VCS -e a FB       //list all call spreads for facebook
 * occ -t VPS -e a $RUT.X   //list all put spreads in RUT (Russel 2000 index)
 *
 * @author jeremy
 */
public class OptionChainXmlComplex extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(OptionChainXmlComplex.class);
    private static final String NAME = "OptionChain";
    private static final ResponseHandler<ComplexOptionChainResults> HANDLER = new ComplexOptionChainHandler();
    private Session session;
    private String symbols;
    private String type; //C, P, VCS, VPS, CCS, CPS, BW, STDL, STGL
    private String expire; //al, a, l, w
    private String interval; //interval between strikes

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "200");
            HttpUriRequest httpget = buildRequest(uri);
            LOG.info("" + httpget.getRequestLine());
            ComplexOptionChainResults ocr = httpclient.execute(httpget, HANDLER);
            System.out.println("Symbol   : " + ocr.getRequest().getSymbol());
            System.out.println("Strategy : " + ocr.getRequest().getStrategy());
            System.out.println("Expirations:");
            for (OptionStrategyList osl : ocr.getOptionStrategyList()) {
                System.out.println("near : " + osl.getExpirationMonth() + " " + osl.getExpirationYear());
                System.out.println("far  : " + osl.getFarExpirationMonth() + " " + osl.getFarExpirationYear());
                System.out.println("Desc\tStrike(s)\tBid\tAsk");
                for (OptionRowList orl : osl.getOptionRowList()) {
                    String desc = orl.getPrimaryLeg().getDescription();
                    String strike = orl.getStrike();
                    Double bid = Double.parseDouble(orl.getBid());
                    Double ask = Double.parseDouble(orl.getAsk());
                    Double mark = (bid + ask) / 2;
                    System.out.println(desc + "\t" + strike + "\t" +
                            bid + "\t" + ask + "\t" + mark);
                }
            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }


    private HttpUriRequest buildRequest(URI uri) {
        return RequestBuilder.get()
                .setUri(uri)
                .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                .addParameter("symbol", symbols)
                .addParameter("quotes", "true")
                .addParameter("type", type)
                .addParameter("expire", expire)
                .addParameter("interval", interval)
                //.addParameter("strike", "113")
                .build();
    }


    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        StringBuilder sb = new StringBuilder();
        for (String arg : args)
            sb.append(arg)
                    .append(" ");
        System.err.println("parsing: " + sb.toString());

        Options options = new Options();
        org.apache.commons.cli.Option typeValue = new org.apache.commons.cli.Option("t", true, "type");
        options.addOption(typeValue);
        org.apache.commons.cli.Option expireValue = new org.apache.commons.cli.Option("e", true, "expire");
        options.addOption(expireValue);
        org.apache.commons.cli.Option intervalValue = new org.apache.commons.cli.Option("i", true, "interval");
        options.addOption(intervalValue);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        if (cmdLine.hasOption("t")) {
            type = cmdLine.getOptionValue("t");
            if (type.equals("P") || type.equals("C")) {
                LOG.info("use optionchain (oc) command for just calls or puts");
                throw new IllegalArgumentException("types P,C are unsupported");
            }
        }
        if (cmdLine.hasOption("e")) {
            expire = cmdLine.getOptionValue("e");
        }
        if (cmdLine.hasOption("i")) {
            interval = cmdLine.getOptionValue("i");
        }

        if (cmdLine.getArgs().length < 1) {
            throw new ParseException("symbol(s) not found");
        }
        symbols = cmdLine.getArgs()[0].toUpperCase(); //first argument
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

    private static class ComplexOptionChainHandler implements ResponseHandler<ComplexOptionChainResults> {

        @Override
        public ComplexOptionChainResults handleResponse(HttpResponse response)
                throws IOException {
            String xml = EntityUtils.toString(response.getEntity());
            Amtd r = XmlUtil.unmarshall(xml);
            if ("OK".equals(r.getResult())) {
                return r.getComplexOptionChainResults();
            } else {
                String msg = r.getResult() + ": " + r.getError();
                throw new IOException(msg);
            }
        }
    }

}
