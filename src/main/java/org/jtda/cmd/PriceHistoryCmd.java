package org.jtda.cmd;

import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.PriceHistoryHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Get as many one minute bars as we can ():
 * ph -f 1min -e 20181224 -s 20170901 aapl
 *
 * Assuming we ran this on 12-24-2018, TDA seems to allow for fetching the most
 * recent 15 months of 1-minute data, which is the most granular historical data they offer.
 *
 * https://apis.tdameritrade.com/apps/100/PriceHistory?source=<##SourceID#>
 * &startdate=&enddate=&requestvalue=&intervaltype=&periodtype=
 * &extended=&intervalduration=&period=&requestidentifiertype=
 */
public class PriceHistoryCmd extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(PriceHistoryCmd.class);
    private static final String NAME = "PriceHistory";
    private static final ResponseHandler<List<PriceHistory>> HANDLER = new PriceHistoryHandler();
    private static final String SYMBOL_DELIMITER = ", ";
    private static final String outDir = System.getProperty("outputDir", USER_DATA_DIR);
    private static final Character DATA_FILE_COMPONENT_DELIMITER = '_';
    private Session session;
    private String symbols;
    private Frequency freq = new Frequency(1, TdaInterval.DAILY);
    private LocalDate startDate;
    private LocalDate endDate; //default is previous business day
    private File outputDirectory;
    private final Map<String, Path> symbolFileMap = new HashMap<>();

    @Override
    public void execute() throws CommandException {
        if (session.getConfiguration() == null) {
            throw new CommandException("must login first");
        }
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .addParameter("requestidentifiertype", "SYMBOL")
                    .addParameter("requestvalue", symbols.replace(",", SYMBOL_DELIMITER))
                    .addParameter("intervalduration", "" + freq.getValue())
                    .addParameter("intervaltype", freq.getType().toString())
                    .addParameter("startdate", startDate == null ? "" : startDate.format(DateTimeFormatter.BASIC_ISO_DATE))
                    .addParameter("enddate", endDate == null ? "" : endDate.format(DateTimeFormatter.BASIC_ISO_DATE))
                    .build();
            LOG.info("" + httpget.getRequestLine());
            List<PriceHistory> histories = httpclient.execute(httpget, HANDLER);
            for (PriceHistory ph : histories) {
                if (outputDirectory != null) {
                    Path outfile = symbolFileMap.get(ph.getSymbol());
                    Files.write(outfile, ph.format(PriceHistory.COMMA_DELIMITER, false).getBytes());
                    System.out.println("wrote " + outfile.toString());
                } else {
                    System.out.println(ph.format(PriceHistory.TAB_DELIMITER, false));
                }
            }
        } catch (ClientProtocolException e) {
            LOG.error(e.getMessage());
        } catch (IOException e) {
            LOG.error(e.getMessage());
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        Options options = new Options();
        Option intDuration = new Option("f", true, "frequency of bars");
        options.addOption(intDuration);
        Option startDateOption = new Option("s", true, "start date");
        options.addOption(startDateOption);
        Option endDateOption = new Option("e", true, "end date");
        options.addOption(endDateOption);
        Option outfileOption = new Option("o", "outputdirectory", false, "save output files to directory");
        options.addOption(outfileOption);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        if (cmdLine.hasOption("f")) {
            freq = Frequency.parse(cmdLine.getOptionValue("f"));
        }
        if (cmdLine.hasOption("s")) {
            String date = cmdLine.getOptionValue("s");
            startDate = LocalDate.parse(date, DateTimeFormatter.BASIC_ISO_DATE);

        }
        if (cmdLine.hasOption("e")) {
            String date = cmdLine.getOptionValue("e");
            endDate = LocalDate.parse(date, DateTimeFormatter.BASIC_ISO_DATE);
        }
        //TODO: extended
        if (cmdLine.getArgs().length < 1) {
            throw new ParseException("symbol(s) not found");
        }
        symbols = cmdLine.getArgs()[0].toUpperCase(); //first argument

        if (cmdLine.hasOption("o")) {
            if (endDate == null) {
                throw new ParseException("-o requires -eYYYYMMDD");
            }
            if (startDate == null) {
                throw new ParseException("-o requires -sYYYYMMDD");
            }
            String fileSuffix = endDate.format(DateTimeFormatter.BASIC_ISO_DATE) + DATA_FILE_COMPONENT_DELIMITER
                + startDate.format(DateTimeFormatter.BASIC_ISO_DATE) + DATA_FILE_COMPONENT_DELIMITER
                + freq.toString() + DATA_FILE_COMPONENT_DELIMITER
                + "ohlc";
            outputDirectory = new File(outDir);
            if(!outputDirectory.exists()){
                try {
                    FileUtils.forceMkdir(outputDirectory);
                } catch (IOException e) {
                    LOG.error("unable to create " + outputDirectory, e);
                }
            }
            for (String symbol : symbols.split(",")) {
                Path outfile = Paths.get(outputDirectory.toString(), symbol + DATA_FILE_COMPONENT_DELIMITER + fileSuffix + ".csv");
                try {
                    Files.createFile(outfile);
                    System.err.println(outfile.toAbsolutePath().toString());
                    symbolFileMap.put(symbol, outfile);
                } catch (FileAlreadyExistsException ignored) {
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

    }

}
