package org.jtda.cmd;

import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.VolHistoryHandler;
import org.jtda.model.VolHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * https://apis.tdameritrade.com/apps/100/PriceHistory?source=<##SourceID#>
 * &startdate=&enddate=&requestvalue=&intervaltype=&periodtype=
 * &extended=&intervalduration=&period=&requestidentifiertype=
 */
public class IvRank extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(IvRank.class);
    private static final String NAME = "VolatilityHistory";
    private static final String SYMBOL_DELIMITER = ", ";
    private static final String outDir = System.getProperty("outputDir", USER_DATA_DIR);
    private static final Character DATA_FILE_COMPONENT_DELIMITER = '_';
    private Session session;
    private String symbols;
    private Period period = new Period(1, TdaPeriod.YEAR);
    private Frequency freq = new Frequency(1, TdaInterval.DAILY);
    private LocalDate startDate;
    private LocalDate endDate; //default is previous business day
    private SurfaceType surfaceType = SurfaceType.builder()
            .id(SurfaceType.Id.DELTA)
            .value("-30")
            .build();
    private int daysToExpiration = 30; //tda's default
    private File outputDirectory;
    private final Map<String, Path> symbolFileMap = new HashMap<>();
    private String volColumnSuffix;

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .addParameter("requestidentifiertype", "SYMBOL")
                    .addParameter("requestvalue", symbols.replace(",", SYMBOL_DELIMITER))
                    .addParameter("volatilityhistorytype", "I") //I=implied or H=historical
                    .addParameter("period", "" + period.getValue())
                    .addParameter("periodtype", period.getPeriod().toString())
                    .addParameter("intervalduration", "" + freq.getValue())
                    .addParameter("intervaltype", freq.getType().toString())
                    .addParameter("startdate", startDate == null ? "" : startDate.format(DateTimeFormatter.BASIC_ISO_DATE))
                    .addParameter("enddate", endDate == null ? "" : endDate.format(DateTimeFormatter.BASIC_ISO_DATE))
                    .addParameter("daystoexpiration", "" + daysToExpiration)
                    .addParameter("surfacetypeidentifier", surfaceType.getId().toString())
                    .addParameter("surfacetypevalue", surfaceType.getValue())
                    .build();
            LOG.info("" + httpget.getRequestLine());
            final ResponseHandler<List<VolHistory>> volResponseHandler = new VolHistoryHandler(freq);
            List<VolHistory> histories = httpclient.execute(httpget, volResponseHandler);
            for (VolHistory vh : histories) {
                if (outputDirectory != null) {
                    Path outfile = symbolFileMap.get(vh.getSymbol());
                    Files.write(outfile, vh.format(volColumnSuffix, PriceHistory.COMMA_DELIMITER, false).getBytes());
                    System.out.println("wrote " + outfile.toString());
                } else {
                    System.out.println(vh.format(volColumnSuffix, PriceHistory.TAB_DELIMITER, true));
                    int ivp = vh.ivPercentile();
                    int ivr = vh.ivRank();
                    System.out.println(vh.getSymbol() + "," + ivp + "," + ivr);
                }
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        Options options = new Options();
        Option pValue = new Option("p", true, "period");
        options.addOption(pValue);
        Option intDuration = new Option("f", true, "frequency of bars");
        options.addOption(intDuration);
        Option startDateOption = new Option("s", true, "start date");
        options.addOption(startDateOption);
        Option endDateOption = new Option("e", true, "end date");
        options.addOption(endDateOption);
        Option surfaceTypeOption = new Option("st", true, "surface type");
        options.addOption(surfaceTypeOption);
        Option daysToExpirationOption = new Option("dte", true, "days to expiration");
        options.addOption(daysToExpirationOption);
        Option outfileOption = new Option("o", "outputdirectory", false, "save output files to directory");
        options.addOption(outfileOption);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        if (cmdLine.hasOption("p")) {
            period = Period.parse(cmdLine.getOptionValue("p"));
        }
        if (cmdLine.hasOption("f")) {
            freq = Frequency.parse(cmdLine.getOptionValue("f"));
        }
        if (cmdLine.hasOption("s")) {
            String date = cmdLine.getOptionValue("s");
            startDate = LocalDate.parse(date, DateTimeFormatter.BASIC_ISO_DATE);

        }
        if (cmdLine.hasOption("e")) {
            String date = cmdLine.getOptionValue("e");
            endDate = LocalDate.parse(date, DateTimeFormatter.BASIC_ISO_DATE);
        }
        if (cmdLine.hasOption("st")) {
            String surfaceTypeIdAndValue = cmdLine.getOptionValue("st");
            surfaceType = SurfaceType.parse(surfaceTypeIdAndValue);
        }
        if (cmdLine.hasOption("dte")) {
            String value = cmdLine.getOptionValue("dte");
            daysToExpiration = Integer.parseInt(value);
        }
        //TODO: extended
        if (cmdLine.getArgs().length < 1) {
            throw new ParseException("symbol(s) not found");
        }
        symbols = cmdLine.getArgs()[0].toUpperCase(); //first argument

        if (cmdLine.hasOption("o")) {
            if (endDate == null) {
                throw new ParseException("-o requires -e<DATE>");
            }
            String fileSuffix = endDate.format(DateTimeFormatter.BASIC_ISO_DATE) + DATA_FILE_COMPONENT_DELIMITER
                    + "iv" + DATA_FILE_COMPONENT_DELIMITER
                    + period.toString() + DATA_FILE_COMPONENT_DELIMITER
                    + freq.toString() + DATA_FILE_COMPONENT_DELIMITER
                    + String.format("%03d", daysToExpiration) + "DTE" + DATA_FILE_COMPONENT_DELIMITER
                    + surfaceType.toString();
            volColumnSuffix = String.format("%03d", daysToExpiration) + "DTE" + DATA_FILE_COMPONENT_DELIMITER
                    + surfaceType.toString();
            outputDirectory = new File(outDir);
            if(!outputDirectory.exists()){
                try {
                    FileUtils.forceMkdir(outputDirectory);
                } catch (IOException e) {
                    LOG.error("unable to create " + outputDirectory, e);
                }
            }
            for (String symbol : symbols.split(",")) {
                Path outfile = Paths.get(outputDirectory.toString(), symbol + DATA_FILE_COMPONENT_DELIMITER + fileSuffix + ".csv");
                try {
                    Files.createFile(outfile);
                    System.err.println(outfile.toAbsolutePath().toString());
                    symbolFileMap.put(symbol, outfile);
                } catch (FileAlreadyExistsException ignored) {
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

    }

}
