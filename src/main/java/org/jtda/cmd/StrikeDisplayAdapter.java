package org.jtda.cmd;

import generated.Option;
import generated.OptionStrike;
import org.apache.commons.lang3.StringUtils;
import org.jtda.model.OptionContract;
import org.jtda.model.Strike;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.temporal.ChronoUnit.DAYS;

public final class StrikeDisplayAdapter {
    private static final NumberFormat FOUR_DECIMALS = new DecimalFormat("#0.0000");
    private static final NumberFormat PRICE = new DecimalFormat("#0.00");
    private static final NumberFormat STRIKE = new DecimalFormat("#0.#");

    private LocalDate expiration;
    private String expirationType;
    private double strikePrice;
    private final Option call;
    private final Option put;

    StrikeDisplayAdapter(final OptionStrike strike, final LocalDate expirationDate) {
        expiration = expirationDate;
        strikePrice = strike.getStrikePrice().doubleValue();
        call = strike.getCall();
        put = strike.getPut();
    }

    StrikeDisplayAdapter(final Strike strike) {
        this(strike.getCallContract(), strike.getPutContract());
        expirationType = strike.getCallContract().getExpirationType() + "" +
            strike.getPutContract().getExpirationType();
    }

    private StrikeDisplayAdapter(final OptionContract callOption, final OptionContract putOption) {
        expiration = callOption.getExpirationDate();
        strikePrice = callOption.getStrikePrice().doubleValue();

        call = new Option();
        call.setOptionSymbol(callOption.getOptionSymbolName());
        if(Objects.nonNull(callOption.getQuote())){
            call.setBid(callOption.getQuote().getBid());
            call.setAsk(callOption.getQuote().getAsk());
            call.setBidAskSize(callOption.getQuote().getBidAskSize());
            call.setLast(callOption.getQuote().getLast());
            call.setLastTradeDate(callOption.getQuote().getLastTradeDate());
            call.setVolume(callOption.getQuote().getVolume());
            call.setOpenInterest((long) callOption.getQuote().getOpenInterest());
            call.setDelta(callOption.getQuote().getDelta());
            final double callIv = callOption.getQuote().getImpliedVolatility();
            if(!Double.isNaN(callIv)){
                call.setImpliedVolatility(Double.toString(callIv));
            } else {
                call.setImpliedVolatility("");
            }
        } else {
            call.setBid(0d);
            call.setAsk(0d);
            call.setBidAskSize("0x0");
            call.setLast(0d);
            call.setLastTradeDate("");
            call.setVolume(0L);
            call.setOpenInterest(0L);
            call.setDelta(Double.NaN);
            call.setImpliedVolatility("");
        }

        put = new Option();
        put.setOptionSymbol(putOption.getOptionSymbolName());
        if(Objects.nonNull(putOption.getQuote())){
            put.setBid(putOption.getQuote().getBid());
            put.setAsk(putOption.getQuote().getAsk());
            put.setBidAskSize(putOption.getQuote().getBidAskSize());
            put.setLast(putOption.getQuote().getLast());
            put.setLastTradeDate(putOption.getQuote().getLastTradeDate());
            put.setVolume(putOption.getQuote().getVolume());
            put.setOpenInterest((long) putOption.getQuote().getOpenInterest());
            put.setDelta(putOption.getQuote().getDelta());
            final double putIv = putOption.getQuote().getImpliedVolatility();
            if(!Double.isNaN(putIv)){
                put.setImpliedVolatility(Double.toString(putIv));
            } else {
                put.setImpliedVolatility("");
            }
        } else {
            put.setBid(0d);
            put.setAsk(0d);
            put.setBidAskSize("0x0");
            put.setLast(0d);
            put.setLastTradeDate("");
            put.setVolume(0L);
            put.setOpenInterest(0L);
            put.setDelta(Double.NaN);
            put.setImpliedVolatility("");

        }
    }


    public LocalDate getExpiration() {
        return expiration;
    }

    public void setExpiration(final LocalDate expiration) {
        this.expiration = expiration;
    }

    public String formatForMls(int strikeIndex) {
        List<String> elements = new ArrayList<>();
        elements.add(strikeIndex + " " + spreadIndicator(call, put));
        elements.add("" + daysToExpiration(expiration));
        elements.add(STRIKE.format(strikePrice) + "C @ " + PRICE.format(call.getLast())); //call
        //elements.add(call.getOptionSymbol());
        elements.add(PRICE.format(call.getBid()));
        elements.add(PRICE.format(call.getAsk()));
        elements.add(call.getBidAskSize());
        elements.add(FOUR_DECIMALS.format(call.getDelta()));
        elements.add("" + call.getVolume());
        elements.add("" + call.getOpenInterest());
        elements.add(STRIKE.format(strikePrice) + "P @ " + PRICE.format(put.getLast())); //put
        //elements.add(put.getOptionSymbol());
        elements.add(PRICE.format(put.getBid()));
        elements.add(PRICE.format(put.getAsk()));
        elements.add(put.getBidAskSize());
        elements.add(FOUR_DECIMALS.format(put.getDelta()));
        elements.add("" + put.getVolume());
        elements.add("" + put.getOpenInterest());

        return StringUtils.join(elements.toArray(), "\t");
    }

    private static long daysToExpiration(LocalDate expiry){
        return DAYS.between(LocalDate.now(), expiry);
    }

    /**
     * Returns 1 asterisk if either call or put spread is 5 cents or under
     * and will return 2 asterisks if both spreads are 5 cents or under.
     * Will return 1 dash if either call or put spread is greater than 5 and 30 cents or less
     * and will return 2 dashes if both spreads are in this range.
     * Will return an empty string if both spreads are greater than 30 cents or either has no bid
     * or bid is zero.
     * @param call .
     * @param put .
     * @return more asterisks for a tighter spread.
     */
    private static String spreadIndicator(final Option call, final Option put) {
        double callSpread = call.getAsk() - call.getBid();
        double putSpread = put.getAsk() - put.getBid();
        String indicator = "";
        if(call.getBid() != 0) {
            if (callSpread <= 0.30 && callSpread > 0.05) {
                indicator += "-";
            } else if (callSpread <= 0.05) {
                indicator += "*";
            }
        }
        if(put.getBid() != 0) {
            if (putSpread <= 0.30 && putSpread > 0.05) {
                indicator += "-";
            } else if (putSpread <= 0.05) {
                indicator += "*";
            }
        }
        return indicator;
    }

    public String format() {
        List<String> elements = new ArrayList<>();
        elements.add(ISO_LOCAL_DATE.format(expiration));
        elements.add(call.getOptionSymbol());
        elements.add(PRICE.format(call.getBid()));
        elements.add(PRICE.format(call.getAsk()));
        elements.add(call.getBidAskSize());
        elements.add(FOUR_DECIMALS.format(call.getDelta()));
        elements.add(call.getImpliedVolatility());
        elements.add(STRIKE.format(strikePrice));
        elements.add(put.getOptionSymbol());
        elements.add(PRICE.format(put.getBid()));
        elements.add(PRICE.format(put.getAsk()));
        elements.add(put.getBidAskSize());
        elements.add(FOUR_DECIMALS.format(put.getDelta()));
        elements.add(put.getImpliedVolatility());

        return StringUtils.join(elements.toArray(), "\t");
    }

    String format2() {
        List<String> elements = new ArrayList<>();
        elements.add(ISO_LOCAL_DATE.format(expiration) + " (" + expirationType + ")");
        elements.add(STRIKE.format(strikePrice) + " |");
        elements.add(PRICE.format(spread(call.getBid(), call.getAsk())));
        elements.add(PRICE.format(call.getBid()));
        elements.add(PRICE.format(call.getAsk()));
        elements.add(call.getBidAskSize());
        elements.add(call.getLast().toString());
        elements.add(call.getLastTradeDate());
        elements.add(call.getVolume().toString());
        elements.add(call.getOpenInterest().toString());
        elements.add(FOUR_DECIMALS.format(call.getDelta()) + " |");

        elements.add(PRICE.format(spread(put.getBid(), put.getAsk())));
        elements.add(PRICE.format(put.getBid()));
        elements.add(PRICE.format(put.getAsk()));
        elements.add(put.getBidAskSize());
        elements.add(put.getLast().toString());
        elements.add(put.getLastTradeDate());
        elements.add(put.getVolume().toString());
        elements.add(put.getOpenInterest().toString());
        elements.add(FOUR_DECIMALS.format(put.getDelta()));

        return StringUtils.join(elements.toArray(), "\t");
    }

    private double spread(double bid, double ask){
        return ask - bid;
    }

}
