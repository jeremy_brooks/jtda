package org.jtda.cmd;

import generated.*;
import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.OrderStatusXmlHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

public class OrderStatusCmd extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(OrderStatusCmd.class);
    private static final String NAME = "OrderStatus";
    private static final ResponseHandler<OrderStatusList> HANDLER = new OrderStatusXmlHandler();
    private static final DateTimeFormatter DATE_TIME_FMT = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss z");
    private Session session;
    private String type = "all"; //all, open, filled, canceled, pending
    private int daysBack = 0;    //0 to 60


    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        Options options = new Options();
        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        final int argSize = cmdLine.getArgs().length;
        if (argSize >= 1) {
            type = cmdLine.getArgs()[0]; //first argument
        }
        if (argSize >= 2) {
            daysBack = Integer.parseInt(cmdLine.getArgs()[1]);
        }
    }

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .addParameter("type", type)
                    .addParameter("days", "" + daysBack)
                    .build();
            LOG.info("" + httpget.getRequestLine());
            OrderStatusList data = httpclient.execute(httpget, HANDLER);
            //System.out.println(XmlUtil.format(responseBody));
            final String headers = StringUtils.join(
                    new String[]{"orderId","status","enhanced", "receivedTime","reportedTime","timeToReport","strategy","price\n"}, "\t");
            StringBuilder sb = new StringBuilder(headers);
            for (OrderStatus os : data.getOrderstatus()) {
                sb.append(os.getOrderNumber()).append("\t");
                sb.append(os.getDisplayStatus()).append("\t");
                sb.append(os.isEnhancedOrder() ? os.getEnhancedType() : "-").append("\t");
                sb.append(os.getOrderReceivedDateTime()).append("\t");
                sb.append(os.getReportedTime()).append("\t");
                sb.append(durationInSeconds(os.getOrderReceivedDateTime(), os.getReportedTime())).append("\t");
                sb.append(os.getStrategy()).append("\t");
                sb.append(os.getOrder().getLimitPrice()).append("\n");
                Order order = os.getOrder();
                sb.append("\t");
                sb.append(order.getOrderId()).append("\t");
                String actionStr = actionString(order);
                sb.append(actionStr).append("\t");
                sb.append(securityName(order.getSecurity())).append("\n");
                if (os.getRelatedOrders() != null) {
                    List<RelatedOrderStatus> rosList = os.getRelatedOrders().getOrderstatus();
                    for (RelatedOrderStatus ros : rosList) {
                        Order relatedOrder = ros.getOrder();
                        sb.append("\t");
                        sb.append(relatedOrder.getOrderId()).append("\t");
                        String rActionStr = actionString(relatedOrder);
                        sb.append(rActionStr).append("\t");
                        sb.append(securityName(relatedOrder.getSecurity())).append("\n");
                    }
                }
            }
            System.out.println(sb.toString());

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    private static String securityName(Security security){
        if(StringUtils.isNotBlank(security.getDescription())){
            return security.getDescription();
        }
        return security.getSymbol();
    }

    private static String actionString(Order order){
        String str = order.getQuantity() + " ";
        if(order.getSecurity().getAssetType().equals("O")) {
            str += order.getAction() + "2" + order.getOpenClose();
        } else {
            str += order.getAction();
        }
        return str;
    }

    private static String durationInSeconds(String receivedTime, String reportedTime){
        if(Objects.nonNull(receivedTime) && Objects.nonNull(reportedTime)){
            LocalDateTime received = LocalDateTime.parse(receivedTime, DATE_TIME_FMT);
            LocalDateTime reported = LocalDateTime.parse(reportedTime, DATE_TIME_FMT);
            final Duration duration = Duration.between(received, reported);
            return "" + duration.getSeconds();
        } else {
            return "N/A";
        }
    }


    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

}
