package org.jtda.cmd;

import java.nio.ByteBuffer;
import java.time.LocalDate;

public class NumberUtil {

    private NumberUtil() {
    }

    public static byte[] toByteArray(double value) {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putDouble(value);
        return bytes;
    }

    public static double toDouble(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getDouble();
    }

    public static final void main(String[] args) {
        //17.50 = 40 31 80 00 00 00 00 00

        byte[] bytes = new byte[]{0x40, 0x31, (byte) 0x80, 0, 0, 0, 0, 0};
        //System.out.println(NumberUtil.toDouble(bytes));
        LocalDate d = LocalDate.now(); //.toString().replace("-","");
        System.out.println(d.toString());
    }
}