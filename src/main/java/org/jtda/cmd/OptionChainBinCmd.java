package org.jtda.cmd;

import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.OptionChainBinaryHandler;
import org.jtda.model.OptionChain;
import org.jtda.model.OptionSeries;
import org.jtda.model.Strike;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Examples:
 * ocb fb          //list all facebook options
 *
 * @author jeremy
 */
public class OptionChainBinCmd extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(OptionChainBinCmd.class);
    private static final String NAME = "BinaryOptionChain";
    private static final ResponseHandler<OptionChain> HANDLER = new OptionChainBinaryHandler();

    private Session session;
    private String symbols;
    private String type; //C, P, VCS, VPS, CCS, CPS, BW, STDL, STGL
    private String expire; //al, a, l, w
    private String interval; //interval between strikes

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "200");
            HttpUriRequest httpget = buildRequest(uri);
            LOG.info("" + httpget.getRequestLine());
            Instant start = Instant.now();
            OptionChain ocr = httpclient.execute(httpget, HANDLER);
            Instant finish = Instant.now();
            LOG.info("response time: {}", Duration.between(start, finish));
            System.out.println("Symbol : " + ocr.getSymbol());
            System.out.println("Last   : " + ocr.getLast());
            System.out.println("Bid    : " + ocr.getBid());
            System.out.println("Ask    : " + ocr.getAsk());
            System.out.println("BASize : " + ocr.getBidAskSize());
            System.out.println("YdayCls: " + ocr.getClose());
            System.out.println("Expirations:");
            LocalDate now = LocalDate.now();

            Set<LocalDate> expirations = ocr.getOptionExpirations();
            for (LocalDate expiration: expirations) {
                long dte = DAYS.between(LocalDate.now(), expiration);
                System.out.println("now    : " + now.toString() + ", DTE=" + dte);
                System.out.println("Calls\tExp: " + expiration.toString() + "\tPuts");
                System.out.println("Expiration\tBid\tAsk\tDelta\tIV\tStrikeOld\tBid\tAsk\tDelta\tIV");

                OptionSeries series = ocr.getOptionSeries(expiration);
                boolean foundAtmStrike = false;
                for(Strike strike: series.getStrikes()){
                    if (!foundAtmStrike && strike.getStrikePrice() >= ocr.getLast()) {
                        System.out.println(now.toString() + " (" + dte + " DTE) ---------------------------|"+ ocr.getLast() +"|------------------------");
                        foundAtmStrike = true;
                    }
                    StrikeDisplayAdapter adapter = new StrikeDisplayAdapter(strike);
                    System.out.println(adapter.format());
                }
            }
            LOG.info("response time: {}", Duration.between(start, finish));

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }


    private HttpUriRequest buildRequest(final URI uri) {
        return RequestBuilder.get()
                .setUri(uri)
                .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                .addParameter("symbol", symbols)
                .addParameter("quotes", "true")
                .addParameter("type", type)
                .addParameter("expire", expire)
                .addParameter("interval", interval)
                //.addParameter("strike", "113")
                .build();
    }


    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        Options options = new Options();
        org.apache.commons.cli.Option typeValue = new org.apache.commons.cli.Option("t", true, "type");
        options.addOption(typeValue);
        org.apache.commons.cli.Option expireValue = new org.apache.commons.cli.Option("e", true, "expire");
        options.addOption(expireValue);
        org.apache.commons.cli.Option intervalValue = new org.apache.commons.cli.Option("i", true, "interval");
        options.addOption(intervalValue);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        if (cmdLine.hasOption("t")) {
            type = cmdLine.getOptionValue("t");
        }
        if (cmdLine.hasOption("e")) {
            expire = cmdLine.getOptionValue("e");
        }
        if (cmdLine.hasOption("i")) {
            interval = cmdLine.getOptionValue("i");
        }

        if (cmdLine.getArgs().length < 1) {
            throw new ParseException("symbol(s) not found");
        }
        symbols = cmdLine.getArgs()[0].toUpperCase(); //first argument
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }
}
