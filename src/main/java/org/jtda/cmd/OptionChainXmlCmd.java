package org.jtda.cmd;

import generated.OptionChainResults;
import generated.OptionDate;
import generated.OptionStrike;
import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.OptionChainXmlHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;

/**
 * Examples:
 * oc -t P FB          //list all puts for facebook
 * oc -t C -e a AAPL   //list all weekly,standard (no leaps) calls for apple
 *
 * @author jeremy
 */
public class OptionChainXmlCmd extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(OptionChainXmlCmd.class);
    private static final String NAME = "OptionChain";
    private static final ResponseHandler<OptionChainResults> HANDLER = new OptionChainXmlHandler();
    private Session session;
    private String symbols;
    private String type; //C, P, VCS, VPS, CCS, CPS, BW, STDL, STGL
    private String expire; //al, a, l, w, YYYYMM
    private String interval; //interval between strikes

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "200");
            HttpUriRequest httpget = buildRequest(uri);
            LOG.info("" + httpget.getRequestLine());
            Instant start = Instant.now();
            OptionChainResults ocr = httpclient.execute(httpget, HANDLER);
            Instant finish = Instant.now();
            System.out.println("Symbol : " + ocr.getSymbol());
            System.out.println("Last   : " + ocr.getLast());
            System.out.println("Bid    : " + ocr.getBid());
            System.out.println("Ask    : " + ocr.getAsk());
            System.out.println("YdayCls: " + ocr.getClose());
            System.out.println("Expirations:");
            LocalDate now = LocalDate.now();


            for (OptionDate od : ocr.getOptionDate()) {
                LocalDate expiration = now.plusDays(od.getDaysToExpiration());
                int dte = od.getDaysToExpiration();
                System.out.println("now    : " + now.toString() + ", DTE=" + od.getDaysToExpiration());
                System.out.println("Calls\tExp: " + expiration.toString() + "\tPuts");
                System.out.println("Expiration\tBid\tAsk\tDelta\tIV\tStrikeOld\tBid\tAsk\tDelta\tIV");
                boolean foundAtmStrike = false;
                for (OptionStrike strike : od.getOptionStrike()) {
                    if (!foundAtmStrike && strike.getStrikePrice().floatValue() >= ocr.getLast().floatValue()) {
                        System.out.println(now.toString() + " (" + dte + " DTE) ---------------------------|"+ ocr.getLast() +"|------------------------");
                        foundAtmStrike = true;
                    }
                    StrikeDisplayAdapter adapter = new StrikeDisplayAdapter(strike, expiration);
                    System.out.println(adapter.format());
                }
            }
            LOG.info("response time: {}", Duration.between(start, finish));
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    private HttpUriRequest buildRequest(final URI uri) {
        return RequestBuilder.get()
                .setUri(uri)
                .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                .addParameter("symbol", symbols)
                .addParameter("quotes", "true")
                .addParameter("type", type)
                .addParameter("expire", expire)
                .addParameter("interval", interval)
                //.addParameter("strike", "113")
                .build();
    }


    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        Options options = new Options();
        org.apache.commons.cli.Option typeValue = new org.apache.commons.cli.Option("t", true, "type");
        options.addOption(typeValue);
        org.apache.commons.cli.Option expireValue = new org.apache.commons.cli.Option("e", true, "expire");
        options.addOption(expireValue);
        org.apache.commons.cli.Option intervalValue = new org.apache.commons.cli.Option("i", true, "interval");
        options.addOption(intervalValue);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        if (cmdLine.hasOption("t")) {
            type = cmdLine.getOptionValue("t");
        }
        if (cmdLine.hasOption("e")) {
            expire = cmdLine.getOptionValue("e");
        }
        if (cmdLine.hasOption("i")) {
            interval = cmdLine.getOptionValue("i");
        }

        if (cmdLine.getArgs().length < 1) {
            throw new ParseException("symbol(s) not found");
        }
        symbols = cmdLine.getArgs()[0].toUpperCase(); //first argument
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }
}
