package org.jtda.cmd;

import generated.Amtd;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class Logout extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(Logout.class);

    private static final String NAME = "LogOut";
    private static final ResponseHandler<Boolean> HANDLER = new LogoutHandler();

    private Session session;

    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public void execute() throws CommandException {
        //https://apis.tdameritrade.com/apps/100/LogOut?source=#sourceID#
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .build();

            LOG.info("" + httpget.getRequestLine());
            Boolean isLoggedOut = httpclient.execute(httpget, HANDLER);
            session.invalidate();
            System.out.println(isLoggedOut ? "Logged out"
                    : "Unable to log out, but local session was invalidated");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(httpclient);
        }

    }

    @Override
    public void setSession(Session session) {
        this.session = session;
    }

    private static class LogoutHandler implements ResponseHandler<Boolean> {

        @Override
        public Boolean handleResponse(HttpResponse response)
                throws IOException {
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String xml = EntityUtils.toString(response.getEntity());
                    Amtd r = XmlUtil.unmarshall(xml);
                    return "LoggedOut".equals(r.getResult());
                }
                return false;
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            }
        }
    }

}
