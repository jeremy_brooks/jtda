package org.jtda.cmd;

/**
 * Created by jeremy on 6/3/18.
 */
public enum TdaPeriod {
    DAY,     //1, 2, 3, 4, 5, 10
    MONTH,   //1, 2, 3, 6
    YEAR,    //1, 2, 3, 5 10, 15, 20
    YTD     //1
}