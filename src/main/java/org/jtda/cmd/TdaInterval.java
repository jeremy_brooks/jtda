package org.jtda.cmd;

/**
 * Created by jeremy on 6/3/18.
 */
public enum TdaInterval {
    MINUTE,
    DAILY,
    WEEKLY,
    MONTHLY
}