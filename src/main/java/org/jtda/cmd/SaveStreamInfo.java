package org.jtda.cmd;

import generated.StreamerInfo;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.util.EntityUtils;
import org.jtda.handler.StreamerInfoXmlHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class SaveStreamInfo extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(SaveStreamInfo.class);
    private static final String NAME = "StreamerInfo";
    private static final ResponseHandler<StreamerInfo> HANDLER = new StreamerInfoXmlHandler();
    private Session session;

    @Override
    public void execute() throws CommandException {
        try {
            //HttpGet httpget = new HttpGet("https://apis.tdameritrade.com/apps/100/BalancesAndPositions?source=<#sourceID#>");


            // Create a custom response handler
            ResponseHandler<String> stringResponseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            };
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .build();
            LOG.info("" + httpget.getRequestLine());
//            String responseBody = httpclient.execute(httpget, stringResponseHandler);
//            System.out.println(XmlUtil.format(responseBody));
            StreamerInfo s = httpclient.execute(httpget, HANDLER);
            System.out.println("streamer-url : " + s.getStreamerUrl());
            System.out.println("token        : " + s.getToken());
            System.out.println("timestamp    : " + s.getTimestamp());
            System.out.println("cd-domain-id : " + s.getCdDomainId());
            System.out.println("usergroup    : " + s.getUsergroup());
            System.out.println("access-level : " + s.getAccessLevel());
            System.out.println("acl          : " + s.getAcl());
            System.out.println("app-id       : " + s.getAppId());
            System.out.println("authorized   : " + s.getAuthorized());
            System.out.println("error-msg    : " + s.getErrorMsg());
            session.setStreamerInfo(s);
            System.out.println("saved streamer info to session");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

}
