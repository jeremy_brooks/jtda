package org.jtda.cmd;

import java.time.Period;
import java.util.List;

public class VolHistoryRequest {

    List<String> symbols;
    Frequency freq;
    Period period;

    protected VolHistoryRequest(List<String> forSymbols, Frequency interval, Period duration) {
        java.time.Period p = java.time.Period.ofYears(1);
        symbols = forSymbols;
        this.freq = interval;
        this.period = duration;
    }

    public static Builder builder() {
        return new Builder();
    }

    /*
     HistoryRequest request = VolHistoryRequest.builder()
        .forSymbols(symbolList)
        .every(1, DAY)           //interval
        .for(1, YEAR)            //period
        .build();                //default is implied volatility history
    HistoryResponse response = client.fetchHistory(request)
     */
    public static class Builder {
        List<String> forSymbols;
        Period forPeriod;
        Frequency freq;

        public void forSymbols(List<String> forSymbols) {
            this.forSymbols = forSymbols;
        }

        public void every(String every) {
            this.freq = Frequency.parse(every);
        }

        public void forPeriod(String duration) {
            this.forPeriod = Period.parse(duration);
        }

        public VolHistoryRequest build() {
            return new VolHistoryRequest(forSymbols, freq, forPeriod);
        }

    }
}
