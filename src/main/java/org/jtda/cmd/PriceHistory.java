package org.jtda.cmd;

import org.apache.commons.lang3.StringUtils;
import org.jtda.model.OhlcData;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class PriceHistory extends ArrayList<OhlcData> {
    public static final char COMMA_DELIMITER = ',';
    public static final char TAB_DELIMITER = '\t';
    private static final String RECORD_DELIMITER = System.getProperty("line.separator");
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("HHmmss");
    private static final long serialVersionUID = 1L;
    private String symbol;
    private String error;

    public PriceHistory(String symbol) {
        super();
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String format(char delimiter, boolean includeSymbolAsColumn) {
        StringBuilder sb = new StringBuilder();
        if (!includeSymbolAsColumn) {
            sb.append("# symbol: ").append(symbol);
            sb.append(RECORD_DELIMITER);
        }
        if (StringUtils.isNotEmpty(error)) {
            sb.append("Error: ").append(error);
            return sb.toString();
        }
        sb.append("timeZone=America/New_York").append(RECORD_DELIMITER);
        sb.append("date").append(delimiter);
        sb.append("time").append(delimiter);
        if (includeSymbolAsColumn) {
            sb.append("symbol").append(delimiter);
        }
        sb.append("open").append(delimiter)
                .append("high").append(delimiter)
                .append("low").append(delimiter)
                .append("close").append(delimiter)
                .append("volume")
                .append(RECORD_DELIMITER);
        for (OhlcData bar : this) {
            sb.append(bar.getDateTimeUtc().format(DATE_FORMAT)).append(delimiter);
            sb.append(bar.getDateTimeUtc().format(TIME_FORMAT)).append(delimiter);
            if (includeSymbolAsColumn) sb.append(symbol).append(delimiter);
            sb.append(bar.getOpen()).append(delimiter);
            sb.append(bar.getHigh()).append(delimiter);
            sb.append(bar.getLow()).append(delimiter);
            sb.append(bar.getClose()).append(delimiter);
            sb.append(new Float(bar.getVolume() * 100).longValue());
            sb.append(RECORD_DELIMITER);
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(error)) {
            sb.append("Error: ").append(error);
            return sb.toString();
        }
        sb.append("DateTime").append(TAB_DELIMITER)
                .append("Symbol").append(TAB_DELIMITER)
                .append("Open").append(TAB_DELIMITER)
                .append("High").append(TAB_DELIMITER)
                .append("Low").append(TAB_DELIMITER)
                .append("Close").append(TAB_DELIMITER)
                .append("VolumeX100")
                .append(RECORD_DELIMITER);
        for (OhlcData bar : this) {
            sb.append(bar.getDateTimeUtc().format(DATE_FORMAT)).append(TAB_DELIMITER);
            sb.append(bar.getDateTimeUtc().format(TIME_FORMAT)).append(TAB_DELIMITER);
            sb.append(symbol).append(TAB_DELIMITER);
            sb.append(bar.getOpen()).append(TAB_DELIMITER);
            sb.append(bar.getHigh()).append(TAB_DELIMITER);
            sb.append(bar.getLow()).append(TAB_DELIMITER);
            sb.append(bar.getClose()).append(TAB_DELIMITER);
            sb.append(bar.getVolume());
            sb.append(RECORD_DELIMITER);
        }
        return sb.toString();
    }
}
