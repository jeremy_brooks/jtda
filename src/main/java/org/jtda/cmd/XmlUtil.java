package org.jtda.cmd;

import generated.Amtd;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Pretty-prints xml, supplied as a string.
 * <p/>
 * eg.
 * <code>
 * String formattedXml = XmlUtil.format("<tag><nested>hello</nested></tag>");
 * </code>
 * http://kveeresham.blogspot.com/2015/03/format-xml-using-java.html
 */
public class XmlUtil {
    private static final Logger LOG = LoggerFactory.getLogger(XmlUtil.class);
    public static final DateTimeFormatter DATE_YYYYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd");

    private static Pattern acctXmlPattern = Pattern
            .compile("<account-id>([0-9]*)</account-id>", Pattern.CASE_INSENSITIVE);
    private static Pattern assocAcctXmlPattern = Pattern
            .compile("<associated-account-id>([0-9]*)</associated-account-id>", Pattern.CASE_INSENSITIVE);

    private static final NumberFormat US_NUMBER_FORMAT = NumberFormat.getNumberInstance(Locale.US);
    private static final DecimalFormat US_DECIMAL_FORMAT = (DecimalFormat) US_NUMBER_FORMAT;
    static {
        US_DECIMAL_FORMAT.setParseBigDecimal(true);
        US_DECIMAL_FORMAT.setGroupingUsed(true);
    }

    private static Unmarshaller unmarshaller;
    static {
        try {
            JAXBContext jc = JAXBContext.newInstance(Amtd.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private XmlUtil() {
    }

    public static String format(String input) {
        try {
            final InputSource src = new InputSource(new StringReader(input));
            final Node document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder().parse(src).getDocumentElement();

            final DOMImplementationRegistry registry = DOMImplementationRegistry
                    .newInstance();
            final DOMImplementationLS impl = (DOMImplementationLS) registry
                    .getDOMImplementation("LS");
            final LSSerializer writer = impl.createLSSerializer();

            writer.getDomConfig().setParameter("format-pretty-print",
                    Boolean.TRUE);
            writer.getDomConfig().setParameter("xml-declaration", true);

            return writer.writeToString(document);
        } catch (Exception e) {
            e.printStackTrace();
            return input;
        }
    }

    public static Amtd unmarshall(String xml) {
        return unmarshall(xml, false);
    }

    public static Amtd unmarshall(String xml, boolean includeRawXml) {
        if (includeRawXml) {
            LOG.info("xml:\n" + ((xml != null) ? maskAccountNumbers(xml) : ""));
        }
        Amtd r = null;
        try {
            InputStream is = IOUtils.toInputStream(xml, Charset.forName("UTF-8"));
            unmarshaller.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
            JAXBElement<Amtd> element = (JAXBElement<Amtd>) unmarshaller.unmarshal(is);
            if (element.getDeclaredType().isAssignableFrom(Amtd.class)) {
                r = element.getValue();
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return r;
    }

    static String maskAccountNumbers(String xml) {
        Matcher m = acctXmlPattern.matcher(xml);
        String xml2 = m.replaceAll("<account-id>-1</account-id>");
        m = assocAcctXmlPattern.matcher(xml2);
        return m.replaceAll("<associated-account-id>-2</associated-account-id>");
    }

    /**
     * Best effort conversion from double to BigDecimal.
     * Use when you want an exact result that can be used in pricing calculations.
     *
     * If value is Double.NaN, zero will be returned.
     * All other values are parsed according to parseBigDecimal(String)
     * @param value
     * @return
     */
    public static BigDecimal toBigDecimal(double value) {
        if(Double.isNaN(value)){
            return BigDecimal.ZERO;
        }
        return parseBigDecimal(Double.toString(value));
    }

    /**
     * Best effort parse a string in US Locale format to a BigDecimal
     * Use when you want an exact result that can be used in pricing calculations.
     *
     * The following strings/values will parse to zero:
     * <ul>
     *     <li><code>null</code></li>
     *     <li>""</li>
     * </ul>
     * Will also parse comma separated groupings (e.g. 100,405).
     */
    public static BigDecimal parseBigDecimal(String value) {
        if (StringUtils.isEmpty(value)) {
            return BigDecimal.ZERO;
        }
        try {
            return (BigDecimal) US_DECIMAL_FORMAT.parse(value);
        } catch (ParseException e) {
            throw new IllegalArgumentException("unable to parse big decimal: " + value, e);
        }
    }

    public static LocalDate parseLocalDate(String value){
        return LocalDate.parse(value, DATE_YYYYMMDD);
    }

    public static double parseDoubleOrEmpty(String value) {
        return NumberUtils.toDouble(value, 0.0);
    }

    public static byte[] filetoByteArray(File file) throws IOException {

        if (file.canRead() == false) {
            throw new RuntimeException("Cant read " + file);
        }

        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        FileInputStream fi = null;
        try {
            fi = new FileInputStream(file);

            byte buffer[] = new byte[1024];
            int len;
            while ((len = fi.read(buffer)) != -1) {
                bout.write(buffer, 0, len);
            }

            bout.close();
            fi.close();
            return bout.toByteArray();
        } finally {
            if (fi != null) {
                fi.close();
            }
        }
    }

    public static final void main(String[] args) {
        String unformattedXml;
        try {
            unformattedXml = Files.readAllLines(Paths.get("C:/Users/jeremy/git/jtda/src/test/resources/history2.xml"))
                    .stream()
                    .map(String::toString)
                    .reduce("", String::concat);
            System.out.println(XmlUtil.format(unformattedXml));
            Amtd r = XmlUtil.unmarshall(unformattedXml);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        /*
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?><QueryMessage\n" +
                "        xmlns=\"http://www.SDMX.org/resources/SDMXML/schemas/v2_0/message\"\n" +
                "        xmlns:query=\"http://www.SDMX.org/resources/SDMXML/schemas/v2_0/query\">\n" +
                "    <Query>" +
                "        <query:CategorySchemeWhere>\n" +
                "   \t\t\t\t\t         <query:AgencyID>ECB\n\n\n\n</query:AgencyID>\n" +
                "        </query:CategorySchemeWhere>\n" +
                "    </Query>\n\n\n\n\n" +
                "</QueryMessage>";
        */
    }

}