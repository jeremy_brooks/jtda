package org.jtda.cmd;

import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.OptionChainBinaryHandler;
import org.jtda.model.OptionChain;
import org.jtda.model.OptionSeries;
import org.jtda.model.Strike;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.SortedSet;
import java.util.stream.Collectors;

/**
 * List the at-the-money strikes for the selected symbol.
 *
 * @author jeremy
 */
public class AtmStrikes extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(AtmStrikes.class);

    private static final String NAME = "BinaryOptionChain";
    private static final ResponseHandler<OptionChain> HANDLER = new OptionChainBinaryHandler();

    private Session session;
    private String symbols;
    private String type; //C, P, VCS, VPS, CCS, CPS, BW, STDL, STGL
    private String interval; //interval between strikes

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "200");
            OptionChain ocr;
            List<LocalDate> expirationDates = TimeUtil.computeStandardExpirationMonths(LocalDate.now());
            HttpUriRequest httpget = buildRequest(uri, YearMonth.from(expirationDates.get(0)));
            LOG.info("" + httpget.getRequestLine());
            Instant start = Instant.now();
            ocr = httpclient.execute(httpget, HANDLER);
            Instant finish = Instant.now();
            LOG.info("response time: {}", Duration.between(start, finish));
            HttpUriRequest httpget2 = buildRequest(uri, YearMonth.from(expirationDates.get(1)));
            LOG.info("" + httpget2.getRequestLine());
            Instant start2 = Instant.now();
            OptionChain ocr2 = httpclient.execute(httpget2, HANDLER);
            Instant finish2 = Instant.now();
            LOG.info("response time: {}", Duration.between(start2, finish2));
            LOG.info("total time: {}", Duration.between(start, finish2));
            double lastPrice = ocr2.getLast();
            System.out.println("Symbol : " + ocr2.getSymbol() + " (" + ocr2.getDescription() + ")");
            System.out.println("Last   : " + lastPrice);
            System.out.println("Bid    : " + ocr2.getBid());
            System.out.println("Ask    : " + ocr2.getAsk());
            System.out.println("BASize : " + ocr2.getBidAskSize());
            System.out.println("YdayCls: " + ocr2.getClose());
            System.out.println("Expirations:");


            ocr.addChain(ocr2.getChain());
            SortedSet<LocalDate> expirations = ocr.getOptionExpirations();
            for (LocalDate expiration : expirations) {
                OptionSeries series = ocr.getOptionSeries(expiration);
//                Strike strikeBelow = series.getStrikesBelow(lastPrice).get(0);
                Strike strikeAbove = series.getStrikesAbove(lastPrice).get(0);
//                StrikeDisplayAdapter adapterBelow = new StrikeDisplayAdapter(strikeBelow);
                StrikeDisplayAdapter adapterAbove = new StrikeDisplayAdapter(strikeAbove);
//                System.out.println(adapterBelow.format2());
                System.out.println(adapterAbove.format2());
            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }


    private HttpUriRequest buildRequest(final URI uri, YearMonth yearMonth) {
        final String expirationMonth = yearMonth.toString().replace("-", "");
        return RequestBuilder.get()
                .setUri(uri)
                .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                .addParameter("symbol", symbols)
                .addParameter("quotes", "true")
                .addParameter("type", type)
                .addParameter("expire", expirationMonth)
                .addParameter("interval", interval)
                //.addParameter("strike", "113")
                .build();
    }

    private Strike computeStrikeWithSmallestCallPutPriceDiff(final OptionSeries series) {
        LOG.debug("compute forward strike for DTE=" + series.getDaysToExpiration());
        // 1) compute avg price of each call and each put for each strike
        List<Strike> mystrikes = series.getStrikes().stream()
                .sorted(new Strike.CallPutDifferenceComparator())
                .collect(Collectors.toList());
        //mystrikes.stream().forEach(s -> System.out.println(s.toString()));
        //System.out.println("strikeCount: " + mystrikes.stream().count());
        return mystrikes.stream().findFirst().get();
    }

    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        Options options = new Options();
        org.apache.commons.cli.Option typeValue = new org.apache.commons.cli.Option("t", true, "type");
        options.addOption(typeValue);
        org.apache.commons.cli.Option expireValue = new org.apache.commons.cli.Option("e", true, "expire");
        options.addOption(expireValue);
        org.apache.commons.cli.Option intervalValue = new org.apache.commons.cli.Option("i", true, "interval");
        options.addOption(intervalValue);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        if (cmdLine.hasOption("t")) {
            type = cmdLine.getOptionValue("t");
        }
        if (cmdLine.hasOption("i")) {
            interval = cmdLine.getOptionValue("i");
        }

        if (cmdLine.getArgs().length < 1) {
            throw new ParseException("symbol(s) not found");
        }
        symbols = cmdLine.getArgs()[0].toUpperCase(); //first argument
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }


}
