package org.jtda.cmd;

import org.jtda.model.VolBar;
import org.jtda.model.VolData;
import org.jtda.model.VolHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Would it make more sense to make VolHistory have a map of date->volatility
 * and just return NaN for dates that aren't found, removing the needed for this class entirely?
 */
public class DefaultRangeFiller implements RangeFiller {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultRangeFiller.class);

    @Override
    public VolHistory fill(VolHistory volHistory, FillFrequency fillFrequency) {
        if(Frequency.parse("1d").equals(volHistory.getBarFrequency())){
            //Okay so we fill in every day (add weekends, holidays any day not accounted for)
            long numFilledDays = 0;
            ZonedDateTime prevZdt = volHistory.get(0).getDateTimeUtc();
            VolHistory filled = new VolHistory(volHistory.getSymbol());
            for(VolData vd: volHistory){
                long daysToFill = DAYS.between(prevZdt, vd.getDateTimeUtc()) - 1;
                int filledDayIndex = 1;
                while(daysToFill > 0){
                    ZonedDateTime filledDay = prevZdt.plusDays(filledDayIndex);
                    filled.add(new VolBar(Float.NaN, filledDay));
                    filledDayIndex++;
                    daysToFill--;
                    LOG.debug("filled Nan for " + filledDay.toLocalDate());
                    numFilledDays++;
                }
                filled.add(vd);
                prevZdt = vd.getDateTimeUtc();
            }
            LOG.info("filled {} days with NaN", numFilledDays);
            return filled;
        }
        return null;
    }
}
