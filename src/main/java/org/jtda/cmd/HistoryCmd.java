package org.jtda.cmd;

import generated.History;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.HistoryXmlHandler;
import org.jtda.model.Option;
import org.jtda.model.TdaTransaction;
import org.jtda.model.TdaTransaction.AssetType;
import org.jtda.model.TdaTransaction.Effect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HistoryCmd extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(HistoryCmd.class);

    private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss z");
    private static final String JTDA_ID_NON_ORDER_PREFIX = "X";
    private static final String DELIMITER = ",";
    private static final String NAME = "History";
    private static final ResponseHandler<History> HANDLER = new HistoryXmlHandler();
    //d MMM uuuu} will format 2011-12-03 as '3 Dec 2011'.
    private static final DateTimeFormatter DATE_TIME_FMT = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss z");
    private Session session;

    enum Type {
        ALL(0),
        TRADES(1),
        BUY(2),
        SELL(3),
        DEPOSITS_AND_WITHDRAWALS(4),
        CHECKING(5),
        DIVIDENDS(6),
        INTEREST(7),
        OTHER(8);

        private final int code;

        Type(int code) {
            this.code = code;
        }

        @Override
        public String toString() {
            return "" + code;
        }
    }

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .addParameter("type", Type.ALL.toString()) //all types
                    .addParameter("startdate", LocalDate.now().minusDays(370).toString().replace("-", ""))
                    .build();
            LOG.info("" + httpget.getRequestLine());
//            String responseBody = httpclient.execute(httpget, stringResponseHandler);
//            System.out.println(XmlUtil.format(responseBody));
            History h = httpclient.execute(httpget, HANDLER);
            System.out.println(h.getStartDate() + " to " + h.getEndDate());
            String columnHeaders = buildColumnHeaderRow();
            System.out.println(columnHeaders);
            String accountNo = session.getAssociatedAccountId().toString();
            Map<String, List<TdaTransaction>> groupedTransactions =
                    h.getTransactionList().stream()
                            .map(unidentifiable -> buildIdentifiableTransaction(unidentifiable, accountNo))
                            .sorted()
                            .collect(Collectors.groupingBy(TdaTransaction::getGroupId));

            //List<HistoricalTransaction> xactions = new ArrayList<>();
            StringBuilder out = new StringBuilder();
            for (List<TdaTransaction> transactions : groupedTransactions.values()) {
                //HistoricalTransaction ht = new HistoricalTransactionImpl(transaction);
                for (TdaTransaction t : transactions) {
                    String row = buildRow(t);
                    out.append(row);
                    out.append(System.getProperty("line.separator"));
                }
                out.append(System.getProperty("line.separator"));
                out.append(System.getProperty("line.separator"));
            }
            System.out.println(out.toString());
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    private TdaTransaction buildIdentifiableTransaction(final generated.Transaction t, final String accountNumber) {
        String groupId;
        String leg = "";
        ZonedDateTime execDate = ZonedDateTime.parse(t.getExecutedDate(), DATE_TIME_FMT);
        List<String> group = buildGroupId(t);
        switch (group.size()) {
            case 1:
                //contains order number
                groupId = execDate.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + group.get(0);
                break;
            case 2:
                //contains order number and leg number
                groupId = execDate.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + group.get(0);
                leg = group.get(1);
                break;
            default:
                LOG.warn("unable to parse orderNumber, using it as is: {}", t.getOrderNumber());
                groupId = t.getOrderNumber();
                break;
        }
        double qty = t.getQuantity();
        if ("S".equals(t.getBuySellCode())) {
            qty = -qty;
        }
        TdaTransaction xaction = TdaTransaction.builder()
                .accountNumber(accountNumber)
                .orderNumber(t.getOrderNumber())
                .groupId(groupId)
                .leg(leg)
                .additionalFees(t.getAdditionalFees())
                .commission(t.getCommission())
                .cusip(t.getCusip())
                .description(t.getDescription())
                .executedDate(execDate)
                .fees(t.getFees())
                .price(t.getPrice())
                .quantity(qty)
                .splitRatio(t.getSplitRatio())
                .symbol(t.getSymbol())
                .symbolDescription(t.getSymbolDescription())
                .type(t.getType() + "-" + t.getSubType())
                .value(t.getValue())
                .build();
        if (!StringUtils.isEmpty(t.getOrderNumber())) {
            xaction.setOrderDateTime(ZonedDateTime.parse(t.getOrderDateTime(), DATE_TIME_FMT));
        }
        if (!StringUtils.isEmpty(t.getAssetType())) {
            xaction.setAssetType(AssetType.parse(t.getAssetType()));
        }
        if (AssetType.OPTION.equals(xaction.getAssetType())) {
            xaction.setOptionUnderlyingCusip(t.getOptionUnderlyingSymbol());
            xaction.setOptionExpirationDate(ZonedDateTime.parse(t.getOptionExpireDate(), DATE_TIME_FMT));
            xaction.setOptionStrike(t.getOptionStrike());
            xaction.setOptionType(TdaTransaction.OptionType.parse(t.getOptionType()));
            LocalDate expirationDate = ZonedDateTime.parse(t.getOptionExpireDate(), DATE_TIME_FMT).toLocalDate();
            Option opt = Option.builder()
                    .underlying(t.getOptionUnderlyingSymbol())
                    .expirationDate(expirationDate)
                    .strike(Double.parseDouble(t.getOptionStrike()))
                    .right(TdaTransaction.OptionType.parse(t.getOptionType()).asJtdaOption())
                    .build();
            xaction.setOption(opt);
        }
        if (!StringUtils.isEmpty(t.getOpenClose())) {
            xaction.setEffect(Effect.parse(t.getOpenClose()));
        }
        if (!StringUtils.isEmpty(t.getSharesAfter())) {
            xaction.setSharesAfter(Double.parseDouble(t.getSharesAfter()));
        }
        if (!StringUtils.isEmpty(t.getSharesBefore())) {
            xaction.setSharesBefore(Double.parseDouble(t.getSharesBefore()));
        }
        return xaction;
    }

    private static String buildRow(final TdaTransaction t) {
        return DATE_TIME_FORMAT.format(t.getExecutedDate()) + DELIMITER
                + (t.getOrderDateTime() != null ? DATE_TIME_FORMAT.format(t.getOrderDateTime()) : "") + DELIMITER
                + t.getGroupId() + DELIMITER
                + t.getLeg() + DELIMITER
                + t.getType() + DELIMITER
                + t.getAssetType() + DELIMITER
                + t.getSymbol() + DELIMITER
                + t.getDescription() + DELIMITER
                + t.getSymbolDescription() + DELIMITER
                + t.getCusip() + DELIMITER
                + t.getPrice() + DELIMITER
                + t.getQuantity() + DELIMITER
                + t.getEffect() + DELIMITER
                + t.getValue() + DELIMITER
                + t.getCommission() + DELIMITER
                + t.getFees() + DELIMITER
                + t.getAdditionalFees() + DELIMITER
                + (t.getOption() == null ? "" : t.getOption().toString()) + DELIMITER
                + t.getOptionUnderlyingCusip() + DELIMITER
                + (t.getOptionExpirationDate() != null ? DATE_TIME_FORMAT.format(t.getOptionExpirationDate()) : "") + DELIMITER
                + t.getOptionStrike() + DELIMITER
                + t.getOptionType() + DELIMITER
                + t.getSharesBefore() + DELIMITER
                + t.getSharesAfter() + DELIMITER
                + t.getSplitRatio() + DELIMITER
                + t.getAccountNumber() + DELIMITER;
    }

    //Construct a unique identifier for a transaction or group
    //of transactions executed under the same order. The returned
    //list contains the groupId as the first element and optionally
    //a second element representing the leg number within that group.
    //
    //For TDAmeritrade order numbers start with
    //a 'T' followed by the order number optionally followed by a period
    //followed by 1,2,3 (the leg number). Transactions with a null
    //order number will be given by an 'X' following by the transactionId.
    private static List<String> buildGroupId(final generated.Transaction t) {
        if (StringUtils.isEmpty(t.getOrderNumber())) {
            return Collections.singletonList(JTDA_ID_NON_ORDER_PREFIX + t.getTransactionId());
        }
        if (t.getOrderNumber().startsWith(JTDA_ID_NON_ORDER_PREFIX)) {
            LOG.warn("jtda expects broker order numbers to start with anything except '{}'",
                    JTDA_ID_NON_ORDER_PREFIX);
        }
        List<String> result = Arrays.asList(t.getOrderNumber().split("\\."));
        switch (result.size()) {
            case 1:
                //contains order number
                break;
            case 2:
                //contains order number and leg number
                break;
            default:
                LOG.warn("unable to parse orderNumber: {}", t.getOrderNumber());
                break;
        }
        return result;
    }

    private static String buildColumnHeaderRow() {
        return "ExecutedDate" + DELIMITER
                + "OrderDateTime" + DELIMITER
                + "JtdaId" + DELIMITER
                + "Leg" + DELIMITER
                + "Type" + DELIMITER
                + "AssetType" + DELIMITER
                + "Symbol" + DELIMITER
                + "Description" + DELIMITER
                + "SymbolDescription" + DELIMITER
                + "Cusip" + DELIMITER
                + "Price" + DELIMITER
                + "Quantity" + DELIMITER
                + "Effect" + DELIMITER
                + "Value" + DELIMITER
                + "Commission" + DELIMITER
                + "Fees" + DELIMITER
                + "AdditionalFees" + DELIMITER
                + "Option" + DELIMITER
                + "OptionUnderlyingCusip" + DELIMITER
                + "OptionExpireDate" + DELIMITER
                + "OptionStrike" + DELIMITER
                + "OptionType" + DELIMITER
                + "SharesBefore" + DELIMITER
                + "SharesAfter" + DELIMITER
                + "SplitRatio" + DELIMITER
                + "AccountNo" + DELIMITER;
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

}
