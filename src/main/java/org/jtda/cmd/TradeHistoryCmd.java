package org.jtda.cmd;

import generated.History;
import generated.Transaction;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.HistoryXmlHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;

public class TradeHistoryCmd extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(TradeHistoryCmd.class);

    private static final String DELIMITER = ",";
    private static final String NAME = "History";
    private static final ResponseHandler<History> HANDLER = new HistoryXmlHandler();
    private Session session;

    enum Type {
        ALL(0),
        TRADES(1),
        BUY(2),
        SELL(3),
        DEPOSITS_AND_WITHDRAWALS(4),
        CHECKING(5),
        DIVIDENDS(6),
        INTEREST(7),
        OTHER(8);

        private final int code;

        Type(int code) {
            this.code = code;
        }

        @Override
        public String toString() {
            return "" + code;
        }
    }

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .addParameter("type", Type.ALL.toString()) //all types
                    .addParameter("startdate", LocalDate.now().minusDays(370).toString().replace("-", ""))
                    .build();
            System.out.println(httpget.getRequestLine());
//            String responseBody = httpclient.execute(httpget, stringResponseHandler);
//            System.out.println(XmlUtil.format(responseBody));
            History h = httpclient.execute(httpget, HANDLER);
            System.out.println(h.getStartDate() + " to " + h.getEndDate());
            System.out.println("execDate" + DELIMITER + "orderNo" + DELIMITER + "leg" + DELIMITER
                    + "action" + DELIMITER + "effect" + DELIMITER + "type" + DELIMITER + "subType" + DELIMITER
                    + "qty" + DELIMITER + "symbol" + DELIMITER + "desc" + DELIMITER
                    + "commAndFees" + DELIMITER + "price" + DELIMITER + "value");
            for (Transaction t : h.getTransactionList()) {
                StringBuilder sb = new StringBuilder();
                double commAndFees = t.getCommission() + t.getFees() + t.getAdditionalFees();

                sb.append(t.getExecutedDate()).append(DELIMITER);
                if (StringUtils.isEmpty(t.getOrderNumber())) {
                    sb.append("").append(DELIMITER)
                            .append("").append(DELIMITER);
                } else {
                    String[] orderNoArray = t.getOrderNumber().split("\\.");
                    if (orderNoArray.length == 2) {
                        sb.append(orderNoArray[0]).append(DELIMITER)
                                .append(orderNoArray[1]).append(DELIMITER);
                    } else if (orderNoArray.length == 1) {
                        //assume leg zero (empty)
                        sb.append(orderNoArray[0]).append(DELIMITER)
                                .append("").append(DELIMITER);
                    }
                }
                if (StringUtils.isEmpty(t.getBuySellCode())) {
                    sb.append("").append(DELIMITER);
                } else {
                    sb.append(t.getBuySellCode()).append(DELIMITER);
                }
                sb.append(t.getOpenClose()).append(DELIMITER)
                        .append(t.getType()).append(DELIMITER)
                        .append(t.getSubType()).append(DELIMITER)
                        .append(t.getQuantity()).append(DELIMITER)
                        .append(t.getSymbol()).append(DELIMITER)
                        .append(t.getDescription()).append(DELIMITER)
                        .append(commAndFees).append(DELIMITER)
                        .append(t.getPrice()).append(DELIMITER)
                        .append(t.getValue());
                System.out.println(sb.toString());
            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

}
