package org.jtda.cmd;

public class SessionCmd extends CommandBase implements Command, Stateful {

    private Session session;

    public void execute() throws CommandException {
        if (session != null) {
            System.out.println(session.toString());
        }
    }

    @Override
    public void setSession(Session session) {
        this.session = session;
    }
}