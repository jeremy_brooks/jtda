package org.jtda.cmd;

import org.apache.commons.cli.ParseException;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class CommandBase {
    private static final Logger LOG = LoggerFactory.getLogger(CommandBase.class);
    /**
     * A properties file containing the following:
     * <pre>
     * server=servername.com
     * path=/path/to/
     * source=yourSourceID
     * version=yourVersion
     * userid=clientUsername
     * password=clientPassword
     * </pre>
     * where server and path will be used to represent a request url
     * Example:
     * the LogIn request base url = https://server + path + "LogIn"
     * the QuoteCmd request base url = https://server + path + "QuoteCmd"
     * Note: path must start and end with a slash.
     */
    private static final String SERVER = System.getProperty("server", "apis.tdameritrade.com");
    private static final String API_PATH = System.getProperty("path", "/apps/300/");
    private static final String SOURCE = System.getProperty("source");
    private static final String VERSION = System.getProperty("version");

    private static final String USER_PROPERTIES = Paths.get(System.getProperty("user.home"), "jtda.properties").toString();
    static final String USER_DATA_DIR = Paths.get(System.getProperty("user.home"), "jtdaData").toString();
    private static final String USR_PROPERTY = "userid";
    private static final String PWD_PROPERTY = "password";
    protected static final String SOURCE_PROPERTY = "source";
    private static final String VERSION_PROPERTY = "version";
    protected CloseableHttpClient httpclient;
    private CookieStore cookieStore;
    private RequestConfig defaultRequestConfig;

    private static String createServerUri() {
        return "https://" + SERVER + addSlashIfNeeded(API_PATH);
    }

    protected static String createServerUri(String sessionId) {
        return createServerUri() + ";jessionid=" + sessionId;
    }

    private static String addSlashIfNeeded(String path) {
        if (path.charAt(path.length() - 1) != '/') {
            return path + '/';
        }
        return path;
    }

    public CommandBase() {
        super();
    }

    protected String getName() {
        return "";
    }

    public void parseArguments(String commandName, String[] args) throws ParseException {
        LOG.warn(getName() + " does not support argument parsing yet!");
    }

    public CloseableHttpClient getHttpclient() {
        return httpclient;
    }

    public void setHttpclient(final CloseableHttpClient httpclient) {
        this.httpclient = httpclient;
    }

    public CookieStore getCookieStore() {
        return cookieStore;
    }

    public void setCookieStore(final CookieStore cookieStore) {
        this.cookieStore = cookieStore;
    }

    public RequestConfig getDefaultRequestConfig() {
        return defaultRequestConfig;
    }

    public void setDefaultRequestConfig(final RequestConfig defaultRequestConfig) {
        this.defaultRequestConfig = defaultRequestConfig;
    }

    protected static Properties loadUserProperties(String file)
            throws IOException, CommandException {
        Properties p = new Properties();
        p.load(new FileInputStream(new File(file)));
        assertPropertyOrThrow(USR_PROPERTY, p);
        assertPropertyOrThrow(PWD_PROPERTY, p);
        assertPropertyOrThrow(SOURCE_PROPERTY, p);
        assertPropertyOrThrow(VERSION_PROPERTY, p);

        //		String path = p.getProperty(PATH_PROPERTY);
        //		boolean doesNotStartsWithSlash = !"/".equals(path.charAt(0));
        //		boolean doesNotEndWithSlash = !"/".equals(path.charAt(path.length()-1));
        //		if(doesNotStartsWithSlash || doesNotEndWithSlash)
        //			throw new Exception(USER_PROPERTIES + ": path must start and end with '/' character");
        return p;
    }

    private static void assertPropertyOrThrow(String propName, Properties props) throws CommandException {
        boolean usrNotFound = !(props.containsKey(propName) && props.get(propName) != null);
        if (usrNotFound)
            throw new CommandException(USER_PROPERTIES + ": " + propName + " is required and must contain a non-empty value");
    }

    static List<NameValuePair> toUrlEncodedNameValuePairs(Properties props) {
        List<NameValuePair> pairs = Collections.emptyList();
        if (props != null) {
            pairs = new ArrayList<>(props.size());
            for (Object key : props.keySet()) {
                String name = key.toString();
                String value = props.getProperty(name);
                try {
                    NameValuePair pair = new BasicNameValuePair(URLEncoder.encode(name, "UTF-8"),
                            URLEncoder.encode(value, "UTF-8"));
                    pairs.add(pair);
                    if (pair.getName().equals(USR_PROPERTY)) System.out.println("Login: " + pair.toString());
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return pairs;
    }

    protected URI getUriForCommand(String commandName, String version) {
        StringBuilder apiPath = new StringBuilder("/apps/");
        apiPath.append(version).append("/");
        apiPath.append(commandName);
        try {
            URIBuilder ub = new URIBuilder();
            return ub.setScheme("https")
                    .setHost(SERVER)
                    .setPath(apiPath.toString())
                    .build();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

}