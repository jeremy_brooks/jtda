package org.jtda.cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jeremy on 6/3/18.
 */
public class Frequency {
    private static final Logger LOG = LoggerFactory.getLogger(Frequency.class);
    private TdaInterval type;
    private int value;

    public Frequency(int value, TdaInterval type) {
        super();
        this.type = type;
        this.value = value;
    }

    public TdaInterval getType() {
        return type;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "" + value + type.toString();
    }

    public static Frequency parse(String frequency) {
        int barFreq;
        TdaInterval interval;
        if (frequency.endsWith("min")) {
            interval = TdaInterval.MINUTE;
            barFreq = Integer.parseInt(frequency.replace("min", ""));
        } else if (frequency.endsWith("d")) {
            interval = TdaInterval.DAILY;
            barFreq = Integer.parseInt(frequency.replace("d", ""));
        } else if (frequency.endsWith("w")) {
            interval = TdaInterval.WEEKLY;
            barFreq = Integer.parseInt(frequency.replace("w", ""));
        } else if (frequency.endsWith("mon")) {
            interval = TdaInterval.MONTHLY;
            barFreq = Integer.parseInt(frequency.replace("mon", ""));
        } else {
            //throw new IllegalArgumentException("unknown interval: "+interval);
            interval = TdaInterval.MINUTE;
            barFreq = Integer.parseInt(frequency);
            LOG.info("assuming " + interval + " frequency units");
        }

        switch (interval) {
            case MINUTE:
                switch (barFreq) {
                    case 1:
                    case 5:
                    case 10:
                    case 15:
                    case 30:
                    case 60:
                        break;
                    default:
                        throw new IllegalArgumentException("unsupported " + interval + ": " + barFreq);
                }
                break;
            case DAILY:
                switch (barFreq) {
                    case 1:
                        break;
                    default:
                        throw new IllegalArgumentException("unsupported " + interval + ": " + barFreq);
                }
                break;
            case WEEKLY:
                switch (barFreq) {
                    case 1:
                        break;
                    default:
                        throw new IllegalArgumentException("unsupported " + interval + ": " + barFreq);
                }
                break;
            case MONTHLY:
                switch (barFreq) {
                    case 1:
                        break;
                    default:
                        throw new IllegalArgumentException("unsupported " + interval + ": " + barFreq);
                }
                break;
        }
        return new Frequency(barFreq, interval);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Frequency frequency = (Frequency) o;

        if (getValue() != frequency.getValue()) return false;
        return getType() == frequency.getType();
    }

    @Override
    public int hashCode() {
        int result = getType() != null ? getType().hashCode() : 0;
        result = 31 * result + getValue();
        return result;
    }
}