package org.jtda.cmd;

public interface Stateful {
    void setSession(Session session);
}
