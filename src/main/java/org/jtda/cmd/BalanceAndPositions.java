package org.jtda.cmd;

import generated.Balances;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.BalanceXmlHandler;
import org.jtda.model.AccountBalance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDateTime;

public class BalanceAndPositions extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(BalanceAndPositions.class);
    private static final String NAME = "BalancesAndPositions";
    private static final ResponseHandler<Balances> HANDLER = new BalanceXmlHandler();
    private Session session;

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .addParameter("suppressquotes", "true") //TODO: xml won't parse if false (unexpected element put-call)
                    .build();
            LOG.info("" + httpget.getRequestLine());
//            String responseBody = httpclient.execute(httpget, SHANDLER);
//            System.out.println(XmlUtil.format(responseBody));
            Balances b = httpclient.execute(httpget, HANDLER);

            BigDecimal cash = b.getCashBalance().getCurrent();
            BigDecimal netLiq = b.getAccountValue().getCurrent();
            BigDecimal available = b.getAvailableFundsForTrading();
            BigDecimal obp = b.getOptionBuyingPower();
            BigDecimal longStock = b.getLongStockValue().getCurrent();
            BigDecimal shortStock = b.getShortStockValue().getCurrent();
            BigDecimal longOptions = b.getLongOptionValue().getCurrent();
            BigDecimal shortOptions = b.getShortOptionValue().getCurrent();
            BigDecimal usedObp = cash.subtract(obp);
            double usedObpPercentage = usedObp.doubleValue() / cash.doubleValue() * 100;
            double percentCash = cash.doubleValue() / netLiq.doubleValue() * 100;
            double obpPercentage = obp.doubleValue() / cash.doubleValue() * 100;
            double capitalAllocation = Math.abs(1 - (obp.doubleValue() / netLiq.doubleValue())) * 100;
            double cushion = obp.doubleValue() / netLiq.doubleValue();

            AccountBalance ab = AccountBalance.builder()
                    .accountName("myacct")
                    .timestamp(LocalDateTime.now())
                    .netLiq(netLiq)
                    .available(available)
                    .cash(cash)
                    .moneyMarket(b.getMoneyMarketBalance().getCurrent())
                    .longStock(longStock)
                    .shortStock(shortStock)
                    .longOption(longOptions)
                    .shortOption(shortOptions)
                    .stockBuyingPower(b.getStockBuyingPower())
                    .optionBuyingPower(b.getOptionBuyingPower())
                    .maintenanceRequirement(b.getMaintenanceRequirement().getCurrent())
                    .build();
            System.out.println(ab.asCsv());
            System.out.println();
            System.out.println("NetLiq      : " + netLiq);
            System.out.printf("Cushion     : %2.0f%% = OBP/NetLiq%n", cushion);
            System.out.printf("CapitalAlloc: %2.0f%% = abs(1 - OBP/NetLiq)%n", capitalAllocation);
            System.out.println("Available   : " + available.toString());
            System.out.println("Cash Balance: " + cash);
            System.out.println("Option BP   : " + obp.toString());
            System.out.printf("Option BP %% : %2.0f%%%n", obpPercentage);
            System.out.println("Short option: " + shortOptions);
            System.out.println("Long option : " + longOptions);
            System.out.println("Short stock : " + shortStock);
            System.out.println("Long stock  : " + longStock);
            System.out.println("Maint Rqmt  : " + b.getMaintenanceRequirement().getCurrent());
            System.out.println("Used OBP    : " + usedObp);
            System.out.printf("Used OBP %%  : %2.0f%%%n", usedObpPercentage);
            System.out.printf("%% in Cash   : %2.0f%%%n", percentCash);

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

}
