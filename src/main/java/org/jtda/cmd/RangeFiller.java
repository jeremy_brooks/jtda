package org.jtda.cmd;

import org.jtda.model.VolHistory;

/**
 * Created by jeremy on 6/3/18.
 */
public interface RangeFiller {

    enum FillFrequency {
        DAILY
    }

    /**
     * Returns a new VolHistory where missing bars are populated with a date/time but an empty value.
     * @param volHistory the history to fill in
     * @return a "filled-in" vol history
     */
    VolHistory fill(VolHistory volHistory, FillFrequency fillFrequency);
}
