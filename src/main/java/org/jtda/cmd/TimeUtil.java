package org.jtda.cmd;

import org.jtda.model.OptionQuote;
import org.jtda.model.Strike;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MINUTES;

/**
 * Created by jeremy on 06-09-2018.
 */
public class TimeUtil {
    private static final Logger LOG = LoggerFactory.getLogger(TimeUtil.class);

    private static final DecimalFormat EIGHT_DECIMAL_PLACES = new DecimalFormat("#.00000000");

    /**
     * Computes the time to expiration (in units of years).
     *
     * The VIX white paper describes this calculation as
     * <pre>
     *
     *    t = ( M<sub>Current day</sub> + M<sub>Settlement day</sub> + M<sub>Other days</sub> ) / M<sub>perYear</sub>
     *
     *      OR as we define it which may be more clear,
     *
     *    t = ( Mum + Mfm + Mib ) / MPY
     *
     *    where,
     *      Mum = Minutes Until Midnight of the current day
     *      Mfm = Minutes From Midnight to closing/settlement on lastDay of trading
     *      Mib = Minutes In Between the midnights
     *      MPY = Minutes Per Year (525,600)
     * </pre>
     * NOTE: This implementation uses Durations and as these are estimated,
     * daylight savings, leap year/seconds won't be accounted for.
     *
     * @param start the current date time
     * @param lastDayOfTrading the last day of trading for the contract (aka, expiration)
     * @return
     */
    public static double timeToExpiryInYears(LocalDateTime start, LocalDate lastDayOfTrading){
        return ((double) estimatedMinutesBetween(start, lastDayOfTrading)) / Duration.ofDays(365).toMinutes();
    }


    /**
     * NOTE: This implementation uses Durations and as these are estimated,
     * daylight savings, leap year/seconds won't be accounted for.
     *
     * @param start the current date time
     * @param lastDayOfTrading the last day of trading for the contract (aka, expiration)
     * @return estimated minutes between start and lastDayOfTrading at 4pm when market closes.
     */
    public static long estimatedMinutesBetween(LocalDateTime start, LocalDate lastDayOfTrading){
        LOG.debug("\nstart={}\nlastD={}\n", start, lastDayOfTrading);

        //compute remaining Minutes Til Midnight tonight
        //or Mcd in VIX terminology.
        LocalDateTime nextMidnight = start.toLocalDate()
                .plus(1, DAYS).atStartOfDay();
        long minutesTillMidnight = MINUTES.between(start, nextMidnight);

        //compute minutes until close of trading on last day
        //or Msd in VIX terminology.
        LocalTime closing = LocalTime.of(16,00, 00);
        LocalDateTime lastDayAtClose = lastDayOfTrading.atTime(closing);
        LocalDateTime lastDayAtMidnight = lastDayOfTrading.atStartOfDay();
        long minutesOnLastDay = MINUTES.between(lastDayAtMidnight, lastDayAtClose);

        //compute minutes in between nextMidnight and midnight of lastTradingDay
        //or Mod in VIX terminology.
        long fullDaysInMinutes = MINUTES.between(nextMidnight, lastDayAtMidnight);
        LOG.debug("\nMcd={}\nMsd={}\nMod={} ({} days)", minutesTillMidnight, minutesOnLastDay,
                fullDaysInMinutes, fullDaysInMinutes/1440);

        long minutesOnStartAndEndDays = Math.addExact(minutesTillMidnight, minutesOnLastDay);
        long totalMinutes = Math.addExact(minutesOnStartAndEndDays, fullDaysInMinutes);
        LOG.debug("minutesBetween={}", totalMinutes);
        return totalMinutes;
    }


    public static double computeForwardStrike(Strike baseStrike, double riskFreeRate, double timeToExpiry){
        //Compute:
        // F = Strike Price + e^(R*T) x (Call Price - Put Price)
        //using BigDecimal math for midQuote calculations.
        //
        // https://www.javaworld.com/article/2073176/caution--double-to-bigdecimal-in-java.html
        // Do we really need to do BigDecimal arithmetic or will imprecise double math work
        // for our purposes here? NOTE: Using double math for calculating mid price fails our
        // TimeUtil tests for this method which aligns it with the calculation results
        // in the vix white paper.
        OptionQuote call = baseStrike.getCallContract().getQuote();
        OptionQuote put = baseStrike.getPutContract().getQuote();
        if(call == null || put == null){
            throw new IllegalArgumentException("can't compute no quotes");
        }
        BigDecimal callMid = quoteMidPrice(call);
        BigDecimal putMid = quoteMidPrice(put);
        double callMinusPutPrice = callMid.subtract(putMid).doubleValue();
        double rt = riskFreeRate * timeToExpiry;
        double eRT = Math.pow(Math.E, rt);

        LOG.info("C price   = {}", callMid);
        LOG.info("P price   = {}", putMid);
        LOG.info("Strike    = {}", baseStrike.getStrikePrice());
        LOG.info("R*T       = {}", rt);
        LOG.info("e^R*T     = {}", eRT);
        LOG.info("C-P Price = {}", callMinusPutPrice);
        String formula = "F = {} + {}^({} * {}) * ({} - {})";
        LOG.info(formula,
                baseStrike.getStrikePrice(),
                EIGHT_DECIMAL_PLACES.format(Math.E),
                EIGHT_DECIMAL_PLACES.format(riskFreeRate),
                EIGHT_DECIMAL_PLACES.format(timeToExpiry),
                callMid,
                putMid
        );
        double x = eRT * callMinusPutPrice;
        LOG.info("eRT * callMinusPutPrice = {}", x);
        return baseStrike.getStrikePrice() + x;
    }

    private static BigDecimal quoteMidPrice(OptionQuote quote){
        BigDecimal bid = BigDecimal.valueOf(quote.getBid());
        BigDecimal ask = BigDecimal.valueOf(quote.getAsk());
        return midPrice(bid, ask);
    }

    private static BigDecimal midPrice(BigDecimal bid, BigDecimal ask){
        return bid.add(ask).divide(BigDecimal.valueOf(2.0d));
    }

    /**
     * https://www.investopedia.com/articles/optioninvestor/03/090303.asp
     *
     * Standardized options expire on the Saturday following the third Friday of the month.
     */
    public static List<LocalDate> computeStandardExpirationMonths(LocalDate today){
        //There will always be at least 4 months trading
        // 1) this month
        // 2) next month
        // 3) the next quarterly month (Jan, Mar, July, Oct)
        // 4) the following quarterly month (Jan, Mar, July, Oct)
        List<LocalDate> expirations = new ArrayList<>();
        LocalDate thisMonth = computeThirdFridayAfter(today);
        expirations.add(thisMonth);
        LocalDate nextMonth = computeThirdFridayAfter(thisMonth.plusDays(1));
        expirations.add(nextMonth);
        //TODO: compute 3rd and 4th months
        return expirations;
    }

    /**
     * Compute the third Friday of the given date.
     * @param date
     * @return
     */
    public static LocalDate computeThirdFridayOf(LocalDate date){
        return date.with(TemporalAdjusters.dayOfWeekInMonth(3, DayOfWeek.FRIDAY));
    }

    /**
     * Compute the third Friday on or after the given date
     * @param date the date to start
     * @return the third Friday on or after the given date
     */
    public static LocalDate computeThirdFridayAfter(LocalDate date){
        LocalDate d = computeThirdFridayOf(date);
        if(d.isBefore(date)){
            d = computeThirdFridayOf(date.plusMonths(1));
        }
        return d;
    }


}
