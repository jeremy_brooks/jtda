package org.jtda.cmd;

import generated.Position;
import generated.Position.Security;
import generated.Positions;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.PositionsXmlHandler;
import org.jtda.model.PositionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PositionsCmd extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(PositionsCmd.class);
    private static final String NAME = "BalancesAndPositions";
    private static final ResponseHandler<Positions> HANDLER = new PositionsXmlHandler();
    private Session session;

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .addParameter("suppressquotes", "false") //TODO: xml won't parse if false (unexpected element put-call)
                    .build();
            LOG.info("" + httpget.getRequestLine());
//            String responseBody = httpclient.execute(httpget, stringResponseHandler);
//            System.out.println(XmlUtil.format(responseBody));
            Positions pos = httpclient.execute(httpget, HANDLER);
            final String formatStringHeader = "%4s%4s%15s%5s%10s%10s%10s%10s%8s%8s%8s%8s%7s%10s%n";
            //final String formatString =       "%4.0f %-3s%15s%5s%10.2f%10.2f%10.2f%8.2f%8.2f%8.2f%8.2f%7.2f%10.0f%n";
            System.out.format(formatStringHeader, "Qty", "Pos", "Symbol", "DTE",
                    "Current", "Close", "Avg", "PnL", "delta", "gamma", "theta", "vega", "IV", "MaintReq");
            double deltaSum = 0, gammaSum = 0, thetaSum = 0, vegaSum = 0, maintReqSum = 0;
            BigDecimal valueSum = new BigDecimal("0");
            BigDecimal pnlSum = new BigDecimal("0");
            //need to add stocks too :)  => pos.getStocks().getPositions()
            List<PositionInfo> positions = new ArrayList<>();
            for (Position p : pos.getStocks().getPosition()) {
                Security security = p.getSecurity();
                BigDecimal currentValue = p.getCurrentValue();
                BigDecimal closePrice = p.getClosePrice();
                BigDecimal avgPrice = p.getAveragePrice();
                BigDecimal qty = p.getQuantity();
                BigDecimal multiplier = p.getQuote().getMultiplier();
                String posType = p.getPositionType().substring(0, 1);
                String symbol = security.getSymbol();
                String dte = "";
                Double delta = qty.doubleValue();
                Double gamma = 0.0;
                Double theta = 0.0;
                Double vega = 0.0;
                Double iv = p.getQuote().getImpliedVolatility();
                BigDecimal maintReq = p.getMaintenanceRequirement();

                PositionInfo pi = buildPositionInfo(multiplier, currentValue, closePrice, avgPrice, qty, posType,
                        symbol, dte, delta, gamma, theta, vega, iv, maintReq);
                positions.add(pi);

                deltaSum += delta;
                gammaSum += gamma;
                thetaSum += theta;
                vegaSum += vega;
                maintReqSum += maintReq.doubleValue();
                valueSum = valueSum.add(currentValue);
                pnlSum = pnlSum.add(pi.getProfitAndLoss());
            }
            for (Position p : pos.getOptions().getPosition()) {
                Security security = p.getSecurity();
                BigDecimal currentValue = p.getCurrentValue();
                BigDecimal closePrice = p.getClosePrice();
                BigDecimal avgPrice = p.getAveragePrice();
                BigDecimal qty = p.getQuantity();
                BigDecimal multiplier = p.getQuote().getMultiplier();
                String posType = p.getPositionType().substring(0, 1);
                String symbol = security.getSymbol();
                String dte = p.getQuote().getDaysToExpiration().toString();
                Double delta = computeTdaGreek(p.getQuote().getDelta() * qty.doubleValue());
                Double gamma = computeTdaGreek(p.getQuote().getGamma() * qty.doubleValue());
                Double theta = computeTdaGreek(p.getQuote().getTheta() * qty.doubleValue());
                Double vega = computeTdaGreek(p.getQuote().getVega() * qty.doubleValue());
                Double iv = p.getQuote().getImpliedVolatility();
                BigDecimal maintReq = p.getMaintenanceRequirement();

                PositionInfo pi = buildPositionInfo(multiplier, currentValue, closePrice, avgPrice, qty, posType,
                        symbol, dte, delta, gamma, theta, vega, iv, maintReq);
                positions.add(pi);

                deltaSum += delta;
                gammaSum += gamma;
                thetaSum += theta;
                vegaSum += vega;
                maintReqSum += maintReq.doubleValue();
                valueSum = valueSum.add(currentValue);
                pnlSum = pnlSum.add(pi.getProfitAndLoss());
            }
            List<PositionInfo> sortedPositions = positions.stream()
                    .sorted()
                    .collect(Collectors.toList());
            for (PositionInfo info : sortedPositions) {
                System.out.println(info.toString());
            }
            final String formatTotal = "%4s%4s%15s%5s%10.2f%10s%10s%10.2f%8.2f%8.2f%8.2f%8.2f%7s%10.0f%n";
            System.out.format(formatTotal, "", "", "Totals", "",
                    valueSum, "", "", pnlSum, deltaSum, gammaSum, thetaSum, vegaSum, "", maintReqSum);

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    private PositionInfo buildPositionInfo(BigDecimal multiplier, BigDecimal currentValue, BigDecimal closePrice, BigDecimal avgPrice, BigDecimal qty, String posType, String symbol, String dte, Double delta, Double gamma, Double theta, Double vega, Double iv, BigDecimal maintReq) {
        PositionInfo pi = new PositionInfo();
        pi.setQuantity(qty);
        pi.setPosition(posType);
        pi.setSymbol(symbol);
        pi.setDte(dte);
        pi.setCurrentValue(currentValue);
        pi.setClosePrice(closePrice);
        pi.setAvgPrice(avgPrice);
        pi.setProfitAndLoss(closePrice.subtract(avgPrice).multiply(multiplier).multiply(qty));
        pi.setDelta(delta);
        pi.setTheta(theta);
        pi.setGamma(gamma);
        pi.setVega(vega);
        pi.setIv(iv);
        pi.setMaintenanceRequirement(maintReq);
        return pi;
    }


    private double computeTdaGreek(double actualGreek) {
        return 100 * actualGreek;
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }
}
