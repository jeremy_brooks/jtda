package org.jtda.cmd;

import generated.Account;
import generated.LoginResult.Accounts;
import generated.StreamerInfo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.http.client.CookieStore;
import org.jtda.model.Strike;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Properties;

public class DefaultSession implements Session {

    private String sessionId;
    private String userId = "";
    private Long associatedAccountId;
    private List<Account> accounts;
    private StreamerInfo streamerInfo;
    private CookieStore cookieStore;
    private Properties config;
    private ZonedDateTime loginDateTime;
    private int timeoutInMinutes;
    private List<Strike> lastLiquidStrikes;
    private String lastOrderId;

    @Override
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }

    @Override
    public List<Account> getAccounts() {
        return accounts;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public void setAssociatedAccountId(Long associatedAccountId) {
        this.associatedAccountId = associatedAccountId;
    }

    public Long getAssociatedAccountId() {
        return associatedAccountId;
    }

    @Override
    public void setAccounts(Accounts accounts) {
        this.accounts = accounts.getAccount();
    }

    @Override
    public StreamerInfo getStreamerInfo() {
        return streamerInfo;
    }

    @Override
    public void setStreamerInfo(StreamerInfo info) {
        streamerInfo = info;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("sessionId", sessionId)
                .append("userId", userId)
                .append("associatedAccountId", associatedAccountId)
                .append("accounts", accounts)
                .append("streamerInfo", streamerInfo)
                .append("cookieStore", cookieStore)
                .append("config", config)
                .append("loginDateTime", loginDateTime)
                .append("timeoutInMinutes", timeoutInMinutes)
                .append("lastLiquidStrikes", lastLiquidStrikes)
                .toString();
    }

    @Override
    public void setConfiguration(Properties config) {
        this.config = config;

    }

    @Override
    public Properties getConfiguration() {
        return config;
    }

    @Override
    public void invalidate() {
        sessionId = null;
    }

    @Override
    public boolean valid() {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("America/New_York"));
        if (now.isAfter(loginDateTime.plusMinutes(timeoutInMinutes))) {
            invalidate();
            return false;
        }
        return true;
    }

    @Override
    public void setSessionTimeout(final int timeoutInMinutes) {
        this.timeoutInMinutes = timeoutInMinutes;
    }

    @Override
    public int getSessionTimeout() {
        return timeoutInMinutes;
    }

    @Override
    public void setLoginTime(final ZonedDateTime loginDateTime) {
        this.loginDateTime = loginDateTime;
    }

    @Override
    public ZonedDateTime getLoginTime() {
        return loginDateTime;
    }

    @Override
    public void addLastLiquidStrikes(final List<Strike> mostLiquidStrikes) {
        this.lastLiquidStrikes = mostLiquidStrikes;
    }

    @Override
    public List<Strike> getLastLiquidStrikes() {
        return lastLiquidStrikes;
    }

    @Override
    public String getLastOrderId() {
        return this.lastOrderId;
    }

    @Override
    public void setLastOrderId(String orderId) {
        this.lastOrderId = orderId;
    }

    @Override
    public boolean expired() {
        return !valid();
    }
}
