package org.jtda.cmd;

import org.apache.commons.cli.ParseException;

public interface Command {
	void execute() throws CommandException;
    void parseArguments(String commandName, String[] args) throws ParseException;
}
