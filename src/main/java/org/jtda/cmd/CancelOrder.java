package org.jtda.cmd;

import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.util.EntityUtils;
import org.jtda.model.XmlNode;
import org.jtda.model.XmlNodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

/**
 * Cancels a given order or with no arguments cancels the order saved in session.getLastOrderId().
 */
public class CancelOrder extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(OrderStatusCmd.class);
    private static final String NAME = "OrderCancel";
    private static final ResponseHandler<XmlNode> HANDLER = (HttpResponse response) -> {
                String xml = EntityUtils.toString(response.getEntity());
                return new XmlNodeBuilder(xml).getRoot();
    };
    private Session session;
    private String orderId;

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        Options options = new Options();
        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);


        if (cmdLine.getArgs().length == 1) {
            orderId = cmdLine.getArgs()[0]; //first argument
        } else {
            System.out.println("setting to last order id: " + session.getLastOrderId());
            orderId = session.getLastOrderId();
        }
    }

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .addParameter("orderid", orderId)
                    .build();
            LOG.info("" + httpget.getRequestLine());
            XmlNode root = httpclient.execute(httpget, HANDLER);
            if ("OK".equals(root.getChildWithNameNonNull("result").getValue())) {
                XmlNode cancelOrderMessages = root.getChildWithName("cancel-order-messages");
                XmlNode[] orderList = cancelOrderMessages.getChildrenWithName("order");
                int index = 0;
                for (XmlNode order : orderList) {
                    String message = order.getChildWithNameNonNull("message").getValue();
                    System.out.println(index++ + ": " + message);
                }
            } else {
                String error = root.getChildWithName("error").getValue();
                System.err.println("Error: " + error);
            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }


    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

}
