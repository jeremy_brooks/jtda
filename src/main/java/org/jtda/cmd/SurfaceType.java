package org.jtda.cmd;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SurfaceType {

    /*
     *  DELTA - The ratio comparing the change in the price of the underlying asset
     *          to the corresponding change in the price of a derivative.
     *   The Contract Type will be "CALL" if the delta is positive.
     *   The Contract Type will be "PUT" if the delta is negative.
     *
     *   DELTA_WITH_COMPOSITE - The mean of 2 delta values, one CALL and one PUT
     *
     *   SKEW - Difference between the implied volatilities at 2 delta values.
     *   The Contract Type will be "CALL" if the delta is positive.
     *   The Contract Type will be "PUT" if the delta is negative.
     *
     */
    enum Id {
        DELTA,
        DELTA_WITH_COMPOSITE,
        SKEW
    }

    private SurfaceType.Id id;
    private String value;

    public static SurfaceType parse(String idAndValues) {
        String[] a = idAndValues.split(",");
        SurfaceType.Id stId = SurfaceType.Id.DELTA;
        String val = "-30";
        String parsedType = a[0];
        if ("delta".equalsIgnoreCase(parsedType)) {
            stId = SurfaceType.Id.DELTA;
            val = a[1];
        } else if ("comp".equalsIgnoreCase(parsedType)) {
            stId = SurfaceType.Id.DELTA_WITH_COMPOSITE;
            val = a[1] + "," + a[2];
        } else if ("skew".equalsIgnoreCase(parsedType)) {
            stId = SurfaceType.Id.SKEW;
            val = a[1] + "," + a[2];
        }
        return SurfaceType.builder()
                .id(stId)
                .value(val)
                .build();
    }

    @Override
    public String toString() {
        return id + value;
    }
}