package org.jtda.cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jeremy on 6/3/18.
 */
public class Period {
    private static final Logger LOG = LoggerFactory.getLogger(Period.class);
    private TdaPeriod period;
    private int value;

    public Period(int value, TdaPeriod period) {
        this.value = value;
        this.period = period;
    }


    public TdaPeriod getPeriod() {
        return period;
    }


    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "" + value + period.toString();
    }

    public static Period parse(String periodType) {
        //1d, 2d, 3d, 4d, 5d, 10d
        //1m, 2m, 3m, 6m
        //1y, 2y, 3y, 5y, 10y, 15y, 20y
        //ytd
        TdaPeriod tdaPeriod;
        int value;
        if (periodType.equals("ytd")) { //must be checked first
            tdaPeriod = TdaPeriod.YTD;
            value = 1;
            //return new Period(1, TdaPeriod.YTD);
        } else if (periodType.endsWith("d")) {
            tdaPeriod = TdaPeriod.DAY;
            value = Integer.parseInt(periodType.substring(0, periodType.length() - 1));
        } else if (periodType.endsWith("m")) {
            tdaPeriod = TdaPeriod.MONTH;
            value = Integer.parseInt(periodType.substring(0, periodType.length() - 1));
        } else if (periodType.endsWith("y")) {
            tdaPeriod = TdaPeriod.YEAR;
            value = Integer.parseInt(periodType.substring(0, periodType.length() - 1));
        } else {
            tdaPeriod = TdaPeriod.DAY;
            value = Integer.parseInt(periodType);
            LOG.info("assuming " + tdaPeriod + " period units");
        }
        return new Period(value, tdaPeriod);
    }
}