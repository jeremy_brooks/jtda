package org.jtda.cmd;

import generated.Order;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.TradeOptionResponseHandler;
import org.jtda.model.TradeOptionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;

/**
 *
 * Usage from command line:
 *
 *  tc s STOCK_ORDER
 *  tc o OPTION_ORDER
 *
 *  where STOCK_ORDER is a conditional stock order string
 *  and OPTION_ORDER is a conditional option order string
 *
 * The order submitted by this command is saved to the session with session.setLastOrderId(orderId)
 */
public class TradeConditionally extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(TradeOptionCmd.class);

    private static final String NAME = "ConditionalOptionTrade";
    private static final ResponseHandler<TradeOptionResponse> HANDLER = new TradeOptionResponseHandler();

    private Session session;
    private String servlet;
    private String orderString;

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(servlet, "100");
            HttpUriRequest httpget = buildRequest(uri);
            LOG.info("" + httpget.getRequestLine());
            Instant start = Instant.now();
            TradeOptionResponse orderResponse = httpclient.execute(httpget, HANDLER);
            Instant finish = Instant.now();
            LOG.info("response time: {}", Duration.between(start, finish));
            if(orderResponse.hasOrder()){
                final Order order = orderResponse.getOrder();
                final String displayText = StringUtils.join(
                        new Object[]{ order.getOrderId(),
                                order.getAction(),
                                order.getOpenClose(),
                                order.getQuantity(),
                                order.getSecurity().getDescription(),
                                order.getLimitPrice()}, " ");
                System.out.println(displayText);
                session.setLastOrderId(order.getOrderId());
            } else {
                final String displayText = StringUtils.join(
                        new Object[]{ orderResponse.getErrorMessage(),
                                orderResponse.getOrderString()}, "\n");
                LOG.error(displayText);
                System.err.println(displayText);
            }

        }
        catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        finally {
            IOUtils.closeQuietly(httpclient);
        }
    }


    private HttpUriRequest buildRequest(final URI uri) {
        //tda example for ConditionalEquityTrade
        //ordticket=ota~totlegs=2~action1=buy~quantity1=2~symbol1=ibm~ordtype1=limit~price1=80~actprice1=~tsparam1=~expire1=day~spinstructions1=none~routing1=auto~displaysize1=~exmonth1=~exday1=~exyear1=~action2=buy~quantity2=4~symbol2=bac~ordtype2=limit~price2=20~actprice2=~tsparam2=~expire2=day~spinstructions2=none~routing2=auto~exmonth2=~exday2=~exyear2=~clientorderid=1

        //my example for ConditionalOptionTrade
        //ordticket=ota~totlegs=2~action1=buytoopen~quantity1=2~symbol1=BAC_011521C15~ordtype1=limit~price1=0.01~actprice1=~expire1=day~spinstructions1=none~routing1=auto~exmonth1=~exday1=~exyear1=~action2=buytoopen~quantity2=1~symbol2=BAC_011521C18~ordtype2=limit~price2=0.02~actprice2=~expire2=day~spinstructions2=none~routing2=auto~exmonth2=~exday2=~exyear2=~clientorderid=1
        String orderStr = orderString + "~accountid=" + session.getAssociatedAccountId();
        return RequestBuilder.get()
                .setUri(uri)
                .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                .addParameter("orderstring", orderStr)
                .build();
    }


    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws org.apache.commons.cli.ParseException {
        Options options = new Options();
        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);
        servlet = cmdLine.getArgs()[0].equals("s") ? "ConditionalEquityTrade" : "ConditionalOptionTrade"; //first argument
        orderString = cmdLine.getArgs()[1];
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }



}
