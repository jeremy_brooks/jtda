package org.jtda.cmd;

import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.OptionChainBinaryHandler;
import org.jtda.model.OptionChain;
import org.jtda.model.OptionSeries;
import org.jtda.model.Strike;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Compute the 30-day implied volatility for an underlying (has an option chain)
 * using the VIX methodology (https://www.cboe.com/micro/vix/vixwhite.pdf).
 * <p>
 * VIX uses a near and next series where DTE > 23 days and < 37 days (30 days +/-7 days).
 * Since we want this to work for those without weekly options
 * we'll extrapolate this to use monthly option series with
 * a range of a (4 week) month instead of a week:
 * DTE > 2 days and < 58 days (30 days +/- 28 days)
 *
 * @author jeremy
 */
public class Iv extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(Iv.class);

    private static final String MONTHLY_EXPIRATION = "R";
    private static final int NEAR_TERM_DTE_THRESHOLD = 2;
    private static final int NEXT_TERM_DTE_THRESHOLD = 58;
    private static final int DTE_DIFFERENCE_THRESHOLD = 28;
    private static final String NAME = "BinaryOptionChain";
    private static final ResponseHandler<OptionChain> HANDLER = new OptionChainBinaryHandler();

    private Session session;
    private String symbols;
    private String type; //C, P, VCS, VPS, CCS, CPS, BW, STDL, STGL
    private String expire = "a"; //al, a, l, w
    private String interval; //interval between strikes

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "200");
            HttpUriRequest httpget = buildRequest(uri);
            LOG.info("" + httpget.getRequestLine());
            Instant start = Instant.now();
            OptionChain ocr = httpclient.execute(httpget, HANDLER);
            Instant finish = Instant.now();
            LOG.info("response time: {}", Duration.between(start, finish));
            System.out.println("Symbol : " + ocr.getSymbol());
            System.out.println("Close  : " + ocr.getClose());
            System.out.println("Last   : " + ocr.getLast());
            System.out.println("Bid    : " + ocr.getBid());
            System.out.println("Ask    : " + ocr.getAsk());
            System.out.println("Expirations:");
            //LocalDate now = LocalDate.now();

            OptionSeries nearTermSeries = null;
            OptionSeries nextTermSeries = null;
            SortedSet<LocalDate> expirations = ocr.getOptionExpirations();
            expirations.stream().forEach(s -> System.out.println("expiration: " + s.toString()));
            for (LocalDate expiration : expirations) {
                //for (OptionDate optionSeries : ocr.getOptionDate()) {
                //LocalDate expiration = now.plusDays(optionSeries.getDaysToExpiration());
                long dte = DAYS.between(LocalDate.now(), expiration); //.getDaysToExpiration();
                if (dte > NEAR_TERM_DTE_THRESHOLD && dte < NEXT_TERM_DTE_THRESHOLD) {
                    if (nearTermSeries == null) {
                        nearTermSeries = ocr.getOptionSeries(expiration);
                    } else {
                        long daysDifference = dte - DAYS.between(LocalDate.now(), nearTermSeries.getExpirationDate());
                        if (daysDifference >= DTE_DIFFERENCE_THRESHOLD) {
                            nextTermSeries = ocr.getOptionSeries(expiration);
                        } else {
                            LOG.info("skipping series DTE=" + dte);
                        }
                    }
                }
                if (nearTermSeries != null && nextTermSeries != null) {
                    break;
                }
            }
            LOG.info("today: {}", Instant.now().atZone(ZoneId.of("America/Los_Angeles")).toLocalDateTime());
            Strike nearBaseStrike = null;
            double t1 = Double.NaN;
            Strike nextBaseStrike = null;
            double t2 = Double.NaN;
            if (nearTermSeries != null) {
                nearBaseStrike = computeStrikeWithSmallestCallPutPriceDiff(nearTermSeries);
                t1 = computeTimeToExpiryInYears(nearTermSeries.getExpirationDate());
                LOG.info("nearTerm: {}", nearTermSeries.getDaysToExpiration() + " (" + nearTermSeries.getExpirationDate() + ")");
                LOG.info("T1      : {} (fmt={})", t1, String.format("%01.8f", t1));
                LOG.info("Strike1 : {}", nearBaseStrike);
            } else {
                LOG.warn("no nearTerm series found");
            }

            if (nextTermSeries != null) {
                nextBaseStrike = computeStrikeWithSmallestCallPutPriceDiff(nextTermSeries);
                t2 = computeTimeToExpiryInYears(nextTermSeries.getExpirationDate());
                LOG.info("nextTerm: {}", nextTermSeries.getDaysToExpiration() + " (" + nextTermSeries.getExpirationDate() + ")");
                LOG.info("T2      : {} (fmt={})", t2, String.format("%01.8f", t2));
                LOG.info("Strike : {}" + nextBaseStrike);
            } else {
                LOG.warn("no nextTerm series found");
            }
            if (Objects.nonNull(nearBaseStrike) && Objects.nonNull(nextBaseStrike)) {
                //For each series need to compute F1 and F2 with
                //
                //F1 = StrikeOld Price + e^(R1*T1) x (Call Price - Put Price)
                //F2 = StrikeOld Price + e^(R2*T2) x (Call Price - Put Price)
                double f1 = TimeUtil.computeForwardStrike(nearBaseStrike, 0.035d, t1);
                double f2 = TimeUtil.computeForwardStrike(nextBaseStrike, 0.035d, t2);
                LOG.info("F1 = {}", f1);
                LOG.info("F2 = {}", f2);
                LOG.info("TODO: now compute K0,1 and K0,2");

                //nearTerm
                List<SelectedContract> nearTermContracts = new ArrayList<>();
                double k01;
                boolean foundK0Strike1 = false;
                for (Strike strike : nearTermSeries.getStrikes()) {
                    if (!foundK0Strike1){
                        if (strike.getStrikePrice() >= f1) {
                            foundK0Strike1 = true;
                            k01 = strike.getStrikePrice();
                            LOG.info("K0,1 = {}", k01);
                            SelectedContract callPut = new SelectedContract(strike.getStrikePrice(),
                                    "call/put",
                                    average(strike.putMidQuote(), strike.callMidQuote()),
                                    strike.getPutContract().getQuote().getBid());
                            nearTermContracts.add(callPut);
                        } else {
                            SelectedContract put = new SelectedContract(strike.getStrikePrice(),
                                    "put",
                                    strike.putMidQuote(),
                                    strike.getPutContract().getQuote().getBid());
                            nearTermContracts.add(put);
                        }
                    } else {
                        SelectedContract call = new SelectedContract(strike.getStrikePrice(),
                                "call",
                                strike.callMidQuote(),
                                strike.getCallContract().getQuote().getBid());
                        nearTermContracts.add(call);
                    }

                   // System.out.println(formatStrikeLine(strike));
                }
                //sort selected strikes below K0,1 in descending order so strike
                //immediately below K0,1 is first

                LOG.info("start selected near contracts");
                for(SelectedContract sc: nearTermContracts){
                    System.out.println(sc);
                }
                LOG.info("end selected near contracts");
                //Now select all puts below K0,1--excluding zero bids--until you get to the last put before
                //two consecutive zero bids
                List<SelectedContract> ntc = nearTermContracts.stream()
                        .filter(SelectedContract::isPut)
                        .sorted(new PutComparator())
                        .collect(Collectors.toList());
                LOG.info("start ordered put near contracts");
                for(SelectedContract sc: ntc){
                    System.out.println(sc);
                }
                LOG.info("end ordered put near contracts");

                //nextTerm
                List<SelectedContract> nextTermContracts = new ArrayList<>();
                double k02;
                boolean foundK0Strike2 = false;
                for (Strike strike : nextTermSeries.getStrikes()) {
                    if (!foundK0Strike2){
                        if (strike.getStrikePrice() >= f1) {
                            foundK0Strike2 = true;
                            k02 = strike.getStrikePrice();
                            LOG.info("K0,2 = {}", k02);
                            SelectedContract callPut = new SelectedContract(strike.getStrikePrice(),
                                    "call/put",
                                    average(strike.putMidQuote(), strike.callMidQuote()),
                                    strike.getPutContract().getQuote().getBid());
                            nextTermContracts.add(callPut);
                        } else {
                            SelectedContract put = new SelectedContract(strike.getStrikePrice(),
                                    "put",
                                    strike.putMidQuote(),
                                    strike.getPutContract().getQuote().getBid());
                            nextTermContracts.add(put);
                        }
                    } else {
                        SelectedContract call = new SelectedContract(strike.getStrikePrice(),
                                "call",
                                strike.callMidQuote(),
                                strike.getCallContract().getQuote().getBid());
                        nextTermContracts.add(call);
                    }
                }
                LOG.info("start selected next contracts");
                for(SelectedContract sc: nextTermContracts){
                    System.out.println(sc);
                }
                LOG.info("end selected next contracts");

            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }


    private HttpUriRequest buildRequest(final URI uri) {
        return RequestBuilder.get()
                .setUri(uri)
                .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                .addParameter("symbol", symbols)
                .addParameter("quotes", "true")
                .addParameter("type", type)
                .addParameter("expire", expire)
                .addParameter("interval", interval)
                //.addParameter("strike", "113")
                .build();
    }

    private Strike computeStrikeWithSmallestCallPutPriceDiff(final OptionSeries series) {
        System.out.println("compute forward strike for DTE=" + series.getDaysToExpiration());
        // 1) compute avg price of each call and each put for each strike
        List<Strike> mystrikes = series.getStrikes().stream()
                .sorted(new Strike.CallPutDifferenceComparator())
                .collect(Collectors.toList());
        mystrikes.stream().forEach(s -> System.out.println(s.toString()));
        System.out.println("strikeCount: " + (long) mystrikes.size());
        return mystrikes.stream().findFirst().get();
    }

    private double computeTimeToExpiryInYears(final LocalDate seriesExpirationDate) {
        return TimeUtil.timeToExpiryInYears(LocalDateTime.now(), seriesExpirationDate);
    }

    private String formatStrikeLine(Strike strike) {
        StrikeDisplayAdapter adapter = new StrikeDisplayAdapter(strike);
        return adapter.format();
    }

    private double average(final double a, final double b){
        return a + b / 2.0;
    }

    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        Options options = new Options();
        org.apache.commons.cli.Option typeValue = new org.apache.commons.cli.Option("t", true, "type");
        options.addOption(typeValue);
        org.apache.commons.cli.Option expireValue = new org.apache.commons.cli.Option("e", true, "expire");
        options.addOption(expireValue);
        org.apache.commons.cli.Option intervalValue = new org.apache.commons.cli.Option("i", true, "interval");
        options.addOption(intervalValue);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        if (cmdLine.hasOption("t")) {
            type = cmdLine.getOptionValue("t");
        }
        if (cmdLine.hasOption("e")) {
            expire = cmdLine.getOptionValue("e");
        }
        if (cmdLine.hasOption("i")) {
            interval = cmdLine.getOptionValue("i");
        }

        if (cmdLine.getArgs().length < 1) {
            throw new ParseException("symbol(s) not found");
        }
        symbols = cmdLine.getArgs()[0].toUpperCase(); //first argument
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }


    private static class SelectedContract {
        private final double strikePrice;
        private final String optionType;
        private final double midQuotePrice;
        private final double bid;

        public SelectedContract(final double strikePrice, final String optionType, final double midQuotePrice, final double bid) {
            this.strikePrice = strikePrice;
            this.optionType = optionType;
            this.midQuotePrice = midQuotePrice;
            this.bid = bid;
        }

        public double getStrikePrice() {
            return strikePrice;
        }

        public String getOptionType() {
            return optionType;
        }

        public boolean isCall(){
            return "call".equals(optionType);
        }

        public boolean isPut(){
            return "put".equals(optionType);
        }

        public double getBid() {
            return bid;
        }

        @Override
        public String toString() {
            return strikePrice +
                    ", '" + optionType + '\'' +
                    ", " + midQuotePrice +
                    ", " + bid;
        }
    }

    static class PutComparator implements Comparator<SelectedContract>
    {
        public int compare(SelectedContract c1, SelectedContract c2)
        {
            double s1 = c1.getStrikePrice();
            double s2 = c2.getStrikePrice();
            return -1 * Double.compare(s1, s2);
        }
    }
}
