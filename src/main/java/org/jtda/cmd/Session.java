package org.jtda.cmd;

import generated.Account;
import generated.LoginResult.Accounts;
import generated.StreamerInfo;
import org.jtda.model.Strike;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Properties;

public interface Session {
    List<Account> getAccounts();
	String getSessionId();
    void setSessionId(String sessionId);
    void setUserId(String userId);
    void setAssociatedAccountId(Long associatedAccountId);
    Long getAssociatedAccountId();
    void setAccounts(Accounts accounts);
    StreamerInfo getStreamerInfo();
    void setStreamerInfo(StreamerInfo info);
    void setConfiguration(Properties config);
    Properties getConfiguration();
    void invalidate();
    boolean valid();
    boolean expired();
    void setSessionTimeout(int timeoutInMinutes);
    int getSessionTimeout();
    void setLoginTime(ZonedDateTime loginDateTime);
    ZonedDateTime getLoginTime();

    /**
     * Add a cache of a recent quotes for the most liquid strikes
     * @param mostLiquidStrikes .
     */
    void addLastLiquidStrikes(List<Strike> mostLiquidStrikes);
    List<Strike> getLastLiquidStrikes();

    /**
     * Returns the orderId of the last (most recent) order submitted (e.g. buy|sell|long|short commands).
     * @return the orderId for the most recently submitted order
     */
    String getLastOrderId();
    void setLastOrderId(String orderId);
}
