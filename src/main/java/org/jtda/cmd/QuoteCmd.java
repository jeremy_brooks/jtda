package org.jtda.cmd;

import generated.QuoteList;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.QuoteListXmlHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class QuoteCmd extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(QuoteCmd.class);
    private static final String NAME = "QuoteCmd";
    private Session session;
    private String symbol;

    public void execute() throws CommandException {
        if (session.getConfiguration() == null) {
            throw new CommandException("must login first");
        }
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = RequestBuilder.get()
                    .setUri(uri)
                    .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                    .addParameter("symbol", symbol)
                    .build();
            LOG.info("" + httpget.getRequestLine());
            final ResponseHandler<QuoteList> quoteListHandler = new QuoteListXmlHandler();
            QuoteList quoteList = httpclient.execute(httpget, quoteListHandler);
            //print it out
            System.out.println(quoteList.getQuote().toString());
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }


    @Override
    public void setSession(final Session session) {
        this.session = session;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws org.apache.commons.cli.ParseException {
        Options options = new Options();
        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);
        symbol = cmdLine.getArgs()[0].toUpperCase(); //first argument
    }
}