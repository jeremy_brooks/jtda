package org.jtda.cmd;

import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.OptionChainBinaryHandler;
import org.jtda.model.OptionChain;
import org.jtda.model.OptionSeries;
import org.jtda.model.Strike;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;
import java.util.stream.Collectors;

/**
 * Compute the most liquid strikes for the selected symbol and expirations.
 *
 * @author jeremy
 */
public class MostLiquidStrikes extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(MostLiquidStrikes.class);

    private static final String NAME = "BinaryOptionChain";
    private static final ResponseHandler<OptionChain> HANDLER = new OptionChainBinaryHandler();

    private Session session;
    private List<Strike> mostLiquidStrikes;
    private String symbols;
    private String type; //C, P, VCS, VPS, CCS, CPS, BW, STDL, STGL
    private String expire = "a"; //al, a, l, w
    private String interval; //interval between strikes

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "200");
            HttpUriRequest httpget = buildRequest(uri);
            LOG.info("" + httpget.getRequestLine());
            Instant start = Instant.now();
            OptionChain ocr = httpclient.execute(httpget, HANDLER);
            Instant finish = Instant.now();
            LOG.info("response time: {}", Duration.between(start, finish));
            System.out.println("Symbol : " + ocr.getSymbol() + " (" + ocr.getDescription() + ")");
            System.out.println("Last   : " + ocr.getLast());
            System.out.println("Bid    : " + ocr.getBid());
            System.out.println("Ask    : " + ocr.getAsk());
            System.out.println("BASize : " + ocr.getBidAskSize());
            System.out.println("YdayCls: " + ocr.getClose());
            System.out.println(csvPrices(ocr));
            System.out.println("Expirations:");

            OptionSeries series;
            mostLiquidStrikes = new ArrayList<>(15);
            int strikeIndex = 1;
            SortedSet<LocalDate> expirations = ocr.getOptionExpirations();
            for (LocalDate expiration : expirations) {
                series = ocr.getOptionSeries(expiration);
                Strike mls = computeStrikeWithSmallestCallPutPriceDiff(series);
                if(!Objects.isNull(mls)){
                    mostLiquidStrikes.add(mls);
                    StrikeDisplayAdapter sda = new StrikeDisplayAdapter(mls);
                    System.out.println(sda.formatForMls(strikeIndex));
                    strikeIndex++;
                }
            }
            session.addLastLiquidStrikes(mostLiquidStrikes);
        } catch (ClientProtocolException e) {
            LOG.error(e.getMessage());
            System.err.println(e.getMessage());
        } catch (IOException e) {
            LOG.error(e.getMessage());
            System.err.println(e.getMessage());
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    private String csvPrices(final OptionChain ocr) {
        //"csv",quoteTime,sym,last,bid,ask,baSize,yesterdayClose,symDescription
        return StringUtils.join(new Object[]{"csv",
                ocr.getQuoteTime(),ocr.getSymbol(), ocr.getLast(), ocr.getBid(), ocr.getAsk(), ocr.getBidAskSize(),
                ocr.getClose(), "\"" + ocr.getDescription() + "\""}, "\t");
    }


    private HttpUriRequest buildRequest(final URI uri) {
        return RequestBuilder.get()
                .setUri(uri)
                .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                .addParameter("symbol", symbols)
                .addParameter("quotes", "true")
                .addParameter("type", type)
                .addParameter("expire", expire)
                .addParameter("interval", interval)
                //.addParameter("strike", "113")
                .build();
    }

    private Strike computeStrikeWithSmallestCallPutPriceDiff(final OptionSeries series) {
        LOG.debug("compute forward strike for DTE=" + series.getDaysToExpiration());
        // 1) compute avg price of each call and each put for each strike
        //System.out.println("series " + series.getExpirationDate() + " number of strikes: " + series.getStrikes().size());
        List<Strike> myStrikes = series.getStrikes().stream()
                .filter(s -> s.hasNonZeroBidsAndOpenInterest())
                .sorted(new Strike.CallPutDifferenceComparator())
                .collect(Collectors.toList());
        if(myStrikes.size() > 0){
            return myStrikes.get(0);
        } else {
            LOG.warn(symbols + " option series " + series.getExpirationDate() + " has no qualifying liquid strikes");
            return null;
        }
    }

    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        Options options = new Options();
        org.apache.commons.cli.Option typeValue = new org.apache.commons.cli.Option("t", true, "type");
        options.addOption(typeValue);
        org.apache.commons.cli.Option expireValue = new org.apache.commons.cli.Option("e", true, "expire");
        options.addOption(expireValue);
        org.apache.commons.cli.Option intervalValue = new org.apache.commons.cli.Option("i", true, "interval");
        options.addOption(intervalValue);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        if (cmdLine.hasOption("t")) {
            type = cmdLine.getOptionValue("t");
        }
        if (cmdLine.hasOption("e")) {
            expire = cmdLine.getOptionValue("e");
        }
        if (cmdLine.hasOption("i")) {
            interval = cmdLine.getOptionValue("i");
        }

        if (cmdLine.getArgs().length < 1) {
            throw new ParseException("symbol(s) not found");
        }
        symbols = cmdLine.getArgs()[0].toUpperCase(); //first argument
    }

    @Override
    public void setSession(final Session session) {
        this.session = session;
    }


}
