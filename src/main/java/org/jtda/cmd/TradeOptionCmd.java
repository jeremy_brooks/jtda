package org.jtda.cmd;

import generated.Order;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jtda.handler.TradeOptionResponseHandler;
import org.jtda.model.OptionContract;
import org.jtda.model.Strike;
import org.jtda.model.TradeOptionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Usage from command line:
 *
 *  buy 10 AAPL_092118C185 0.05
 *
 * (buy to open ten Apple 185 calls expiring in September for 5 cents each
 *  with an implied day limit order)
 *
 *  sell 10 AAPL_092118C185 10.05
 *
 *  (sell to close ten Apple 185 calls expiring in September for 10.05 each
 *  with an implied day limit order)
 *
 *
 * Order accepted response:
 * 1883229576 B O 10 AAPL Oct 19 2018 220 Call 0.05
 *
 * The first value is the order-id.
 *
 *
 * Order rejected response:
 * Your buying power will be below zero ($xxx.yy) if this order is accepted.
 * symbol=AAPL_101918C220~accountid=1~quantity=1000~price=5.95~expire=day~ordtype=limit~action=buytoopen
 *
 * The order submitted by this command is saved to the session with session.setLastOrderId(orderId)
 */
public class TradeOptionCmd extends CommandBase implements Command, Stateful {
    private static final Logger LOG = LoggerFactory.getLogger(TradeOptionCmd.class);

    private static final String NAME = "OptionTrade";
    private static final ResponseHandler<TradeOptionResponse> HANDLER = new TradeOptionResponseHandler();

    private Session session;
    private String buyOrSell;
    private String symbol;
    private int quantity;                 //number of contracts to buy or sell
    private String action = "buytoopen";  //buytoopen, selltoclose, etc
    private final String expire = "day";        //day, gtc
    private final String ordType = "limit";     //market, limit, stop_market, stop_limit
    private final String specialInstructions = "none";  //none=none, fok=FillOrKill, aon=AllOrNone
    private BigDecimal price;

    @Override
    public void execute() throws CommandException {
        try {
            URI uri = getUriForCommand(NAME, "100");
            HttpUriRequest httpget = buildRequest(uri);
            LOG.info("" + httpget.getRequestLine());
            Instant start = Instant.now();
            TradeOptionResponse orderResponse = httpclient.execute(httpget, HANDLER);
            Instant finish = Instant.now();
            LOG.info("response time: {}", Duration.between(start, finish));
            if(orderResponse.hasOrder()){
                final Order order = orderResponse.getOrder();
                final String displayText = StringUtils.join(
                        new Object[]{ order.getOrderId(),
                        order.getAction(),
                        order.getOpenClose(),
                        order.getQuantity(),
                        order.getSecurity().getDescription(),
                        order.getLimitPrice()}, " ");
                System.out.println(displayText);
                session.setLastOrderId(order.getOrderId());
            } else {
                final String displayText = StringUtils.join(
                        new Object[]{ orderResponse.getErrorMessage(),
                                orderResponse.getOrderString()}, "\n");
                System.err.println(displayText);
            }

        }
        catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        finally {
            IOUtils.closeQuietly(httpclient);
        }
    }


    private HttpUriRequest buildRequest(final URI uri) {
        String actionToTake;
        switch(buyOrSell){
            case "buy":
                actionToTake = "buytoopen";
                break;
            case "sell":
                actionToTake = "selltoclose";
                break;
            default:
                throw new UnsupportedOperationException("must be 'buy' or 'sell'");
        }

        Map<String,String> orderParams = new HashMap<>();
        orderParams.put("symbol", symbol);
        orderParams.put("quantity", "" + quantity);
        orderParams.put("action", actionToTake);
        orderParams.put("price", price.toString());
        orderParams.put("expire", expire);
        orderParams.put("ordtype", ordType);
        orderParams.put("spinstructions", specialInstructions);
        orderParams.put("accountid", "" + session.getAssociatedAccountId());
        StringBuilder sb = new StringBuilder();
        orderParams.forEach((k,v) -> sb.append(k).append("=").append(v).append('~'));
        sb.deleteCharAt(sb.length()-1);
        final String orderString = sb.toString();
        return RequestBuilder.get()
                .setUri(uri)
                .addParameter("source", session.getConfiguration().getProperty(SOURCE_PROPERTY))
                .addParameter("orderstring", orderString)
                .build();
    }


    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public void parseArguments(final String commandName, final String[] args) throws ParseException {
        //We parse arguments manually so we can (eventually) support entering negative numbers
        //which would get confused as an option to the command if we used CommandLineParser
        buyOrSell = commandName.toLowerCase();
        if("buy".equals(buyOrSell) || "sell".equals(buyOrSell)){
            if (args.length < 3) {
                System.err.println("usage: [buy|sell] <quantity> <symbol> <price>" +
                        "\n\tSubmits a limit order for given quantity of symbol at given price" +
                        "\n\tA buy order is submitted as buy to open; sell as sell to close." +
                        "\n\tSelling to open or buying to close are not yet supported.");

                return;
            }
            quantity = Integer.parseInt(args[0]);    //first argument
            symbol = args[1].toUpperCase();
            price = XmlUtil.parseBigDecimal(args[2]);
        } else if (session.getLastLiquidStrikes().size() > 0) {
            //use the last liquid strikes from the session to buy calls or puts at bid price
            //
            //"long 3"  = "buy 1 <call symbol from index 3> <call bid price from index 3>"
            //"short 2" = "buy 1 <put symbol from index 2> <put bid price from index 2>"
            final String callOrPut = buyOrSell.equals("long") ? "call" : buyOrSell.equals("short") ? "put" : "invalid";
            buyOrSell = "buy";
            int strikeIndex = Integer.parseInt(args[0]) - 1;    //first argument, minus 1 for zero-based list index
            Strike strike = session.getLastLiquidStrikes().get(strikeIndex);
            OptionContract contract;
            if("call".equals(callOrPut) || "put".equals(callOrPut)) {
                quantity = 1;
                contract = "call".equals(callOrPut) ? strike.getCallContract() : strike.getPutContract();
                symbol = contract.getOptionSymbolName().toUpperCase();
                price = BigDecimal.valueOf(contract.getQuote().getBid());
                System.err.println(buyOrSell + " " + quantity + " " + symbol + " @ " + price);
            } else {
                System.err.println("usage: long|short N");
            }
        } else {
            System.err.println("Usage: long|short N requires calling 'mls <symbol>' first");
        }
    }


    @Override
    public void setSession(final Session session) {
        this.session = session;
    }



}
