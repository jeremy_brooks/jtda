package org.jtda;

import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.jtda.cmd.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class JtdaClient {
    private String appId;
    private String appVersion;
    private Session session = new DefaultSession();
    private Properties commands = ConsoleApp.loadCommands();

    public JtdaClient(String appId, String appVersion) {
        super();
        this.appId = appId;
        this.appVersion = appVersion;
    }

    public void login(String user, String password) {
        try {
            invoke(new String[]{"login"});
        } catch (CommandException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void logout() {
        try {
            invoke(new String[]{"logout"});
        } catch (CommandException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void getBalances() {
        try {
            invoke(new String[]{"balance"});
        } catch (CommandException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void getQuote(List<String> symbols) {
        String symCsv = StringUtils.join(symbols, ",");
        try {
            invoke(new String[]{"quote", symCsv});
        } catch (CommandException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void getTradeHistory() {
        try {
            invoke(new String[]{"tradehistory"});
        } catch (CommandException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void invoke(String[] cmdLine) throws CommandException {
        try {
            String command = "";
            String[] args = new String[0];
            if (cmdLine == null || cmdLine.length < 1) {
                throw new CommandException("no command specified, try 'help' for command names");
            } else if (cmdLine.length == 1) {
                command = cmdLine[0];
            } else {
                command = cmdLine[0];
                args = Arrays.copyOfRange(cmdLine, 1, cmdLine.length);
            }
            if (commands.containsKey(command)) {
                String className = (String) commands.getOrDefault(command, command);
                Class<?> cmd = Class.forName(className);
                Command c = (Command) cmd.newInstance();
                c.parseArguments(command, args);
                if (c instanceof Stateful) {
                    ((Stateful) c).setSession(session);
                }
                System.out.println("running " + className);
                c.execute();
            } else {
                StringBuilder sb = new StringBuilder();
                for (String arg : args)
                    sb.append(arg)
                            .append(" ");
                System.out.println(sb.toString());
            }
        } catch (ParseException | IllegalArgumentException e) {
            System.err.println("Parsing failed: " + e.getMessage());
            e.printStackTrace(System.err);
        } catch (ClassNotFoundException e) {
            e.printStackTrace(System.err);
            throw new CommandException("command not found: " + e.getMessage(), e);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static final void main(String[] args) {
        JtdaClient jc = new JtdaClient("TESTSOURCE", "1.0");
        jc.login("notused", "alsonotusedyet");
        jc.getBalances();
        jc.getTradeHistory();
        List<String> symbols = new ArrayList<>();
        symbols.add("aapl");
        symbols.add("fb");
        jc.getQuote(symbols);
    }
}
