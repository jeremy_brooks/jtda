package org.jtda.handler;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.jtda.model.Deliverable;
import org.jtda.model.OptionChain;
import org.jtda.model.OptionContract;
import org.jtda.model.OptionQuote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

public class OptionChainBinaryHandler implements ResponseHandler<OptionChain> {
    private static final Logger LOG = LoggerFactory.getLogger(OptionChainBinaryHandler.class);

    private final boolean includeQuotes;


    public OptionChainBinaryHandler() {
        this.includeQuotes = true;
    }

    public OptionChainBinaryHandler(final boolean includeQuotes) {
        this.includeQuotes = includeQuotes;
    }

    @Override
    public final OptionChain handleResponse(final HttpResponse response)
            throws IOException {
        final InputStream is = response.getEntity().getContent();
        byte[] input = IOUtils.toByteArray(is);
        Path tempFile = Files.createTempFile("jtda", "OCB");
        LOG.info("writing result bytes to: {}", tempFile);
        FileUtils.writeByteArrayToFile(tempFile.toFile(), input);
        return parse(input);
    }

    public OptionChain parse(final byte[] input) throws IOException {
        ByteBuffer bb = ByteBuffer.wrap(input);
        byte errorCode = bb.get();
        if (0x01 == errorCode) {
            String msg = extractString(bb, "errorCode");
            throw new IOException(msg);
        }
        OptionChain chain = new OptionChain();
        chain.setSymbol(extractString(bb, "symbol"));
        chain.setDescription(extractString(bb, "symbolDesc"));
        if (includeQuotes) {
            chain.setBid(bb.getDouble());
            chain.setAsk(bb.getDouble());
            chain.setBidAskSize(extractString(bb, "bidAskSize"));
            chain.setLast(bb.getDouble());
            chain.setOpen(bb.getDouble());
            chain.setHigh(bb.getDouble());
            chain.setLow(bb.getDouble());
            chain.setClose(bb.getDouble());
            chain.setVolume(bb.getDouble());
            chain.setChange(bb.getDouble());
            chain.setRealTimeQuote('R' == bb.getChar()); //R or D
            chain.setQuoteTime(extractString(bb, "quoteTime"));
        }
        int symbolCount = bb.getInt();
        LOG.info("optionSymbolCount: {}", symbolCount);
        for (int sym = 0; sym < symbolCount; sym++) {
            OptionContract contract = parseOption(bb);
            chain.addContract(contract);
        }
        return chain;
    }

    private OptionContract parseOption(final ByteBuffer bb) {
        OptionContract os = new OptionContract();
        final String expDate = extractString(bb, "expDate");
        os.setExpiration(expDate);
        os.setExpirationType(extractString(bb, "expType"));
        os.setStrike(bb.getDouble());
        os.setStandardOption(0x01 == bb.get());
        os.setRight(bb.getChar());
        os.setOptionSymbolName(extractString(bb, "optSym"));
        os.setOptionDescription(extractString(bb, "optDesc"));
        if (includeQuotes) {
            OptionQuote quote = new OptionQuote();
            quote.setBid(bb.getDouble());
            quote.setAsk(bb.getDouble());
            quote.setBidAskSize(extractString(bb, "optBidAskSize"));
            quote.setLast(bb.getDouble());
            quote.setLastTradeSize(extractString(bb, "tradeSize"));
            quote.setLastTradeDate(extractString(bb, "tradeDate"));
            quote.setVolume(bb.getLong());
            quote.setOpenInterest(bb.getInt());
            quote.setRealTimeQuote(0x01 == bb.get());
            quote.setUnderlyingSymbol(extractString(bb, "underlying"));
            quote.setDelta(bb.getDouble());
            quote.setGamma(bb.getDouble());
            quote.setTheta(bb.getDouble());
            quote.setVega(bb.getDouble());
            quote.setRho(bb.getDouble());
            quote.setImpliedVolatility(bb.getDouble());
            quote.setTimeValueIndex(bb.getDouble());
            quote.setMultiplier(bb.getDouble());
            quote.setChange(bb.getDouble());
            quote.setChangePercentage(bb.getDouble());
            quote.setInTheMoney(0x01 == bb.get());
            quote.setNearTheMoney(0x01 == bb.get());
            quote.setTheoValue(bb.getDouble());
            quote.setDeliverableNote(extractString(bb, "deliverableNote"));
            quote.setCashInLieuDollarAmount(bb.getDouble());
            quote.setOpCashDollarAmount(bb.getDouble());
            quote.setIndexOption(0x01 == bb.get());

            int deliverablesLength = bb.getInt();
            for (int d = 0; d < deliverablesLength; d++) {
                Deliverable deliverable = new Deliverable();
                deliverable.setSymbol(extractString(bb, "deliverableSymbol"));
                deliverable.setShares(bb.getInt());
                quote.addDeliverable(deliverable);
            }
            os.setQuote(quote);
        }
        return os;
    }

    private String extractString(final ByteBuffer bb, final String comment) {
        LOG.trace("extracting {}", comment);
        short length = bb.getShort();
        LOG.trace("length={}", length);
        final byte[] buffer = new byte[length];
        bb.get(buffer, 0, length);
        return new String(buffer, Charset.forName("UTF-8"));
    }
}
