package org.jtda.handler;

import generated.Amtd;
import generated.History;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.jtda.cmd.XmlUtil;

import java.io.IOException;

public class HistoryXmlHandler implements ResponseHandler<History> {

    @Override
    public final History handleResponse(final HttpResponse response)
            throws IOException {
        String xml = EntityUtils.toString(response.getEntity());
        Amtd r = XmlUtil.unmarshall(xml);
        if ("OK".equals(r.getResult())) {
            return r.getHistory();
        } else {
            String msg = r.getResult() + ": " + r.getError();
            throw new IOException(msg);
        }
    }
}