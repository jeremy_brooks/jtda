package org.jtda.handler;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.jtda.cmd.Frequency;
import org.jtda.model.VolBar2;
import org.jtda.model.VolData;
import org.jtda.model.VolHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VolHistoryHandler implements ResponseHandler<List<VolHistory>> {
    private static final int ERROR = 1;
    private static final short END_OF_BARS = -1;

    private static final Logger LOG = LoggerFactory.getLogger(VolHistoryHandler.class);
    private static final int MAX_SYMBOLS_AND_OR_ERROR_RESPONSE = 300;

    private final Frequency barFrequency;


    public VolHistoryHandler(Frequency frequency) {
        barFrequency = frequency;
    }

    @Override
    public final List<VolHistory> handleResponse(final HttpResponse response)
            throws IOException {
        Header[] headers = response.getAllHeaders();
        int contentLength = 0;
        for (Header h : headers) {
            LOG.debug("{}={}", h.getName(), h.getValue());
            if ("Content-Length".equals(h.getName())) {
                contentLength = Integer.parseInt(h.getValue());
            }
        }
        final InputStream is = response.getEntity().getContent();
        byte[] input = IOUtils.toByteArray(is);
        ByteBuffer bb = ByteBuffer.wrap(input);
//        System.out.println("as array : "+Arrays.toString(input));
//        System.out.println("as string: "+new String(input));
        List<VolHistory> histories;
        byte[] buffer;

        //Symbol count: 4 bytes
        int symbolCount = bb.getInt();
        LOG.debug("symbolCount: {}", symbolCount);
        if (symbolCount > MAX_SYMBOLS_AND_OR_ERROR_RESPONSE) {
            //rewind the byte buffer and print whole response as string
            bb.rewind();
            buffer = new byte[contentLength];
            bb.get(buffer, 1, contentLength - 1); //skip first char
            String msg = new String(buffer, Charset.forName("UTF-8"));
            throw new IOException(msg);
        }
        histories = new ArrayList<>(symbolCount);
        for (int sym = 0; sym < symbolCount; sym++) {
            //Symbol
            int symbolLength = bb.getShort();
            buffer = new byte[symbolLength];
            bb.get(buffer, 0, symbolLength);
            String symbol = new String(buffer, Charset.forName("UTF-8"));
            VolHistory history = new VolHistory(symbol, barFrequency);
            //Error code (and optional error text)
            byte errorCode = bb.get();
            if (ERROR == errorCode) {
                int errorLength = bb.getShort();
                buffer = new byte[errorLength];
                bb.get(buffer, 0, errorLength);
                String error = new String(buffer, Charset.forName("UTF-8"));
                history.setError(error);
            }
            int barCount = bb.getInt();
            history.ensureCapacity(barCount);
            LOG.info("sym={} bars={}", symbol, barCount);

            for (int bar = 0; bar < barCount; bar++) {
                LOG.debug("reading bar {}", bar);
                float volatility = bb.getFloat();
                long timestamp = bb.getLong();
                LOG.debug("vol={},timestamp={}",
                        volatility, timestamp);
                //ZonedDateTime dateTimeUtc = Instant.ofEpochMilli(timestamp)
                //        .atZone(ZoneOffset.UTC);
                VolData barData = new VolBar2(timestamp, volatility);
                history.add(barData);
            }
            short terminator = bb.getShort();
            //assert END_OF_BARS == terminator;
            if (END_OF_BARS != terminator) {
                LOG.warn("expected {} bars of data for {} "
                                + "followed by a terminator signal but found this instead: {}",
                        barCount, symbol, terminator);
            }
            histories.add(history);
        }
        return histories;

    }
}
