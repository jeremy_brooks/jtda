package org.jtda.handler;

import generated.Order;
import generated.Security;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.jtda.model.TradeOptionResponse;
import org.jtda.model.XmlNode;
import org.jtda.model.XmlNodeBuilder;

import java.io.IOException;

/**
 * Created by jeremy on 08-18-2018.
 */
public class TradeOptionResponseHandler implements ResponseHandler<TradeOptionResponse> {

    @Override
    public TradeOptionResponse handleResponse(HttpResponse response)
            throws IOException {
        String xml = EntityUtils.toString(response.getEntity());
        return parse(xml);
    }

    public TradeOptionResponse parse(final String xml) throws IOException {
        XmlNode root = new XmlNodeBuilder(xml).getRoot();
        TradeOptionResponse r = new TradeOptionResponse();

        if ("OK".equals(root.getChildWithNameNonNull("result").getValue())) {
            XmlNode orderWrapperNode = root.getChildWithNameNonNull("order-wrapper");

            r.setOrderString(child(orderWrapperNode, "orderstring"));

            String errorMessage = orderWrapperNode.getChildWithNameNonNull("error").getValue();
            if(StringUtils.isNotBlank(errorMessage)){
                r.setErrorMessage(errorMessage);
            } else {
                XmlNode orderNode = orderWrapperNode.getChildWithNameNonNull("order");
                Order order = new Order();
                order.setOrderId(child(orderNode, "order-id"));
                order.setAccountId(Long.parseLong(child(orderNode, "account-id")));
                order.setSymbol(child(orderNode, "symbol"));

                XmlNode securityNode = orderNode.getChildWithName("security");
                Security security = new Security();
                security.setSymbol(child(securityNode, "symbol"));
                security.setSymbolWithTypePrefix(child(securityNode, "symbol-with-type-prefix"));
                security.setDescription(child(securityNode, "description"));
                security.setAssetType(child(securityNode, "asset-type"));
                order.setSecurity(security);
                order.setAction(child(orderNode, "action"));
                order.setQuantity(Integer.parseInt(child(orderNode, "quantity")));
                order.setLimitPrice(child(orderNode, "limit-price"));
                order.setStopPrice(child(orderNode, "stop-price"));
                order.setPutCall(child(orderNode, "put-call"));
                order.setOpenClose(child(orderNode, "open-close"));
                order.setOrderType(child(orderNode, "order-type"));
                order.setRoutingDisplaySize(Short.parseShort(child(orderNode, "routing-display-size")));
                r.setOrder(order);
            }
        }
        return r;
    }

    private static String child(XmlNode root, final String childNode) {
        return root.getChildWithNameNonNull(childNode).getValue();
    }
}
