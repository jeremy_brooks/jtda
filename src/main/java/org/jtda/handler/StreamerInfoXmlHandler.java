package org.jtda.handler;

import generated.Amtd;
import generated.StreamerInfo;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.jtda.cmd.XmlUtil;

import java.io.IOException;

public class StreamerInfoXmlHandler implements ResponseHandler<StreamerInfo> {

    @Override
    public StreamerInfo handleResponse(HttpResponse response)
            throws IOException {
        String xml = EntityUtils.toString(response.getEntity());
        Amtd r = XmlUtil.unmarshall(xml);
        if ("OK".equals(r.getResult())) {
            return r.getStreamerInfo();
        } else {
            return new StreamerInfo();
        }

    }
}
