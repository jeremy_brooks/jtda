package org.jtda;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by jeremy on 06-21-2018.
 */
public class StrHelper {

    /**
     * If null, return ""
     *
     * @param str String
     * @return Value of String or "" if null
     */
    public static String nonNull(Object str) {
        if (str != null) {
            return str.toString();
        } else {
            return "";
        }

    }

    /**
     * Tokenizers Strings, and Trims all Strings in result
     *
     * @param st    String to Trim
     * @param token Token
     * @return Array of Strings, which have white space removed
     */
    public static String[] StringTrimTokenizer(String st, String token) {
        String[] sa = StringTokenizer(st, token);

        for (int i = 0; i < sa.length; i++) {
            sa[i] = sa[i].trim();
        }
        return sa;
    }

    /**
     * Tokenizer for Strings
     *
     * @param st    String  to tokenizer
     * @param token Token for String
     * @return Array
     */
    public static String[] StringTokenizer(String st, String token) {
        if (st == null) {
            throw new RuntimeException(" null st");
        }
        if (token == null) {
            throw new RuntimeException(" null token");
        }

        int pos = 0;
        int count = 0;
        for (; ; ) {
            int i = st.indexOf(token, pos);
            if (i < 0) {
                count++;
                break;
            }
            count++;
            pos = i + token.length();
        }

        String[] sl = new String[count];

        pos = 0;
        count = 0;
        for (; ; ) {
            int i = st.indexOf(token, pos);
            if (i < 0) {
                sl[count] = st.substring(pos);
                break;
            }
            sl[count] = st.substring(pos, i);
            pos = i + token.length();
            count++;
        }

        return sl;
    }

    /**
     * @param fi InputStream to dump
     * @return InputStream as String
     * @throws IOException
     */
    public static String inputStreamToString(InputStream fi) throws IOException {

        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        byte buffer[] = new byte[1000];
        int len;
        while ((len = fi.read(buffer)) != -1) {
            bout.write(buffer, 0, len);
        }

        bout.close();
        fi.close();
        return bout.toString();
    }

}
