package org.jtda;

import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.codehaus.plexus.util.FileUtils;
import org.jtda.cmd.*;
import org.jtda.model.XmlNode;
import org.jtda.model.XmlNodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Prompts for login credentials before allowing
 * use of jtda commands from console.
 */
public final class ConsoleApp {
    private static final Logger LOG = LoggerFactory.getLogger(ConsoleApp.class);
    private static final String SERVER = System.getProperty("server", "apis.tdameritrade.com");
    protected static final String API_PATH = System.getProperty("path", "/apps/300/");
    private static final DateTimeFormatter LOGIN_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");

    private static final String USER_CONFIG_DIR = ".jtda";
    private static final String USER_CREDENTIALS_FILE = "credentials";
    private static final String USER_CONFIG_FILE = "config";

    private static final String USR_PROPERTY = "userid";
    private static final String PWD_PROPERTY = "password";
    private static final String SOURCE_PROPERTY = "source";
    private static final String VERSION_PROPERTY = "version";
    private static final String PROMPT = ">";
    private static final Path JTDA_TEMP_DIR = Paths.get(System.getProperty("java.io.tmpdir"), "org.jtda");

    private static CloseableHttpClient httpclient;
    private static CookieStore cookieStore;
    private static RequestConfig defaultRequestConfig;
    private static HttpClientConnectionManager connectionManager;
    private static Properties commands;
    private static Session session;

    static {
        try {
            FileUtils.forceMkdir(JTDA_TEMP_DIR.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String... args) {
        Console console = System.console();

        cookieStore = new BasicCookieStore();
        defaultRequestConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.DEFAULT)
                .setExpectContinueEnabled(true)
                .build();
        //https://stackoverflow.com/a/28646605
        connectionManager = new PoolingHttpClientConnectionManager();
        httpclient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .setDefaultRequestConfig(defaultRequestConfig)
                .setConnectionManager(connectionManager) // shared connection manager
                .setConnectionManagerShared(true)
                .build();

        Properties appConfig = loadConfig();
        config(console, appConfig);

        Properties userCreds = loadUserCreds();
        boolean isInteractive = userCreds.isEmpty() || System.getProperty("skipCreds") != null;
        login(console, appConfig, userCreds, isInteractive);

        commands = loadCommands();

        if(isInteractive) {
            console.printf(System.getProperty("line.separator") + PROMPT);
        }
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
            String input;
            while ((input = br.readLine()) != null) {
                if ("q".equals(input)) {
                    try {
                        invoke(new String[]{"logout"});
                    } catch (CommandException e) {
                        System.err.println("unable to logout (session is still active): " + e.getMessage());
                    }
                    break;
                }
                switch (input) {
                    case "help":
                        commands.keySet().forEach(obj -> System.err.println((String) obj));
                        break;
                    case "login":
                        login(console, appConfig, userCreds, isInteractive);
                        break;
                    default:
                        final String[] cmdLine = input.split("\\s+");
                        try {
                            invoke(cmdLine);
                        } catch (CommandException e) {
                            LOG.error(e.getMessage());
                            System.err.println(e.getMessage());
                        }
                        break;
                }
                if(isInteractive) {
                    console.printf(System.getProperty("line.separator") + PROMPT);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void config(final Console console, final Properties appConfig) {
        if (appConfig.isEmpty()) {
            String source = console.readLine("source: ");
            appConfig.setProperty(SOURCE_PROPERTY, source);

            String version = console.readLine("version: ");
            appConfig.setProperty(VERSION_PROPERTY, version);
        }
    }

    private static void login(final Console console, final Properties appConfig,
                              final Properties userCreds, final boolean isInteractive) {
        String userId;
        char[] password;
        if (isInteractive) {
            userId = console.readLine("UserId: ");
            password = console.readPassword("Password: ");
        } else {
            userId = userCreds.getProperty("userid");
            password = userCreds.getProperty("password").toCharArray();
        }

        try {
            HttpUriRequest loginRequest = buildLoginRequest(userId, password, appConfig);
            LoginResponseHandler rh = new LoginResponseHandler();
            LOG.info("logging in...");
            session = httpclient.execute(loginRequest, rh);
            Arrays.fill(password, ' ');
            session.setConfiguration(appConfig);
            System.out.println("Post login cookies:");
            List<Cookie> cookies = cookieStore.getCookies();
            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                cookies.stream().forEach(System.out::println);
            }

            if(isInteractive){
                console.printf("Welcome, %1$s.", userId);
            }
        } catch (ClientProtocolException e) {
            throw new RuntimeException("error executing http request", e);
        } catch (IOException e) {
            throw new RuntimeException("io error during http request", e);
        } finally {
            IOUtils.closeQuietly(httpclient);
        }
    }

    // PRIVATE
    private static void invoke(final String[] cmdLine) throws CommandException {
        try {
            String command;
            String[] args = new String[0];
            if (cmdLine == null || cmdLine.length < 1) {
                throw new CommandException("no command specified, try 'help' for command names");
            } else if (cmdLine.length == 1) {
                command = cmdLine[0];
            } else {
                command = cmdLine[0];
                args = Arrays.copyOfRange(cmdLine, 1, cmdLine.length);
            }

            String className = (String) commands.getOrDefault(command, command);
            Class<?> cmd = Class.forName(className);
            Command c = (Command) cmd.newInstance();
            if (c instanceof Stateful) {
                ((Stateful) c).setSession(session);
            }
            if (c instanceof CommandBase) {
                ((CommandBase) c).setHttpclient(httpclient);
                ((CommandBase) c).setCookieStore(cookieStore);
                ((CommandBase) c).setDefaultRequestConfig(defaultRequestConfig);
            }
            c.parseArguments(command, args);

            LOG.info("running " + className);
            c.execute();

        } catch (ParseException | IllegalArgumentException e) {
            System.err.println("Parsing failed: " + e.getMessage());
            e.printStackTrace(System.err);
        } catch (ClassNotFoundException e) {
            throw new CommandException("command not found: " + e.getMessage(), e);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    private static Properties loadUserCreds() {
        Properties config = new Properties();
        String fullPath = "";
        try {
            fullPath = Paths.get(System.getProperty("user.home"), USER_CONFIG_DIR, USER_CREDENTIALS_FILE).toString();
            config.load(new FileInputStream(new File(fullPath)));
        } catch (FileNotFoundException e) {
            final String msg = String.format("unable to find credentials file %s", fullPath);
            LOG.error(msg);
            System.err.println(msg);
        } catch (IOException e) {
            final String msg = String.format("unable to read credentials file %s", fullPath);
            LOG.error(msg);
            System.err.println(msg);
        }
        return config;
    }

    private static Properties loadConfig() {
        Properties config = new Properties();
        String fullPath = "";
        try {
            fullPath = Paths.get(System.getProperty("user.home"), USER_CONFIG_DIR, USER_CONFIG_FILE).toString();
            config.load(new FileInputStream(new File(fullPath)));
        } catch (FileNotFoundException e) {
            final String msg = String.format("unable to find config file %s", fullPath);
            LOG.error(msg);
            System.err.println(msg);
        } catch (IOException e) {
            final String msg = String.format("unable to read config file %s", fullPath);
            LOG.error(msg);
            System.err.println(msg);
        }
        return config;
    }

    public static Properties loadCommands() {
        Properties commands = new Properties();
        try {
            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            InputStream is = classLoader.getResourceAsStream("command.properties");
            commands.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return commands;
    }


    private static HttpUriRequest buildLoginRequest(final String userId, char[] password, Properties config) {
        URI uri;
        try {
            uri = new URIBuilder().setScheme("https")
                    .setHost(SERVER)
                    .setPath("/apps/300/LogIn")
                    .build();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
        return RequestBuilder.post()
                .setUri(uri)
                .addParameter(SOURCE_PROPERTY, config.getProperty(SOURCE_PROPERTY))
                .addParameter(VERSION_PROPERTY, config.getProperty(VERSION_PROPERTY))
                .addParameter(USR_PROPERTY, userId)
                .addParameter(PWD_PROPERTY, new String(password))
                .build();
    }

    private static String child(XmlNode root, final String childNode) {
        return root.getChildWithNameNonNull(childNode).getValue();
    }

    private static class LoginResponseHandler implements ResponseHandler<Session> {

        @Override
        public Session handleResponse(HttpResponse response)
                throws IOException {
            String xml = EntityUtils.toString(response.getEntity());
            XmlNode root = new XmlNodeBuilder(xml).getRoot();
            Session session = new DefaultSession();

            if ("OK".equals(root.getChildWithNameNonNull("result").getValue())) {
                XmlNode xmlLogin = root.getChildWithNameNonNull("xml-log-in");
                session.setSessionId(child(xmlLogin, "session-id"));
                session.setUserId(child(xmlLogin, "user-id"));
                session.setAssociatedAccountId(Long.parseLong(child(xmlLogin, "associated-account-id")));
                session.setLoginTime(ZonedDateTime.parse(child(xmlLogin, "login-time"), LOGIN_TIME_FORMAT));
                session.setSessionTimeout(Integer.parseInt(child(xmlLogin, "timeout")));
                //assert session.valid();
            }
            LOG.info("session valid for {} minutes, until {}", session.getSessionTimeout(),
                    session.getLoginTime().plusMinutes(session.getSessionTimeout()));
            return session;
        }
    }
}
