{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "array",
  "title": "Root schema.",
  "description": "An array of transactions",
  "items": {
    "type": "transaction",
    "title": "219 schema.",
    "description": "A transaction, (e.g., trade, leg of a complex trade, option expiration/assignment, a dividend, etc).",
    "properties": {
      "execDate": {
        "type": "string",
        "title": "ExecDate schema.",
        "description": "Date/time of order execution.  For example: 2008-03-25 10:39:47 EDT"
      },
      "orderNo": {
        "type": "string",
        "title": "OrderNo schema.",
        "description": "Order key associated with the transaction if it is a trade, empty otherwise."
      },
      "leg": {
        "type": "string",
        "title": "Leg schema.",
        "description": "The unique leg within this order (empty means this is a parent order or simply a one-leg order)"
      },
      "action": {
        "type": "string",
        "title": "Action schema.",
        "description": "Indicates the action: either B (buy) or S (sell), otherwise empty."
      },
      "effect": {
        "type": "string",
        "title": "Effect schema.",
        "description": "Indicates position effect: either O (open) or C (close) order"
      },
      "type": {
        "type": "string",
        "title": "Type schema.",
        "description": "Type of transaction (can be an enumeration later on, these are fixed known 2-char strings)"
      },
      "subType": {
        "type": "string",
        "title": "SubType schema.",
        "description": "Sub type of transaction (can be an enumeration later on, these are fixed known 2-char strings)"
      },
      "qty": {
        "type": "number",
        "title": "Qty schema.",
        "description": "Number of shares or contracts"
      },
      "symbol": {
        "type": "string",
        "title": "Symbol schema.",
        "description": "Symbol of the security involved in the transaction"
      },
      "desc": {
        "type": "string",
        "title": "Desc schema.",
        "description": "Description of the transaction (e.g., Bought, Sold to Open, Bought to Close, REMOVAL OF OPTION DUE TO EXPIRATION, etc)"
      },
      "commAndFees": {
        "type": "number",
        "title": "CommAndFees schema.",
        "description": "Sum of commissions, fees and additionalFees"
      },
      "price": {
        "type": "number",
        "title": "Price schema.",
        "description": "Price per share or per contract.  (6 digits maximum to the right of the decimal point)"
      },
      "value": {
        "type": "number",
        "title": "Value schema.",
        "description": "Net value of transaction including commission or fees charged. Negative numbers indicate a debit, positive a credit to the account"
      }
    },
    "required": [
      "execDate",
      "orderNo",
      "leg",
      "action",
      "effect",
      "type",
      "qty",
      "symbol",
      "desc",
      "commAndFees",
      "price",
      "value"
    ]
  }
}