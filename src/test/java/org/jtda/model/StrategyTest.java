package org.jtda.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jtda.model.TdaTransaction.AssetType;
import org.junit.Test;

public class StrategyTest {

    @Test
    public void whenNoTransactionsEmptyName() {
        String name = Strategy.generateSpreadName(Collections.emptyList());
        assertEquals("", name);
    }

    @Test
    public void whenTwoLegsThenVertical() {
        List<TdaTransaction> xactions = new ArrayList<>();
        String option1 = "GILD_081916P80";
        String option2 = "GILD_081916P90";
        TdaTransaction x1 = TdaTransaction.builder()
                .assetType(AssetType.OPTION)
                .symbol(option1)
                .groupId("1")
                .option(Option.parse(option1))
                .optionUnderlyingCusip("GILD")
                .quantity(1)
                .price(3.00)
                .build();
        TdaTransaction x2 = TdaTransaction.builder()
                .assetType(AssetType.OPTION)
                .symbol(option1)
                .groupId("1")
                .option(Option.parse(option2))
                .optionUnderlyingCusip("GILD")
                .quantity(-1)
                .price(3.00)
                .build();
        xactions.add(x1);
        xactions.add(x2);
        String name = Strategy.generateSpreadName(xactions);
        assertEquals("Vertical", name);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenMoreThan2LegsThrows() {
        List<TdaTransaction> xactions = new ArrayList<>();
        String option1 = "GILD_081916P80";
        TdaTransaction x1 = TdaTransaction.builder()
                .assetType(AssetType.OPTION)
                .symbol(option1)
                .groupId("1")
                .option(Option.parse(option1))
                .optionUnderlyingCusip("GILD")
                .quantity(1)
                .price(3.00)
                .build();
        xactions.add(x1);
        xactions.add(x1);
        xactions.add(x1);
        Strategy.generateSpreadName(xactions);
    }

}
