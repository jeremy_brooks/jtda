package org.jtda.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OptionTradeStringTest {

    @Test
    public void testBuildingOrderString() {
        OptionTradeString.Builder b = OptionTradeString.newOptionTradeStringBuilder();
        OptionTradeString ots = b.accountId(123456L)
                .quantity(100)
                .symbol("aapl")
                .orderType(OptionTradeString.OrderType.LIMIT)
                .timeInForce(OptionTradeString.TimeInForce.DAY)
                .action(OptionTradeString.Action.BUY_TO_CLOSE)
                .build();

        String expected = "accountId=123456~action=buytoclose~quantity=100~symbol=AAPL" +
                "~expire=day~ordType=limit~routing=auto";
        assertEquals(expected, ots.toString());
        
    }
}
