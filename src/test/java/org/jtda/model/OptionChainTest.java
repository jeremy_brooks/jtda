package org.jtda.model;

import org.junit.Test;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeremy on 06-20-2018.
 */
public class OptionChainTest {
    private static final double EPSILON = 0.000000000000001;

//    @Test
//    public void testAddingChainsTogether() {
//        OptionChain c1 = new OptionChain();
//        Strike s1 = new Strike(callContract(100, 2017), putContract(100, 2017));
//        c1.ensureCapacity(1);
//        c1.addStrike(s1);
//
//        Strike s2 = new Strike(callContract(110, 2018), putContract(110, 2018));
//        Strike s3 = new Strike(callContract(120, 2019), putContract(120, 2019));
//        OptionChain c2 = new OptionChain();
//        c2.ensureCapacity(2);
//        c2.addStrike(s2);
//        c2.addStrike(s3);
//
//        assertEquals(1, c1.getChain().size());
//        assertEquals(2, c2.getChain().size());
//        c1.addChain(c2.getChain());
//        assertEquals(3, c1.getChain().size());
//        assertTrue(c1.getOptionExpirations().contains(LocalDate.parse("2017-07-20")));
//        assertTrue(c1.getOptionExpirations().contains(LocalDate.parse("2018-07-20")));
//        assertTrue(c1.getOptionExpirations().contains(LocalDate.parse("2019-07-20")));
//        assertEquals(3, c1.getOptionExpirations().size());
//    }

    @Test
    public void testChainMaking() {
        OptionChain chain = new OptionChain();
        OptionContract call1 = callContract(100, 2017);
        OptionContract put1 = putContract(100, 2017);
        Strike s1 = new Strike(call1, put1);
        OptionContract call2 = callContract(110, 2018);
        OptionContract put2 = putContract(110, 2018);
        Strike s2 = new Strike(call2, put2);
        OptionContract call3 = callContract(120, 2019);
        OptionContract put3 = putContract(120, 2019);
        Strike s3 = new Strike(call3, put3);

        //add them in non sorted order
        chain.addContract(call3);
        chain.addContract(call2);
        chain.addContract(call1);
        chain.addContract(put2);
        chain.addContract(put3);
        chain.addContract(put1);

        final Map<LocalDate, Set<OptionContract>> expectedCalls = new HashMap<>();
        final Map<LocalDate, Set<OptionContract>> expectedPuts = new HashMap<>();

        final Set<OptionContract> expectedCalls1 = new TreeSet<>(Collections.singleton(call1));
        final Set<OptionContract> expectedPuts1 = new TreeSet<>(Collections.singleton(put1));
        final Set<OptionContract> expectedCalls2 = new TreeSet<>(Collections.singleton(call2));
        final Set<OptionContract> expectedPuts2 = new TreeSet<>(Collections.singleton(put2));
        final Set<OptionContract> expectedCalls3 = new TreeSet<>(Collections.singleton(call3));
        final Set<OptionContract> expectedPuts3 = new TreeSet<>(Collections.singleton(put3));
        expectedCalls.put(LocalDate.of(2017, 7, 20), expectedCalls1);
        expectedCalls.put(LocalDate.of(2018, 7, 20), expectedCalls2);
        expectedCalls.put(LocalDate.of(2019, 7, 20), expectedCalls3);

        expectedPuts.put(LocalDate.of(2017, 7, 20), expectedPuts1);
        expectedPuts.put(LocalDate.of(2018, 7, 20), expectedPuts2);
        expectedPuts.put(LocalDate.of(2019, 7, 20), expectedPuts3);

        final Map<LocalDate, Strike> expectedStrikeMap = new HashMap<>();
        expectedStrikeMap.put(LocalDate.of(2017, 7, 20), s1);
        expectedStrikeMap.put(LocalDate.of(2018, 7, 20), s2);
        expectedStrikeMap.put(LocalDate.of(2019, 7, 20), s3);

        Set<LocalDate> expirationList = chain.getOptionExpirations();
        for (final LocalDate expiration : expirationList) {
            final OptionChain.Series s = chain.getSeries(expiration);
            assertEquals(expectedCalls.get(expiration), s.getCalls());
            assertEquals(expectedPuts.get(expiration), s.getPuts());

            for (final int strikeCents : s.getSetOfStrikes()) {
                Strike strike = s.getStrikeObject(strikeCents);
                System.out.println(strike);
                assertEquals(expectedStrikeMap.get(expiration), strike);
            }
        }
    }


    @Test
    public void testStrikeCalculations() {
        OptionContract call = callContract(120, 2019);
        call.setQuote(quote(1.50d, 1.51d));
        OptionContract put = putContract(120, 2019);
        put.setQuote(quote(1.60d, 1.70d));
        Strike strike = new Strike(call, put);

        double expectedPutMid = 1.65d;
        double actualPutMid = strike.putMidQuote();
        assertEquals(expectedPutMid, actualPutMid, EPSILON);
        double expectedCallMid = 1.505d;
        double actualCallMid = strike.callMidQuote();
        assertEquals(expectedCallMid, actualCallMid, EPSILON);

        double expectedDiff = Math.abs(expectedPutMid - expectedCallMid);
        double actualDiff = strike.diff();

        assertEquals(expectedDiff, actualDiff, EPSILON);
    }

    private OptionQuote quote(final double bid, final double ask) {
        OptionQuote q = new OptionQuote();
        q.setBid(bid);
        q.setAsk(ask);
        return q;
    }


    private OptionContract putContract(double strikePrice, int year) {
        OptionContract c = new OptionContract();
        c.setOptionSymbolName("AAPL" + year + "0720P" + strikePrice);
        c.setStrike(strikePrice);
        c.setRight('P');
        c.setExpirationType("R");
        c.setExpiration(LocalDate.of(year, 7, 20).toString());
        c.setStandardOption(true);
        return c;
    }

    private OptionContract callContract(double strikePrice, int year) {
        OptionContract c = new OptionContract();
        c.setOptionSymbolName("AAPL" + year + "0720C" + strikePrice);
        c.setStrike(strikePrice);
        c.setRight('C');
        c.setExpirationType("R");
        c.setExpiration(LocalDate.of(year, 7, 20).toString());
        c.setStandardOption(true);
        return c;
    }


}
