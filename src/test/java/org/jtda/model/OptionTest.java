package org.jtda.model;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.jtda.model.Option;
import org.junit.Test;

public class OptionTest {

    @Test public void testToStringDecimalStrike(){
        Option o = Option.builder()
                .underlying("AMZN")
                .expirationDate(LocalDate.of(2016, 8, 19))
                .strike(752.5)
                .right(Option.Type.CALL)
                .build();
        assertEquals("AMZN 08-19-2016 752.5 CALL", o.toString());
    }
    
    @Test public void testToStringEvenStrike(){
        Option o = Option.builder()
                .underlying("AMZN")
                .expirationDate(LocalDate.of(2016, 8, 19))
                .strike(750.01)
                .right(Option.Type.PUT)
                .build();
        assertEquals("AMZN 08-19-2016 750 PUT", o.toString());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testParseFailsOnNoUnderscore(){
        String toParse = "AMZN";
        Option.parse(toParse);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseFailsWhenNotCallOrPut(){
        String toParse = "AMZN_081916X752.5";
        Option.parse(toParse);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseFailsWhenNoStrikePrice(){
        String toParse = "AMZN_081916C";
        Option.parse(toParse);
    }

    @Test
    public void testParseSucceeds(){
        String toParse = "AMZN_081916P752.5";
        assertEquals("AMZN 08-19-2016 752.5 PUT", Option.parse(toParse).toString());

        //doesn't handle last century as an expiration date
        toParse = "AMZN_081297C50";
        assertEquals("AMZN 08-12-2097 50 CALL", Option.parse(toParse).toString());
    }

}
