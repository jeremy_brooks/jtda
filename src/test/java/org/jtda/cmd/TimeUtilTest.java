package org.jtda.cmd;

import org.jtda.model.OptionContract;
import org.jtda.model.OptionQuote;
import org.jtda.model.Strike;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeremy on 06-09-2018.
 */
public class TimeUtilTest {

    @Test
    public void testDuringNoDaylightSavingsChanges(){
        LocalDate may1st = LocalDate.of(2018, 5, 1);
        LocalDate june9th = LocalDate.of(2018, 6, 9);
        LocalDate june15th = LocalDate.of(2018, 6, 15);

        LocalTime nineFortySixAm = LocalTime.of(9, 46, 00);
        LocalTime sevenThirtyFourPm = LocalTime.of(17, 34, 8);
        LocalDateTime june10thMidnight = june9th.atStartOfDay().plus(1, ChronoUnit.DAYS);
        LocalDateTime start = LocalDateTime.of(june9th, sevenThirtyFourPm);

        long estimatedMinutes = TimeUtil.estimatedMinutesBetween(start, june15th);
        assertEquals(8545, estimatedMinutes);
    }

    @Test
    public void testOverADaylightSavingsSpringAheadChange(){
        LocalDate march1st = LocalDate.of(2018, 3, 1);
        //DST begins on March 11, 2018
        LocalDate thirdFridayInMarch = LocalDate.of(2018, 3, 16);

        LocalTime nineFortySixAm = LocalTime.of(9, 46, 00);
        LocalDateTime start = LocalDateTime.of(march1st, nineFortySixAm);

        long estimatedMinutes = TimeUtil.estimatedMinutesBetween(start, thirdFridayInMarch);
        //correct value would be 21914 but we don't account for DST
        assertEquals(21_974, estimatedMinutes);
    }

    @Test
    public void testInDifferentYearsStartingAtMidnight(){
        LocalDate december1stPrevYear = LocalDate.of(2017, 12, 1);
        LocalDate thirdFridayInFebruary = LocalDate.of(2018, 2, 16);
        //That's 77 full days in between (110,880 minutes)
        LocalDateTime start = december1stPrevYear.atStartOfDay();

        long estimatedMinutes = TimeUtil.estimatedMinutesBetween(start, thirdFridayInFebruary);
        assertEquals(111_840, estimatedMinutes);
    }

    @Test
    public void testOverTenYearsApart(){
        LocalDate december2007 = LocalDate.of(2007, 12, 1);
        LocalDate february2018 = LocalDate.of(2018, 2, 16);
        LocalDateTime start = december2007.atStartOfDay();

        long estimatedMinutes = TimeUtil.estimatedMinutesBetween(start, february2018);
        assertEquals(5_372_160, estimatedMinutes);
    }

    @Test
    public void testComputeTimeToExpiration(){
        LocalDate december1stPrevYear = LocalDate.of(2017, 12, 1);
        LocalDateTime start = december1stPrevYear.atStartOfDay();
        LocalDateTime start345pm = december1stPrevYear.atTime(15, 45);


        double just15MinutesTimeRemaining = TimeUtil.timeToExpiryInYears(start345pm, december1stPrevYear);
        assertEquals(0.000028538812785388127d, just15MinutesTimeRemaining,
                        0.0000000000000000000001);

        double withinSameDayTimeRemaining = TimeUtil.timeToExpiryInYears(start, december1stPrevYear);
        assertEquals(0.0018264840182648401d, withinSameDayTimeRemaining,
                        0.00000000000000000001);

        LocalDate aboutThreeMonthsLater = LocalDate.of(2018, 2, 16);
        double quarterYearTimeRemaining = TimeUtil.timeToExpiryInYears(start, aboutThreeMonthsLater);
        assertEquals(0.21278538812785389d, quarterYearTimeRemaining,
                        0.000000000000000001);

        LocalDate aboutSixMonthsLater = LocalDate.of(2018, 5, 16);
        double halfYearTimeRemaining = TimeUtil.timeToExpiryInYears(start, aboutSixMonthsLater);
        assertEquals(0.45662100456621d, halfYearTimeRemaining,
                        0.000000000000000001);

        LocalDate aboutElevenMonthsLater = LocalDate.of(2018, 11, 16);
        double justUnderYearTimeRemaining = TimeUtil.timeToExpiryInYears(start, aboutElevenMonthsLater);
        assertEquals(0.960730593607306d, justUnderYearTimeRemaining,
                        0.000000000000000001);

        LocalDate aboutEighteenMonthsLater = LocalDate.of(2019, 5, 16);
        double overAYearTimeRemaining = TimeUtil.timeToExpiryInYears(start, aboutEighteenMonthsLater);
        assertEquals(1.45662100456621d, overAYearTimeRemaining,
                        0.000000000000000001);
    }

    @Test
    public void testComputeForwardStrikeVixWhitePaperExampleF1(){
        Strike nineteenSixtyFiveStrike = buildStrike(
                BigDecimal.valueOf(1965.0d),
                BigDecimal.valueOf(21.05d),
                BigDecimal.valueOf(23.15d));
        double riskFreeRate = 0.000305d;
        double timeToExpiry = 0.0683486d;
        Double computedDouble = TimeUtil.computeForwardStrike(nineteenSixtyFiveStrike, riskFreeRate, timeToExpiry);
//        BigDecimal computedBD = TimeUtil.computeForwardStrikeBD(nineteenSixtyFiveStrike, riskFreeRate, timeToExpiry);
        assertEquals(1962.8999562222654016d, computedDouble, 0.00000000000001d);
        assertEquals("1962.8999562222655", computedDouble.toString());
//        assertEquals("1962.89995622226539940", computedBD.toString());
    }

    @Test
    public void testComputeForwardStrikeVixWhitePaperExampleF2(){
        Strike nineteenSixtyStrike = buildStrike(
                BigDecimal.valueOf(1960.0d),
                BigDecimal.valueOf(27.30d),
                BigDecimal.valueOf(24.90d));
        double riskFreeRate = 0.000286d;
        double timeToExpiry = 0.0882686d;
        Double computedDouble = TimeUtil.computeForwardStrike(nineteenSixtyStrike, riskFreeRate, timeToExpiry);
//        BigDecimal computedBD = TimeUtil.computeForwardStrikeBD(nineteenSixtyStrike, riskFreeRate, timeToExpiry);
        assertEquals(1962.40006058833181d, computedDouble,0.00000000000001d);
        assertEquals("1962.4000605883318", computedDouble.toString());
//        assertEquals("1962.40006058833180768", computedBD.toString());
    }

    @Test
    public void testSmallDoubleIssue(){
        double riskFree = 0.035;
        double expiry =.00000020192107957604643d;
        double rt = riskFree * expiry;
        System.out.println(rt);
        double eRT = Math.pow(Math.E, rt);
        System.out.println(eRT);
    }


    @Test
    public void testComputeThirdFridayAfter(){
        //in 2008 good friday is an exchange holiday and the last trading day
        //is actually the previous business day, aka Thursday.
        //https://www.thinkorswim.com/trading/market/expirationCalendar2008.pdf
        //Our code does NOT account for holidays on expiration friday yet.
        LocalDate d = LocalDate.of(2008, 01, 01);
        LocalDate stop = LocalDate.of(2009, 01, 01);
        LocalDate previousThirdFriday = d.minusDays(1);
        while(d.isBefore(stop)){
            LocalDate thirdFriday = TimeUtil.computeThirdFridayAfter(d);
            if(!thirdFriday.equals(previousThirdFriday)) {
                System.out.println(d.toString() + ": " + thirdFriday.toString());
            }
            previousThirdFriday = thirdFriday;
            d = d.plusDays(1);
        }
    }

    private static Strike buildStrike(BigDecimal strikePrice, BigDecimal callMidQuote, BigDecimal putMidQuote){
        OptionContract call = new OptionContract();
        OptionContract put = new OptionContract();
        call.setStrike(strikePrice.doubleValue());
        put.setStrike(strikePrice.doubleValue());
        call.setExpiration("20180115");
        put.setExpiration("20180115");
        call.setStandardOption(true);
        put.setStandardOption(true);

        OptionQuote cQuote = new OptionQuote(callMidQuote.doubleValue(), callMidQuote.doubleValue());
        OptionQuote pQuote = new OptionQuote(putMidQuote.doubleValue(), putMidQuote.doubleValue());
        call.setQuote(cQuote);
        put.setQuote(pQuote);
        return new Strike(call, put);
    }
}
