package org.jtda.cmd;

import org.jtda.model.VolBar;
import org.jtda.model.VolHistory;
import org.junit.Test;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeremy on 6/2/18.
 */
public class VolHistoryTest {

    public static final ZonedDateTime ZONED_DATE_TIME = ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("America/New_York"));

    @Test
    public void testIvStats(){
        List<VolBar> volBars = new ArrayList<>();
        volBars.add(new VolBar(0.10f, ZONED_DATE_TIME));
        volBars.add(new VolBar(0.10f, ZONED_DATE_TIME));
        volBars.add(new VolBar(0.25f, ZONED_DATE_TIME));
        volBars.add(new VolBar(0.50f, ZONED_DATE_TIME));
        VolHistory vh = new VolHistory("FB");
        vh.addAll(volBars);
        int ivp = vh.ivPercentile();
        assertEquals(75, ivp);
        int ivr = vh.ivRank();
//        String actualDebug = vh.getIvRankDebug();
//        System.out.println(actualDebug);
        assertEquals(100, ivr);

    }

    @Test
    public void testIvStats2(){
        List<VolBar> volBars = new ArrayList<>();
        volBars.add(new VolBar(0.10f, ZONED_DATE_TIME));
        volBars.add(new VolBar(0.10f, ZONED_DATE_TIME));
        volBars.add(new VolBar(0.25f, ZONED_DATE_TIME));
        volBars.add(new VolBar(0.50f, ZONED_DATE_TIME));
        volBars.add(new VolBar(0.30f, ZONED_DATE_TIME));
        VolHistory vh = new VolHistory("FB");
        vh.addAll(volBars);
        int ivp = vh.ivPercentile();
        assertEquals(60, ivp);
        int ivr = vh.ivRank();
//        String actualDebug = vh.getIvRankDebug();
//        System.out.println(actualDebug);
        assertEquals(50, ivr);
    }

    @Test
    public void testFormat(){
        NumberFormat formatter = new DecimalFormat("#0.0000");
        assertEquals("0.1235", formatter.format(0.123456));
    }

}
