package org.jtda.cmd;

import org.apache.commons.lang3.StringUtils;
import org.jtda.model.XmlNode;
import org.jtda.model.XmlNodeBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class XmlNodeBuilderTest {

    @Test
    public void testSimpleParse() {
        String unformattedXml =
                //"<?xml version=\"1.0\" encoding=\"UTF-16\"?>" +
                        "<Message><Content type=\"blah\">Hello World</Content></Message>";

        String expected = "Content";


        StringBuilder sb = new StringBuilder();
        XmlNode root = new XmlNodeBuilder(unformattedXml).getRoot();

        for(XmlNode node: root.getNodeList()){
            sb.append(StringUtils.leftPad(node.getShortTreePath(), node.getDepth()));
        }
        assertEquals(expected, sb.toString());
    }

    @Test
    public void testNestedParse() {
        String unformattedXml =
                //"<?xml version=\"1.0\" encoding=\"UTF-16\"?>" +
                "<Message>" +
                        "<Content type=\"blah\">Hello World</Content>" +
                        "<Another><foo><bar><baz>1</baz><baz>2</baz><baz>3</baz></bar></foo></Another>" +
                        "</Message>";

        String expected = "\n" +  //empty root node
                "Content\n" +
                "Another\n" +
                "Another,foo\n" +
                "Another,foo,bar\n" +
                "Another,foo,bar,baz\n" +
                "Another,foo,bar,baz\n" +
                "Another,foo,bar,baz\n";


        StringBuilder sb = new StringBuilder();
        XmlNode root = new XmlNodeBuilder(unformattedXml).getRoot();

        for(XmlNode node: root.getNodeList()){
            //node.getDepth must not get set so it doesn't pad with spaces
            sb.append(StringUtils.leftPad(node.getShortTreePath(), node.getDepth()));
            sb.append(System.lineSeparator());
        }
        assertEquals(expected, sb.toString());
        XmlNode another = root.getChildWithName("Another");
        XmlNode foo = another.getChildWithName("foo");
        assertNotNull(foo);
        XmlNode bar = foo.getChildWithName("bar");
        XmlNode[] bazList = bar.getChildrenWithName("baz");
        assertEquals(3, bazList.length);
        assertEquals("1", bazList[0].getValue());
        assertEquals("2", bazList[1].getValue());
        assertEquals("3", bazList[2].getValue());
    }

}
