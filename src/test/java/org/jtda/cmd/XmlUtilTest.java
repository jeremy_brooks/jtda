package org.jtda.cmd;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class XmlUtilTest {
    private static final Logger LOG = LoggerFactory.getLogger(XmlUtilTest.class);

    @Test
    public void testSimpleFormat() {
        String unformattedXml =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?>" +
                        "<Message><Content type=\"blah\">Hello World</Content></Message>";

        String expectedFormat =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n" +
                        "<Message>\n" +
                        "    <Content type=\"blah\">Hello World</Content>\n" +
                        "</Message>\n";

        String formatted = XmlUtil.format(unformattedXml);
        assertEquals(expectedFormat, formatted);
    }

    @Test
    public void doubleTest() {
        double value = 0.585;
        System.out.println(new BigDecimal(value));
        System.out.println(BigDecimal.valueOf(value));
    }

    @Test
    public void testMaskAccountNumbers() {
        String unmaskedXml =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?>" +
                        "<Message><account-id>123456789</account-id><abc>123</abc>" +
                        "<associated-account-id>987</associated-account-id></Message>";

        String maskedXml =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?>" +
                        "<Message><account-id>-1</account-id><abc>123</abc>" +
                        "<associated-account-id>-2</associated-account-id></Message>";

        String formatted = XmlUtil.maskAccountNumbers(unmaskedXml);
        assertEquals(maskedXml, formatted);
    }

    @Test
    public void testBigDecimalParsing() {
        BigDecimal bd = XmlUtil.parseBigDecimal("234.56");
        assertEquals(new BigDecimal("234.56"), bd);
    }

    @Test
    public void testBigDecimalParsingWithGroupingSeparator() {
        BigDecimal bd = XmlUtil.parseBigDecimal("-1,234.56");
        assertEquals(new BigDecimal("-1234.56"), bd);
    }

    @Test
    public void testBigDecimalParsingEmptyString() {
        BigDecimal bd = XmlUtil.parseBigDecimal("");
        assertEquals(BigDecimal.ZERO, bd);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBigDecimalParsingDoubleNan() {
        BigDecimal bd = XmlUtil.parseBigDecimal(Double.toString(Double.NaN));
        assertEquals(BigDecimal.ZERO, bd);
    }

    @Test
    public void testToBigDecimal() {
        BigDecimal bd = XmlUtil.toBigDecimal(Double.NaN);
        assertEquals(BigDecimal.ZERO, bd);
    }

    @Test
    public void testParseDouble() {
        double d = XmlUtil.parseDoubleOrEmpty("-1234.56");
        assertEquals(-1234.56, d, 0.000001);

        double d2 = XmlUtil.parseDoubleOrEmpty("-1,234.56");
        assertEquals(0.0, d2, 0.000001);

        double d3 = XmlUtil.parseDoubleOrEmpty("");
        assertEquals(0.0, d3, 0.000001);

        double d4 = XmlUtil.parseDoubleOrEmpty("-abc");
        assertEquals(0.0, d4, 0.000001);
    }

    @Test
    public void testParseLocalDate(){
//        LocalDate ld = XmlUtil.parseLocalDate("2018-07-20");
//        LocalDate expected = LocalDate.of(2018, 7, 20);
//        assertEquals(expected, ld);

        LocalDate ld2 = XmlUtil.parseLocalDate("20180720");
        LocalDate expected2 = LocalDate.of(2018, 7, 20);
        assertEquals(expected2, ld2);
    }

}
