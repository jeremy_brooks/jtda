package org.jtda.cmd;

import org.jtda.model.VolBar;
import org.jtda.model.VolData;
import org.jtda.model.VolHistory;
import org.jtda.cmd.DefaultRangeFiller;
import org.jtda.cmd.Frequency;
import org.jtda.cmd.RangeFiller;
import org.junit.Test;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeremy on 6/4/18.
 */
public class DefaultRangerFillerTest {

    @Test
    public void testFill(){
        VolHistory vh = new VolHistory("FB", Frequency.parse("1d"));
        vh.add(new VolBar(0.46f, ZonedDateTime.now()));
        vh.add(new VolBar(0.23f, ZonedDateTime.now().plusDays(4)));
        assertEquals("precondition not met",2, vh.size());

        RangeFiller filler = new DefaultRangeFiller();
        VolHistory vhFilled = filler.fill(vh, RangeFiller.FillFrequency.DAILY);
        assertEquals(5, vhFilled.size());
        for(VolData vd: vhFilled){
            System.out.println(vd);
        }
    }

    @Test
    public void testMe(){
        ZonedDateTime d1 = ZonedDateTime.now();
        ZonedDateTime d2 = d1.plusDays(4);
        System.out.println(d1 + " = " + d1.toEpochSecond());
        System.out.println(d2 + " = " + d2.toEpochSecond());

    }
}
