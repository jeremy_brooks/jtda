package org.jtda.handler;

import org.jtda.cmd.XmlUtil;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by jeremy on 06-09-2018.
 */
public class OptionChainBinaryHandlerTest {

    @Test
    public void testParsingWithQuotes(){
        try {
            byte[] fileBytes = XmlUtil.filetoByteArray(
                    Paths.get("src/test/resources/binaryoptionchain-quotes.dat").toFile());
            OptionChainBinaryHandler handler = new OptionChainBinaryHandler();
            handler.parse(fileBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParsingWithoutQuotes(){
        try {
            byte[] fileBytes = XmlUtil.filetoByteArray(
                    Paths.get("src/test/resources/binaryoptionchain-noquotes-a.dat").toFile());
            OptionChainBinaryHandler handler = new OptionChainBinaryHandler(false);
            handler.parse(fileBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParsingWithoutQuotesWithAnOddNumberOfContractsReturned(){
        try {
            byte[] fileBytes = XmlUtil.filetoByteArray(
                    Paths.get("src/test/resources/binaryoptionchain-noquotes-spy.dat").toFile());
            OptionChainBinaryHandler handler = new OptionChainBinaryHandler(false);
            handler.parse(fileBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParsingWithoutQuotesWithRangeEqualToN(){
        try {
            byte[] fileBytes = XmlUtil.filetoByteArray(
                    Paths.get("src/test/resources/binaryoptionchain-range-is-n.dat").toFile());
            OptionChainBinaryHandler handler = new OptionChainBinaryHandler();
            handler.parse(fileBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = java.nio.BufferUnderflowException.class)
    public void testParsingInternalServerError(){
        //this can happen when session expires and you call a command that requires a jsessionid
        //note the parsing code does not return an error code of 0x01 as docs say it should
        //it returns (in hex) 00 15 49 ... where the first byte is 0x00 and next byte is length of
        //error message "Internal Server Error". TD's API should change the first byte to indicate
        //an error (0x01) so clients can handle parsing gracefully.
        try {
            byte[] fileBytes = XmlUtil.filetoByteArray(
                    Paths.get("src/test/resources/binaryoptionchain-internalservererror.dat").toFile());
            OptionChainBinaryHandler handler = new OptionChainBinaryHandler();
            handler.parse(fileBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParsing20190316a(){
        try {
            byte[] fileBytes = XmlUtil.filetoByteArray(
                    Paths.get("src/test/resources/binaryoptionchain-aapl-saturday-2019-03-16.dat").toFile());
            OptionChainBinaryHandler handler = new OptionChainBinaryHandler();
            handler.parse(fileBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParsing20190316b(){
        try {
            byte[] fileBytes = XmlUtil.filetoByteArray(
                    Paths.get("src/test/resources/binaryoptionchain-fb-saturday-2019-03-16.dat").toFile());
            OptionChainBinaryHandler handler = new OptionChainBinaryHandler();
            handler.parse(fileBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
