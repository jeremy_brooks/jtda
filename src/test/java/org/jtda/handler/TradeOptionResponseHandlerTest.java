package org.jtda.handler;

import org.apache.commons.io.IOUtils;
import org.jtda.model.TradeOptionResponse;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by jeremy on 06-09-2018.
 */
public class TradeOptionResponseHandlerTest {

    @Test
    public void testParsingOrderSubmitted(){
        try {
            final String xml = IOUtils.toString(
                    Paths.get("src/test/resources/tradeoptioncmd-ordersubmitted.xml").toUri());
            TradeOptionResponseHandler handler = new TradeOptionResponseHandler();
            TradeOptionResponse r = handler.parse(xml);
            assertNull(r.getErrorMessage());
            assertEquals("symbol=AAPL_101918C220~accountid=865630518~quantity=3~price=3.01~expire=day~" +
                            "ordtype=limit~action=buytoopen", r.getOrderString());
            assertEquals("AAPL_101918C220", r.getOrder().getSymbol());
            assertEquals("AAPL_101918C220", r.getOrder().getSecurity().getSymbol());
            assertEquals("AAPL_101918C220", r.getOrder().getSecurity().getSymbolWithTypePrefix());
            assertEquals("AAPL Oct 19 2018 220 Call", r.getOrder().getSecurity().getDescription());
            assertEquals("O",               r.getOrder().getSecurity().getAssetType());

            assertEquals("B", r.getOrder().getAction());
            assertEquals("3.01", r.getOrder().getLimitPrice());
            assertEquals("O", r.getOrder().getOpenClose());
            assertEquals("1883124034", r.getOrder().getOrderId());
            assertEquals("L", r.getOrder().getOrderType());
            assertEquals("C", r.getOrder().getPutCall());
            assertEquals("", r.getOrder().getStopPrice());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParsingOrderRejected(){
        try {
            final String xml = IOUtils.toString(
                    Paths.get("src/test/resources/tradeoptioncmd-orderrejected.xml").toUri());
            TradeOptionResponseHandler handler = new TradeOptionResponseHandler();
            TradeOptionResponse r = handler.parse(xml);
            assertNull(r.getOrder());
            assertEquals("symbol=AAPL_101918C220~accountid=865630518~quantity=200~price=7.01~" +
                            "expire=day~ordtype=limit~action=selltoclose", r.getOrderString());
            assertEquals("You will open a prohibited position with BP: Illegal -1 shares", r.getErrorMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
